﻿using DailyBasket.Warehouse.Core.Interface.BLL.Login;
using System;
using DailyBasket.Warehouse.Core.Model.Login;
using DailyBasket.Warehouse.Repository.User;
using System.Data;
using DailyBasket.Warehouse.Core.Model.Users;
using DailyBasket.Warehouse.Core.Helper;

namespace DailyBasket.Warehouse.BLL
{
    public class AccountBLL : IUser
    {

        Core.Interface.DAL.Login.IUser iUserRepository;
        public AccountBLL()
        {
            iUserRepository = new UserRepository();
        }
        public SessionUserModel Authenticate(LoginModel loginModel)
        {
            loginModel.Password = CryptographyHelper.Encrypt(loginModel.Password);
            DataTable dataTable = iUserRepository.Authenticate(loginModel);
            if(dataTable != null && dataTable.Rows.Count != 0)
            {
                //loginModel = null;
                SessionUserModel userModel = new SessionUserModel();
                userModel.UserId = Convert.ToUInt64(dataTable.Rows[0]["user_id"].ToString());
                userModel.UserName = dataTable.Rows[0]["user_name"].ToString();
                userModel.UserType = dataTable.Rows[0]["user_type"].ToString();
                return userModel;
            }
            return null;
        }

        public int RegisterUser(UserModel userModel, ulong userId)
        {
            throw new NotImplementedException();
        }
    }
}