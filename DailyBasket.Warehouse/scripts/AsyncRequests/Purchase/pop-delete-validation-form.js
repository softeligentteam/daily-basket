﻿var poId;
function pendingProductOrder(id) {
    poId = id;
    if (confirm("Are you sure to delete this Pending Purchase Order?")) {
        if (poId !== '') {
            $.get("/WebApi/Purchase/DeletePendingPurchaseEntry", { 'poId': poId })
            .done(function (data) {
                if (data !== 0) {
                    alert('Pending Purchase Product is deleted successfully!');
                    window.location.reload();
                }
                else {
                    alert('Sorry, Pending Purchase Product is not deleted successfully, Please try again!');
                }
            });
        }
    }
    else {
        var status = false;
        return false;
    }
}