var isChecked;
var btnBillStatus = true;
var btnFillStatus = true;
var status = true;
$(document).ready(function () {
    isChecked = $("#AutoGenerateBarcode").is(":checked");
    $("#AddNextProduct").prop('disabled', true);
    $("#SubmitOrder").prop('disabled', true);
    if (isChecked == false) {
        $("#ProductBarcodeId").attr('required', true);
    }
    // remove
    $("#PurchaseRate").change(function () {
        validateTotalAmount();
    });

    $("#Quantity").change(function () {
        validateTotalAmount();
    });

    setBatchDetails(); // Refer Stock/product-batch-validation
});

function validatePop() {
    if(validateButton() && validateTotalAmount()){
        return true;
    }
    else {
        return false;
    }
}
/*************************************************Date Validations*************************************************************/
// Expiry date
function validateExpiryDate() {
       var dateVal = $('#ExpiryDate').val();
        if (isDate(dateVal)) {
            $("#ExpiryDate").next().html('');
            status = true;
            btnBillStatus = true;
            validateButton();
        }
        else {
            $("#ExpiryDate").next().html('Please Select valid Date');
            status = false;
            btnBillStatus = false;
            validateButton();
        }
}

// Packaging date
function validatePackagingDate() {
       var dateVal = $('#PackagingDate').val();
        if (isDate(dateVal)) {
            $("#PackagingDate").next().html('');
            status = true;
            btnFillStatus = true;
            validateButton();
        }
        else {
            $("#PackagingDate").next().html('Please Select valid Date');
            status = false;
            btnFillStatus = false;
            validateButton();
        }
}

function validateButton() {
   if (status === false || btnBillStatus === false || btnFillStatus === false) {
        return false;
    }
    else {
        return true;
    }
}

function isDate(dateVal) {
   var currVal = dateVal;
    if (currVal == '') {
        return true;
    }
    else {
       var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtDay = dtArray[1];
        dtMonth = dtArray[3];
        dtYear = dtArray[5];

        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }
}

function validateTotalAmount() {
   var totalBillAmount = parseInt($("#TotalBAmount").text());
    var totalProductsAmount = $("#TotalPAmount").text();
    var quantity = $("#Quantity").val();
    var purchaseRate = $("#PurchaseRate").val();
    if (quantity === '') {
        quantity = 0;
    }
    if (purchaseRate === '') {
        purchaseRate = 0;
    }
    var totalPurchaseAmount = parseInt(quantity) * parseInt(purchaseRate) + parseInt(totalProductsAmount);
    if (totalPurchaseAmount == totalBillAmount) {

        $("#AddNextProduct").prop('disabled', true);
        $("#SubmitOrder").prop('disabled', false);
        return true;
    }
    else if (totalPurchaseAmount > totalBillAmount) {
        $("#SubmitOrder").prop('disabled', true);
        $("#AddNextProduct").prop('disabled', true);
        return false;
    }
    else if (totalPurchaseAmount == 0) {
        $("#AddNextProduct").prop('disabled', true);
        return false;
    }
    else {
        $("#AddNextProduct").prop('disabled', false);
        $("#SubmitOrder").prop('disabled', true);
        return true;
    }
}


//Auto Generate Barcode 
$("#AutoGenerateBarcode").change(function () {
    var isChecked = $("#AutoGenerateBarcode").is(":checked");
    if (isChecked) {
        // $("#ProductBarcodeId").prop('disabled', true);
        $("#ProductBarcodeId").attr('required', false);
        $("#ProductBarcodeId").hide();
        $("#PBarcodeId").hide();
    } else {
        $("#ProductBarcodeId").show();
        $("#PBarcodeId").show();
    }
});

$("#SubmitOrder").click(function () {
    if (confirm("Are you sure to submit this Purchase Order?")) {
        return true;
    }
    else {
        return false;
    }
});
