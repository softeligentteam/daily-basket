﻿var upStatus;
var btnBillStatus = true;
var btnFillStatus = true;
var status = true;
$(document).ready(function () {
    var isChecked = $("#AutoGenerateBarcode").is(":checked");
    $("#SubmitOrder").prop("disabled", true);
    if (isChecked == true) {
        $("#ProductBarcodeId").hide();
        $("#PBarcodeId").hide();
        $("#ProductBarcodeId").attr('required', false);

    }
    else {
        $("#ProductBarcodeId").attr('required', true);
    }
    $("#PurchaseRate").change(function () {
        validateTotalAmount();
    });

    $("#Quantity").change(function () {
        validateTotalAmount();
    });

});

/*************************************************Date Validations*************************************************************/
// Expiry date
function validateExpiryDate() {
    var dateVal = $('#ExpiryDate').val();
    if (isDate(dateVal)) {
        $("#ExpiryDate").next().html('');
        status = true;
        btnBillStatus = true;

    }
    else {
        $("#ExpiryDate").next().html('Please Select valid Date');
        status = false;
        btnBillStatus = false;

    }
}

// Packaging date
function validatePackagingDate() {
    var dateVal = $('#PackagingDate').val();
    if (isDate(dateVal)) {
        $("#PackagingDate").next().html('');
        status = true;
        btnFillStatus = true;
    }
    else {
        $("#PackagingDate").next().html('Please Select valid Date');
        status = false;
        btnFillStatus = false;

    }
}

function validateButton() {
    if (status === false || btnBillStatus === false || btnFillStatus === false) {
        return false;
    }
    else {
        return true;
    }
}

function isDate(dateVal) {
    var currVal = dateVal;
    if (currVal == '') {
        return true;
    }
    else {
        var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtDay = dtArray[1];
        dtMonth = dtArray[3];
        dtYear = dtArray[5];

        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }
}
/************************************************End of date validations********************************************************************/
function validateTotalAmount() {
    var newtotalPurchaseRate;
    var totalBillAmount = parseInt($("#TotalBAmount").text());
    var totalProductsAmount = $("#TotalPAmount").text();
    var quantity = $("#Quantity").val();
    var purchaseRate = $("#PurchaseRate").val();
    if (refPurchaseRate > purchaseRate) {
        newtotalPurchaseRate = parseFloat(totalProductsAmount) - parseInt(refPurchaseQuantity) * parseFloat(refPurchaseRate);
    }
    else if (refPurchaseRate < purchaseRate) {
        newtotalPurchaseRate = parseFloat(totalProductsAmount) - parseInt(refPurchaseQuantity) * parseFloat(refPurchaseRate);
    }
    else if (refPurchaseRate == purchaseRate) {
        newtotalPurchaseRate = parseFloat(totalProductsAmount) - parseInt(purchaseRate);
    }

    if (quantity === '') {
        quantity = 0;
    }
    if (purchaseRate === '') {
        purchaseRate = 0;
    }
    var totalPurchaseAmount = parseInt(quantity) * parseInt(purchaseRate) + parseInt(newtotalPurchaseRate);
    if (totalPurchaseAmount == totalBillAmount) {
        $("#ProductUpdate").prop("disabled", true);
        $("#SubmitOrder").prop("disabled", false);
        return true;
    }
    else if (totalPurchaseAmount > totalBillAmount) {
        $("#ProductUpdate").prop('disabled', true);
        $("#SubmitOrder").prop("disabled", true);
        alert("Total products amount is greather than Total bill amount, Please check once.");
        return false;

    }
    else if (totalPurchaseAmount == 0) {
        $("#ProductUpdate").prop('disabled', true);
        $("#SubmitOrder").prop("disabled", true);
        return false;
    }
    else {
        $("#ProductUpdate").prop('disabled', false);
        $("#SubmitOrder").prop("disabled", true);
        return true;
    }
}

//Auto Generate Barcode 
$("#AutoGenerateBarcode").change(function () {
    var isChecked = $("#AutoGenerateBarcode").is(":checked");
    var productBarcode = $("#ProductBarcodeId");
    if (isChecked == true) {
        $("#ProductBarcodeId").attr('required', false);
        productBarcode.hide();
        $("#PBarcodeId").hide();
    } else {
        $("#ProductBarcodeId").attr('required', true);
        productBarcode.prop('disabled', false);
        productBarcode.val();
        productBarcode.show();
        $("#PBarcodeId").show();
    }
});
function confirmToSubOrUpdate() {
    validateProductBarcode();
    if (upStatus == true) {
        if (confirm("Are you sure to update this Purchase Order?")) {
            return true;
        }
        else {
            return false;
        }
    }
}
function validateProductBarcode() {
    var isChecked = $("#AutoGenerateBarcode").is(":checked");
    var barcode = $("#ProductBarcodeId").val();
    if (isChecked == false && barcode == '') {
        return upStatus = false;
    }
    else {
        return upStatus = true;
    }
}

function validatePop() {
    if (validateButton() && validateTotalAmount()) {
        return confirmToSubOrUpdate();
    }
    else {
        return false;
    }
}