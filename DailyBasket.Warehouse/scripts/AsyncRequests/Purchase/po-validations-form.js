﻿
var btnBillStatus = true;
var btnFillStatus = true;
var status = true;

// Billing date
function validateBillDate() {
    
        var dateVal = $('#BillDate').val();
        if (isDate(dateVal)) {
            $("#BillDate").next().html('');
            status = true;
            btnBillStatus = true;
        }
        else {
            $("#BillDate").next().html('Please Select valid Date');
            status = false;
            btnBillStatus = false;
        }
        validateButton();
}

function validateUpdatePendingOrder() {
    debugger;
    var totalOrderAmount = $('#TotalBillAmount').val();
    if(parse.Float(productTotalAmount) <= parse.Float(totalOrderAmount).toFixed()){
        if (validateButton()) {
            return true;
        }
        return false;
    }
    else {
        $("#TotalBillAmount").next().html("Total Bill Amount must be equal or greater than Product Total Amount("+productTotalAmount+")");
        return false;
    }
    
}

function validateButton() {
    if (status === false || btnBillStatus === false || btnFillStatus === false) {
        //$("input[type='submit']").attr('disabled', true);
        return false;
    }
    else {
        //$("input[type='submit']").attr('disabled', false);
        return true;
    }
}
// Filling date
function validateFillDate() {
    
        var dateVal = $('#FillingDate').val();
        if (isDate(dateVal)) {
            $("#FillingDate").next().html('');
            status = true;
            btnFillStatus = true;
        }
        else {
            $("#FillingDate").next().html('Please Select valid Date');
            status = false;
            btnFillStatus = false;
        }
        validateButton();
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}


function isDate(dateVal) {
    var currVal = dateVal;
    if (currVal == '')
        return false;
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}

