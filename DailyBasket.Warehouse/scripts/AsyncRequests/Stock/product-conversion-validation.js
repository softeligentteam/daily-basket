﻿var autoPopulateProductList = new Array();
var expiryDate = true;
var packagingDate = true;
$(document).ready(function () {
    
    $(jsonProductList).each(function (index, product) {
        autoPopulateProductList.push(product.Text);
    });

    if ($("#SelectProductId").val() != '') {
        var id = "";
        $(jsonProductList).each(function (index, product) {
            if (product.Text == $("#SelectProductId").val()) {
                id = product.Value;
            }
        });
        $('#ProductId option:selected').removeAttr('selected');
        $("#ProductId option[value='" + id + "']").attr("selected", true);
        $('#ProductId').val(id);
    }
    debugger;
    // Remove default 0 values
    resetConvertableValues();

    $("#SelectProductId").autocomplete({
        source: autoPopulateProductList,

        change: function (event, ui) {

            if (!ui.item) {
                this.value = '';
                $('#ProductId option:selected').removeAttr('selected');
                $("#ProductId option[value='']").attr("selected", true);
            }
            else {
                var id = "";
                $(jsonProductList).each(function (index, product) {
                    if (product.Text == ui.item.value) {
                        id = product.Value;
                    }
                });
                $('#ProductId option:selected').removeAttr('selected');
                $("#ProductId option[value='" + id + "']").attr("selected", true);
                $('#ProductId').val(id);
                getProductPakagingSizeAndSaleRate();
            }
        }
    });
});

function getProductPakagingSizeAndSaleRate() {
    
    var product_id = $('#ProductId option:selected').val();
    if (product_id !== '' && product_id !== undefined) {
        $.ajax({
            url: window.location.origin + '/WebApi/Product/GetConvertableProductPackagingSizeAndSaleRate',
            dataType: 'json',
            data: { 'ProductId': product_id },
            type: "get",
            success: function (data) {
                
                // Sale Rate Section
                var saleRates = data["SaleRates"];
                if (saleRates !== null) {
                    $('#MRP').val(saleRates.Mrp);
                    $('#OnlineSaleRate').val(saleRates.OnlineSaleRate);
                    $('#SaleRateA').val(saleRates.SaleRateA);
                    $('#SaleRateB').val(saleRates.SaleRateB);
                }
                else {
                    resetPrices();
                }
            },
            error: function (error) {
                resetConvertableProduct();
                alert("Opp's error occured! Try again.");
            }
        });
    }
    else {
        resetConvertableProduct();
    }
}


/**************** Validate Dates ********************/

// Expiry date
function validateExpiryDate() {
    var dateVal = $('#ExpiryDate').val();
    if (isDate(dateVal)) {
        $("#ExpiryDate").next().html('');
        expiryDate = true;
    }
    else {
        $("#ExpiryDate").next().html('Please Select valid Date');
        expiryDate = false;
    }
}

// Packaging date
function validatePackagingDate() {
    var dateVal = $('#PackagingDate').val();
    if (isDate(dateVal)) {
        $("#PackagingDate").next().html('');
        packagingDate = true;
    }
    else {
        $("#PackagingDate").next().html('Please Select valid Date');
        packagingDate = false;
    }
}

function isDate(dateVal) {
    var currVal = dateVal;
    if (currVal == '') {
        return true;
    }
    else {
        var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtDay = dtArray[1];
        dtMonth = dtArray[3];
        dtYear = dtArray[5];

        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }
}

function validateConvertableProduct() {
    if (expiryDate && packagingDate)
        return true;
    return false;
}


/************************** Reset Form ****************************/
function resetConvertableProduct() {

    $('#ProductId').append($("<option/>", {
        value: '',
        text: 'Select Product Name'
    }));
    resetConvertableValues();
    resetPrices();
}

function resetConvertableValues() {

    $('#ConvertStockFrom').val("");
    $('#ConvertStockInto').val("");
}

function resetPrices() {
    $('#MRP').val("");
    $('#SaleRateA').val("");
    $('#SaleRateB').val("");
    $('#OnlineSaleRate').val("");
}

function convertStockNumber() {
    
    var convertFrom = parseFloat($("#ConvertStockFrom").val());
    if (convertFrom > availStock || convertFrom < 1) {
        $('#ConvertStockFrom').next().html('Please check available stock');
        resetConvertableValues();
        return false;
    }
    $('#ConvertStockFrom').next().html('');
    $('#ConvertStockInto').next().html('');
    return true;
}

