﻿$("#BatchNo").change(function () {
    setBatchDetails();
});

function setBatchDetails() {
    var batchNo = $("#BatchNo").val();
    if (batchNo == 0) {
        $("#ExpiryDate").val('01-01-0001');//.prop("readonly", true);
        $("#PackagingDate").val('01-01-0001');//.prop("readonly", true);
        $('#MRP').focus();
    }
    else {
        $("#ExpiryDate").prop("readonly", false);
        $("#PackagingDate").prop("readonly", false);
    }
}
$(document).ready(function () {

    setBatchDetails();
});