﻿//var mobileValidation = false;
var emailValidation = false;
var mobileValidation = false;
var gstINumberValidation = false;
function validateFranchisee() {
    
    /*if (mobileValidation == false) {
        mobileValidation = validateMobileNo();
    }

    if (emailValidation == false) {
        emailValidation = validateEmail();
    }*/

    debugger;
    if (mobileValidation && gstINumberValidation) {
        return true;
    }
    
    return false;
}

function validateGSTINumber() {
    debugger;
    gstINumberValidation = false;
    var value = $('#GSTNO').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsFranchiseeGSTINExists',
            dataType: 'json',
            data: { 'gstinumber': value },
            success: function (data) {
                if (data == true) {
                    $('#GSTNO').next().html('GSTIN Number is already exists.');
                    gstINumberValidation = false;
                }
                else {
                    $('#GSTNO').next().html('');
                    gstINumberValidation = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                gstINumberValidation = false;
            }
        });
    }
    else {
        gstINumberValidation = true;
    }
}

function validateMobileNo() {
    debugger;
    mobileValidation = false;
    var value = $('#Mobile').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsFranchiseeMobileNoExists',
            dataType: 'json',
            data: { 'franchiseeMobile': value },
            success: function (data) {
                if (data == true) {
                    debugger;
                    $('#Mobile').next().html('Mobile No is already exists.');
                    mobileValidation = false;
                    return mobileValidation;
                }
                else {
                    debugger;
                    $('#Mobile').next().html('');
                    mobileValidation = true;
                    return mobileValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return mobileValidation;
            }
        });
    }
    else {
        $('#Mobile').next().html('Mobile No is required field.');
        return mobileValidation;
    }
}

//Contact person mobile no. validation
function validatecpMobileNo() {
    debugger

    var value = $('#CPMobileNo').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsFranchiseeMobileNoExists',
            dataType: 'json',
            data: { 'franchiseeMobile': value },
            success: function (data) {
                if (data == true) {
                    $('#CPMobileNo').next().html('Mobile No is already exists.');
                    mobileValidation = false;
                    return mobileValidation;
                }
                else {
                    $('#CPMobileNo').next().html('');
                    mobileValidation = true;
                    return mobileValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return mobileValidation;
            }
        });
    }
    else {
        $('#CPMobileNo').next().html('Mobile No is required field.');
        return mobileValidation;
    }
}

//Official Email Id validation

function validateEmail() {
    debugger;
    var value = $('#OfficialEmailId').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsFranchiseeEmailIdExists',
            dataType: 'json',
            data: { 'UserEmail': value },
            success: function (data) {
                if (data == true) {
                    $('#OfficialEmailId').next().html('Email Id is already exists.');
                    emailValidation = false;
                    return emailValidation;
                }
                else {
                    $('#OfficialEmailId').next().html('');
                    emailValidation = true;
                    return emailValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                emailValidation = false;
                return emailValidation;
            }
        });
    }
    else {
        $('#OfficialEmailId').next().html('Email Id is required field.');
        emailValidation = false;
        return emailValidation;
    }
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}