﻿/**********************************************Warehouse User Validations**********************************************************/

var mobileValidation = false;
var emailValidation = false;

function validateMobileNo() {
    debugger;
    var value = $('#Mobile').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsWarehouseUserMobileNoExists',
            dataType: 'json',
            data: { 'WarehouseUserMobile': value },
            success: function (data) {
                if (data == true) {
                    $('#Mobile').next().html('Mobile No is already exists.');
                    mobileValidation = false;
                    return mobileValidation;
                }
                else {
                    $('#Mobile').next().html('');
                    mobileValidation = true;
                    return mobileValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return mobileValidation;
            }
        });
    }
    else {
        $('#Mobile').next().html('Mobile No is required field.');
        return mobileValidation;
    }
}

function validateEmail() {
    debugger;
    var value = $('#Email').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsWarehouseUserEmailIdExists',
            dataType: 'json',
            data: { 'UserEmail': value },
            success: function (data) {
                if (data == true) {
                    $('#Email').next().html('Email Id is already exists.');
                    emailValidation = false;
                    return emailValidation;
                }
                else {
                    $('#Email').next().html('');
                    emailValidation = true;
                    return emailValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                emailValidation = false;
                return emailValidation;
            }
        });
    }
    else {
        $('#Email').next().html('Email Id is required field.');
        emailValidation = false;
        return emailValidation;
    }
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}