﻿var mobileValidation = true;
//var emailValidation = false;
var gstINumberValidation = true;
function validateFranchisee() {
    
    /*if (mobileValidation == false) {
        mobileValidation = validateMobileNo();
    }

    if (emailValidation == false) {
        emailValidation = validateEmail();
    }*/
    if (mobileValidation && gstINumberValidation) {
        return true;
    }
    
    return false;
}

function validateGSTINumber() {
    gstINumberValidation = false;
    var value = $('#GSTNO').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsFranchiseeGSTINExists',
            dataType: 'json',
            data: { 'gstinumber': value },
            success: function (data) {
                if (data == true) {
                    $('#GSTNO').next().html('GSTIN Number is already exists.');
                    gstINumberValidation = false;
                }
                else {
                    $('#GSTNO').next().html('');
                    gstINumberValidation = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                gstINumberValidation = false;
            }
        });
    }
    else {
        gstINumberValidation = true;
    }
}

//function validateMobileNo() {
//    debugger;
//    mobileValidation = false;
//    var value = $('#Mobile').val();
//    if (value !== '') {
//        $.ajax({
//            type: "get",
//            url: window.location.origin + '/WebApi/Users/IsFranchiseeMobileNoExists',
//            dataType: 'json',
//            data: { 'franchiseeMobile': value },
//            success: function (data) {
//                if (data == true) {
//                    debugger;
//                    $('#Mobile').next().html('Mobile No is already exists.');
//                    mobileValidation = false;
//                    return mobileValidation;
//                }
//                else {
//                    debugger;
//                    $('#Mobile').next().html('');
//                    mobileValidation = true;
//                    return mobileValidation;
//                }
//            },
//            error: function (error) {
//                alert('error' + error);
//                return mobileValidation;
//            }
//        });
//    }
//    else {
//        $('#Mobile').next().html('Mobile No is required field.');
//        return mobileValidation;
//    }
//}

//Contact person mobile no. validation
function validatecpMobileNo() {
    var value = $('#CPMobileNo').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Users/IsFranchiseeMobileNoExists',
            dataType: 'json',
            data: { 'franchiseeMobile': value },
            success: function (data) {
                if (data == true) {
                    $('#CPMobileNo').next().html('Mobile No is already exists.');
                    mobileValidation = false;
                    return mobileValidation;
                }
                else {
                    $('#CPMobileNo').next().html('');
                    mobileValidation = true;
                    return mobileValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return mobileValidation;
            }
        });
    }
    else {
        $('#CPMobileNo').next().html('Mobile No is required field.');
        return mobileValidation;
    }
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}