﻿//var mobileValidation = false;
//var emailValidation = false;
var firmNameValidation = false;
var gstINumberValidation = false;
function validateVendor() {
    
    /*if (mobileValidation == false) {
        mobileValidation = validateMobileNo();
    }

    if (emailValidation == false) {
        emailValidation = validateEmail();
    }*/


    if (firmNameValidation && gstINumberValidation) {
        return true;
    }
    
    return false;
}

function validateFirmName() {
    
    var value = $('#FirmName').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Vendor/IsVendorNameExists',
            dataType: 'json',
            data: { 'vendorName': value },
            success: function (data) {
                if (data == true) {
                    $('#FirmName').next().html('Firm Name is already exists.');
                    firmNameValidation = false;
                    return firmNameValidation;
                }
                else {
                    $('#FirmName').next().html('');
                    firmNameValidation = true;
                    return firmNameValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return firmNameValidation;
            }
        });
    }
    else {
        $('#FirmName').next().html('Firm Name is required field.');
        return firmNameValidation;
    }
}

function validateGSTINumber() {
    
    gstINumberValidation = false;
    var value = $('#GSTIN').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Vendor/IsVendorGSTINExists',
            dataType: 'json',
            data: { 'gstinumber': value },
            success: function (data) {
                if (data == true) {
                    $('#GSTIN').next().html('GSTIN Number is already exists.');
                    gstINumberValidation = false;
                }
                else {
                    $('#GSTIN').next().html('');
                    gstINumberValidation = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                gstINumberValidation = false;
            }
        });
    }
    else {
        gstINumberValidation = true;
    }
}

/*function validateMobileNo() {
    
    var value = $('#Mobile').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Vendor/IsVendorMobileNoExists',
            dataType: 'json',
            data: { 'vendorMobile': value },
            success: function (data) {
                if (data == true) {
                    $('#Mobile').next().html('Mobile No is already exists.');
                    mobileValidation = false;
                    return mobileValidation;
                }
                else {
                    $('#Mobile').next().html('');
                    mobileValidation = true;
                    return mobileValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return mobileValidation;
            }
        });
    }
    else {
        $('#Mobile').next().html('Mobile No is required field.');
        return mobileValidation;
    }
}

function validateEmail() {
    
    var value = $('#Email').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Vendor/IsVendorEmailIdExists',
            dataType: 'json',
            data: { 'vendorEmail': value },
            success: function (data) {
                if (data == true) {
                    $('#Email').next().html('Email Id is already exists.');
                    emailValidation = false;
                    return emailValidation;
                }
                else {
                    $('#Email').next().html('');
                    emailValidation = true;
                    return emailValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                emailValidation = false;
                return emailValidation;
            }
        });
    }
    else {
        $('#Email').next().html('Email Id is required field.');
        emailValidation = false;
        return emailValidation;
    }
}*/

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}