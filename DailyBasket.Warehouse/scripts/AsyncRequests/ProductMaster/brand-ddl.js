var btnBillStatus = true;
var btnFillStatus = true;
var status = true;

$(document).ready(function () {
    $(jsonBrandList).each(function (index, brand) {
        autoPopulateBrandList.push(brand.Text);
    });

    if ($("#SelectBrandId").val() !== '') {
        var id = "";
        $(jsonBrandList).each(function (index, brand) {
            if (brand.Text == $("#SelectBrandId").val()) {
                id = brand.Value;
            }
        });
        $('#BrandId option:selected').removeAttr('selected');
        $("#BrandId option[value='" + id + "']").attr("selected", true);
        $('#BrandId').val(id);
    }

    $("#SelectBrandId").autocomplete({
        source: autoPopulateBrandList,

        change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
                $('#BrandId option:selected').removeAttr('selected');
                $("#BrandId option[value='']").attr("selected", true);
            }
            else {
                var id = "";
                $(jsonBrandList).each(function (index, brand) {
                    if (brand.Text == ui.item.value) {
                        id = brand.Value;
                    }
                });
                $('#BrandId option:selected').removeAttr('selected');
                $("#BrandId option[value='" + id + "']").attr("selected", true);
                $('#BrandId').val(id);
            }
        }
    });
});


function validateStartDate() {
    debugger;
    var dateVal = $('#StartDate').val();
    if (isDate(dateVal)) {
        $("#StartDate").next().html('');
        status = true;
        btnBillStatus = true;
    }
    else {
        $("#StartDate").next().html('Please Select valid Date');
        status = false;
        btnBillStatus = false;
    }
    validateButton();
}

// Filling date
function validateEndDate() {
    debugger;
    var dateVal = $('#EndDate').val();
    if (isDate(dateVal)) {
        $("#EndDate").next().html('');
        status = true;
        btnFillStatus = true;
    }
    else {
        $("#EndDate").next().html('Please Select valid Date');
        status = false;
        btnFillStatus = false;
    }
    validateButton();
}


function validateButton() {
    if (status === false || btnBillStatus === false || btnFillStatus === false) {
        return false;
    }
    else {
        return true;
    }
}

function isDate(dateVal) {
    debugger;
    var currVal = dateVal;
    if (currVal == '')
        return false;
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}