
/********************************************* Product Image1 Validation  ***************************************************/
var scImage = false;
var flag = false;
function GetSubCategoryImageSize(value) {

    var file = getNameFromPath($(value).val());

    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                flag = true;
                break;
            default:
                flag = false;
        }
    }

    if (flag == false) {
        debugger;
        var str = value.name;
        var res = str.split("_");
        var data = "_val" + res[1];
        $('#' + value.id).next().html('You can upload only jpg, jpeg, png extension file Only');
        scImage = flag;
        return false;
    }

    else {

        //this.files[0].size gets the size of your file.
        var fileUpload = $("#" + value.id)[0];
        //var fileUpload = value.files[0];
        //reader code for temp use//
        var reader = new FileReader();
        reader.readAsDataURL(fileUpload.files[0]);

        reader.onload = function (e) {
            //Initiate the JavaScript Image object.
            var image = new Image();
            //Set the Base64 string return from FileReader as source.
            image.src = e.target.result;
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                if (height != 200 && width != 200) {
                    $('#' + value.id).next().html('Image Height and Width must be 200*200 pixels.');
                    flag = false;
                    scImage = flag;
                    return flag;
                }
                else {
                    $('#' + value.id).next().html('');
                }
            };
            var size = (value.files[0].size / 1024);
            if (size > 100) {
                $('#' + value.id).next().html('You can upload file size up to 100 KB.');
                flag = false;
                scImage = flag;
                return flag;
            }
            else {
                $('#' + value.id).next().html('');
            }
        }
        flag = true;
        scImage = flag;
        return flag;
    }
}

//get file path from client system
function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}

function validateSubCategoryImage() {
    debugger;
    return scImage;
}



/********************************************* Product Subcategory Image Validation Over ***************************************************/
