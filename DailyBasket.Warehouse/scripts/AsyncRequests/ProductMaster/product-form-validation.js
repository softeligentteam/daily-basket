﻿var productNameValidation = false;
function validateProduct() {
    debugger;
    if (validateProductImages() && productNameValidation) {
        return true;
    }

    if (!productNameValidation) {
        return isProductNameExists("Name");
    }
    return false;
}

// Call while adding product
function validatePrductName(id) {

    return isProductNameExists(id);
}

function validateUpdateProduct() {
    
    var isProductNameChanged = $('#IsNameUpdated:checked').val();
    if (isProductNameChanged) {
        if (productNameValidation)
            return true;
        return false;
    }
    $('#Name').next().html('');
    return true;
}


// Call while updating product
function validateUpdatedPrductName(id) {
    
    var isProductNameChanged = $('#IsNameUpdated:checked').val();
    if (isProductNameChanged) {
        isProductNameExists(id);
    }
}

function isProductNameExists(id) {
    var prodName = $('#' + id).val();
    if (prodName !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Product/IsProductNameExists',
            dataType: 'json',
            data: { 'productName': prodName },
            success: function (data) {
                if (data == true) {
                    $('#' + id).next().html('Product Name is already exists.');
                    productNameValidation = false;
                    return productNameValidation;
                }
                else {
                    $('#' + id).next().html('');
                    productNameValidation = true;
                    return productNameValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                productNameValidation = false;
                return productNameValidation;
            }
        });
    }
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}