﻿/**************************** Ajax call for Product's Sub Category list *****************************/
$(document).ready(function () {
    $('#MainCategoryId').change(function () {
        
        var maincategory_id = $('#MainCategoryId option:selected').val();
        if (maincategory_id !== '') {
            $.ajax({
                url: window.location.origin + '/WebApi/Product/GetSubCategoryDdlList',
                dataType: 'json',
                data: { 'MainCategoryId': maincategory_id },
                type: "get",
                success: function (data) {
                    $('#SubCategoryId').html('');
                    $('#SubCategoryId').append($('<option/>', {
                        value: '',
                        text: "Select Product SubCategory"
                    })).attr('selected', true);
                    $.each(data, function (Index, SubCategory) {
                        $('#SubCategoryId').append($('<option/>', {
                            value: SubCategory.Id,
                            text: SubCategory.Name
                        }));
                    });
                },
                error: function (error) {
                    alert('error' + error);
                }
            });
        }
        else {
            reset_subcategoryddl();
        }
    });
});

/************************** Reset city and pincode ****************************/
function reset_subcategoryddl() {

    $('#SubCategoryId').html('');
    $('#SubCategoryId').append($("<option/>", {
        value: '',
        text: 'Select Product SubCategory'
    }));

}
