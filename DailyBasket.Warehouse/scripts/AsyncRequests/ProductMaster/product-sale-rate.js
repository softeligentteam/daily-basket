function getProductSaleRate() {
    debugger;
    var l;
    var product_id = $('#ProductId option:selected').val();
    if (product_id !== '') {
        $.ajax({
            url: window.location.origin + '/WebApi/StockProduct/StockProductSaleRate',
            dataType: 'json',
            data: { 'ProductId': product_id },
            type: "get",
            success: function (data) {
                if (data != null) {
                    $("#MRP").val(data.Mrp);
                    $("#OnlineSaleRate").val(data.OnlineSaleRate);
                    $("#SaleRateA").val(data.SaleRateA);
                    $("#SaleRateB").val(data.SaleRateB);
                }
                else {
                    $("#MRP").val('');
                    $("#OnlineSaleRate").val('');
                    $("#SaleRateA").val(0);
                    $("#SaleRateB").val(0);
                }
            },
            error: function (error) {
                return false;
            }
        });
    }
}