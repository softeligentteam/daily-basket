﻿var brandStatus = false;
function validateBrandForm() {
    debugger;
    if (getBrandImageStatus() && brandStatus) {
        return true;
    }
    return false;
}

function validateBrandUpdateName() {
    if (brandStatus) {
        return true;
    }
    return false;
}

function validateBrandUpdateImage() {
    if (getBrandImageStatus()) {
        return true;
    }
    return false;
}


$('#Name').change(function () {
    debugger;
    brandStatus = false;
    var value = $('#Name').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Product/IsBrandNameExists',
            dataType: 'json',
            data: { 'brandName': value },
            success: function (data) {
                debugger;
                if (data == true) { // It's already exists
                    $('#Name').next().html('Brand Name is already exists.');
                    brandStatus = false;
                }
                else {
                    $('#Name').next().html('');
                    brandStatus = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                brandStatus = false;
            }
        });
    }
    else {
        $('#Name').next().html('Brand Name is required field.');
        brandStatus = false;
    }
});


function removeValidationMessage(id) {
    $('#' + id).next().html('');
}