﻿var firmNameValidation = true;
var gstINumberValidation = true;
function validateVendor() {
    debugger;
    if (firmNameValidation && gstINumberValidation) {
        return true;
    }

    return false;
}

function validateFirmName() {
    firmNameValidation = false;
    var value = $('#FirmName').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Vendor/IsVendorNameExists',
            dataType: 'json',
            data: { 'vendorName': value },
            success: function (data) {
                if (data == true) {
                    $('#FirmName').next().html('Firm Name is already exists.');
                    firmNameValidation = false;
                    return firmNameValidation;
                }
                else {
                    $('#FirmName').next().html('');
                    firmNameValidation = true;
                    return firmNameValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                return firmNameValidation;
            }
        });
    }
    else {
        $('#FirmName').next().html('Firm Name is required field.');
        return firmNameValidation;
    }
}

function validateGSTINumber() {

    gstINumberValidation = false;
    var value = $('#GSTIN').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Vendor/IsVendorGSTINExists',
            dataType: 'json',
            data: { 'gstinumber': value },
            success: function (data) {
                if (data == true) {
                    $('#GSTIN').next().html('GSTIN Number is already exists.');
                    gstINumberValidation = false;
                }
                else {
                    $('#GSTIN').next().html('');
                    gstINumberValidation = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                gstINumberValidation = false;
            }
        });
    }
    else {
        gstINumberValidation = true;
    }
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}