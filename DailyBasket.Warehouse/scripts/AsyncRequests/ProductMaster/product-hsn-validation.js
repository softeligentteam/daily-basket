﻿var productHsnValidation = false;
function validateProductHSNForm() {
    return productHsnValidation;
}

function isProductHsnNumberExists(id) {
    debugger;
    var hsnNumber = $('#' + id).val();
    if (hsnNumber !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Product/IsProductHsnNumberExists',
            dataType: 'json',
            data: { 'productHsnNumber': hsnNumber },
            success: function (data) {
                if (data == true) {
                    $('#' + id).next().html('Product HSN Number is already exists.');
                    productHsnValidation = false;
                    return productHsnValidation;
                }
                else {
                    $('#' + id).next().html('');
                    productHsnValidation = true;
                    return productHsnValidation;
                }
            },
            error: function (error) {
                alert('error' + error);
                productHsnValidation = false;
                return productHsnValidation;
            }
        });
    }
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}