﻿var maincatStatus = false;
function validateMainCategoryForm() {
    
    if (getCategoryImageStatus() && maincatStatus) {
        return true;
    }
    return false;
}

function validateMainCategoryUpdateName() {
    if (maincatStatus) {
        return true;
    }
    return false;
}

function validateMainCategoryUpdateImage() {
    if (getCategoryImageStatus()) {
        return true;
    }
    return false;
}

function validateMainCategory() {
    maincatStatus = false;
    var value = $('#MainCategoryName').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Product/IsMainCategoryNameExists',
            dataType: 'json',
            data: { 'mainCategory': value },
            success: function (data) {
                if (data == true) { // It's already exists
                    $('#MainCategoryName').next().html('Main Category is already exists.');
                    //$("input[type=submit]").attr("disabled", "disabled");
                    maincatStatus = false;
                }
                else {
                    $('#MainCategoryName').next().html('');
                    //$("input[type=submit]").removeAttr("disabled");
                    maincatStatus = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                maincatStatus = false;
            }
        });
    }
    else {
        $('#MainCategoryName').next().html('Main Category is required field.');
        return false;
    }
}


function removeValidationMessage(id) {
    $('#' + id).next().html('');
}