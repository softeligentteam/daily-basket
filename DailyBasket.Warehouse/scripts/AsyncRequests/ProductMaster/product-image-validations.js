
/********************************************* Product Image1 Validation  ***************************************************/
var productImage1 = false;
var productImage2 = true;
var productImage3 = true;
var productImage4 = true;
var flag = false;
function GetProductImageSize(value) {
    debugger;

    var file = getNameFromPath($(value).val());

    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                flag = true;
                break;
            default:
                flag = false;
        }
    }

    if (flag == false) {
        debugger;
        var str = value.name;
        var res = str.split("_");
        var data = "_val" + res[1];
        $('#' + value.id).next().html('You can upload only jpg, jpeg, png extension file Only');
        setImageValidateStatus(value.name, flag);
        return false;
    }

    else {

        //this.files[0].size gets the size of your file.
        var fileUpload = $("#" + value.id)[0];
        //var fileUpload = value.files[0];
        //reader code for temp use//
        var reader = new FileReader();
        reader.readAsDataURL(fileUpload.files[0]);

        reader.onload = function (e) {
            //Initiate the JavaScript Image object.
            var image = new Image();
            //Set the Base64 string return from FileReader as source.
            image.src = e.target.result;
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                if (height != 400 && width != 400) {
                    $('#' + value.id).next().html('Image Height and Width must be 400*400 pixels.');
                    flag = false;
                    setImageValidateStatus(value.name, flag);
                    return flag;
                }
                else {
                    $('#' + value.id).next().html('');
                }
            };
            var size = (value.files[0].size / 1024);
            if (size > 400) {
                $('#' + value.id).next().html('You can upload file size up to 400 KB.');
                flag = false;
                setImageValidateStatus(value.name, flag);
                return flag;
            }
            else {
                $('#' + value.id).next().html('');
            }
        }
        flag = true;
        setImageValidateStatus(value.name, flag);
        return flag;
    }

}

//get file path from client system
function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}

function setImageValidateStatus(name, flag){
    debugger;
    switch (name) {
        case 'ProductImage1':
            productImage1 = flag;
            break;
        case 'ProductImage2':
            productImage1 = flag;
            break;
        case 'ProductImage3':
            productImage1 = flag;
            break;
        case 'ProductImage4':
            productImage1 = flag;
            break;
    }
}

function validateProductImages() {
    debugger;
    if (validateProductImage1() && validateProductImage2() && validateProductImage3() && validateProductImage4()) {
        return true;
    }
    return false;
}

function validateProductImage1() {

    if ($('#ProductImage1').val() !== null) {
        return productImage1;
    }
    return false;
}

function validateProductImage2() {

    if ($('#ProductImage2').val() !== null) {
        return productImage2;
    }
    return true;
}

function validateProductImage3() {

    if ($('#ProductImage3').val() !== null) {
        return productImage3;
    }
    return true;
}

function validateProductImage4() {

    if ($('#ProductImage4').val() !== null) {
        return productImage4;
    }
    return true;
}

/********************************************* Product Image1 Validation Over ***************************************************/
