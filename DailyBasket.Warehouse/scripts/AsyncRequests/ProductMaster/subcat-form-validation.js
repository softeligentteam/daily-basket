﻿var subcatStatus = false;
function validateSubCategoryForm() {
    debugger;
    //validateSubCategory() && 
    if (getCategoryImageStatus() && subcatStatus) {
        return true;
    }
    return false;
}

function validateSubCategoryUpdateName() {
    if (subcatStatus) {
        return true;
    }
    return false;
}

function validateSubCategoryUpdateImage() {
    if (getCategoryImageStatus()) {
        return true;
    }
    return false;
}

function validateSubCategory() {
    subcatStatus = false;
    var value = $('#SubCategoryName').val();
    if (value !== '') {
        $.ajax({
            type: "get",
            url: window.location.origin + '/WebApi/Product/IsSubCategoryNameExists',
            dataType: 'json',
            data: { 'subCategory': value },
            success: function (data) {
                if (data == true) {
                    $('#SubCategoryName').next().html('Sub Category is already exists.');
                    //$("input[type=submit]").attr("disabled", "disabled");
                    subcatStatus = false;
                }
                else {
                    $('#SubCategoryName').next().html('');
                    //$("input[type=submit]").removeAttr("disabled");
                    subcatStatus = true;
                }
            },
            error: function (error) {
                alert('error' + error);
                subcatStatus = false;
            }
        });
    }
    else {
        $('#SubCategoryName').next().html('Sub Category is required field.');
        return false;
    }
}


function removeValidationMessage(id) {
    $('#' + id).next().html('');
}