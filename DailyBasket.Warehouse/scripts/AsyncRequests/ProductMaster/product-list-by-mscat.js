﻿/**************************** Ajax call for Product's Sub Category list *****************************/
$(document).ready(function () {
    $('#SubCategoryId').change(function () {

        var maincategory_id = $('#MainCategoryId option:selected').val();
        var subcategory_id = $('#SubCategoryId option:selected').val();
        if (maincategory_id !== '' && subcategory_id !== '') {
            $.ajax({
                url: window.location.origin + '/WebApi/Product/GetProductsDdlListByMSCat',
                dataType: 'json',
                data: { 'MainCategoryId': maincategory_id, 'SubCategoryId': subcategory_id },
                type: "get",
                success: function (data) {
                    debugger;
                    $('#ProductId').html('');
                    $('#ProductId').append($('<option/>', {
                        value: '',
                        text: "Select Product Name"
                    })).attr('selected', true);
                    $.each(data, function (Index, Product) {
                        debugger;
                        $('#ProductId').append($('<option/>', {
                            value: Product.Id,
                            text: Product.Name
                        }));
                    });
                },
                error: function (error) {
                    alert('error' + error);
                }
            });
        }
        else {
            reset_productddl();
        }
    });
});

/************************** Reset city and pincode ****************************/
function reset_productddl() {

    $('#ProductId').html('');
    $('#ProductId').append($("<option/>", {
        value: '',
        text: 'Select Product Name'
    }));

}
