﻿$(document).ready(function () {

    //jsonMCList = @Html.Raw(Json.Encode(@ViewBag.MainCategoryDdlList));
    $(jsonMCList).each(function(index, MainCategory){
        autoPopulateMCList.push(MainCategory.Text);
    });

    if($("#SelectMainCategoryId").val() !== ''){
        var id = "";
        $(jsonMCList).each(function(index, MainCategory){
            if(MainCategory.Text == $("#SelectMainCategoryId").val()){
                id = MainCategory.Value;
            }
        });
        $('#MainCategoryId option:selected').removeAttr('selected');
        $("#MainCategoryId option[value='" + id + "']").attr("selected", true);
        $('#MainCategoryId').val(id);
    }

    $("#SelectMainCategoryId").autocomplete({
        source: autoPopulateMCList,

        change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
                $('#MainCategoryId option:selected').removeAttr('selected');
                $("#MainCategoryId option[value='']").attr("selected", true);
                reset_subcategoryddl();
            }
            else
            {

                var id = "";
                $(jsonMCList).each(function(index, MainCategory){
                    if(MainCategory.Text==ui.item.value){
                        id = MainCategory.Value;
                    }
                });
                $('#MainCategoryId option:selected').removeAttr('selected');
                $("#MainCategoryId option[value='" + id + "']").attr("selected", true);
                $('#MainCategoryId').val(id);
                getSubCategories();
            }
        }
    });


    //jsonBrandList = @Html.Raw(Json.Encode(@ViewBag.BrandDdlList));
    $(jsonBrandList).each(function(index, brand){
        autoPopulateBrandList.push(brand.Text);
    });

    if($("#SelectBrandId").val() !== ''){
        var id = "";
        $(jsonBrandList).each(function(index, brand){
            if(brand.Text== $("#SelectBrandId").val()){
                id = brand.Value;
            }
        });
        $('#BrandId option:selected').removeAttr('selected');
        $("#BrandId option[value='" + id + "']").attr("selected", true);
        $('#BrandId').val(id);
    }

    $("#SelectBrandId").autocomplete({
        source: autoPopulateBrandList,

        change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
                $('#BrandId option:selected').removeAttr('selected');
                $("#BrandId option[value='']").attr("selected", true);
            }
            else
            {
                var id = "";
                $(jsonBrandList).each(function(index, brand){
                    if(brand.Text==ui.item.value){
                        id = brand.Value;
                    }
                });
                $('#BrandId option:selected').removeAttr('selected');
                $("#BrandId option[value='" + id + "']").attr("selected", true);
                $('#BrandId').val(id);
            }
        }
    });

    //jsonProductHSNList = @Html.Raw(Json.Encode(@ViewBag.ProductHSNDdlList));
    $(jsonProductHSNList).each(function(index, productHsn){
        autoPopulateProductHSNList.push(productHsn.Text);
    });

    if($("#SelectProductHSNNumberId").val() !== ''){
        var id = "";
        $(jsonProductHSNList).each(function(index, productHsn){
            if(productHsn.Text== $("#SelectProductHSNNumberId").val()){
                id = productHsn.Value;
            }
        });
        $('#ProductHSNNumberId option:selected').removeAttr('selected');
        $("#ProductHSNNumberId option[value='" + id + "']").attr("selected", true);
        $('#ProductHSNNumberId').val(id);
    }

    $("#SelectProductHSNNumberId").autocomplete({
        source: autoPopulateProductHSNList,

        change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
                $('#ProductHSNNumberId option:selected').removeAttr('selected');
                $("#ProductHSNNumberId option[value='']").attr("selected", true);
            }
            else
            {
                var id = "";
                $(jsonProductHSNList).each(function(index, productHsn){
                    if(productHsn.Text==ui.item.value){
                        id = productHsn.Value;
                    }
                });
                $('#ProductHSNNumberId option:selected').removeAttr('selected');
                $("#ProductHSNNumberId option[value='" + id + "']").attr("selected", true);
                $('#ProductHSNNumberId').val(id);
            }
        }
    });


    function getSubCategories(){

        var maincategory_id = $('#MainCategoryId option:selected').val();
        if (maincategory_id !== '') {
            $.ajax({
                url: window.location.origin + '/WebApi/Product/GetSubCategoryDdlList',
                dataType: 'json',
                data: { 'MainCategoryId': maincategory_id },
                type: "get",
                success: function (data) {
                    jsonSCList = data;
                    $('#SubCategoryId').html('');
                    $('#SubCategoryId').append($('<option />', {
                        value: '',
                        text: "Select Product SubCategory"
                    })).attr('selected', true);
                    $.each(data, function (Index, SubCategory) {
                        $('#SubCategoryId').append($('<option />', {
                            value: SubCategory.Id,
                            text: SubCategory.Name
                        }));
                    });
                },
                error: function (error) {
                    alert('error' + error);
                }
            });
        }
        else {
            reset_subcategoryddl();
        }
    }

    //debugger;
    if($("#SelectSubCategoryId").val() !== ''){
        
        var id = "";
        $(jsonSCList).each(function(index, subCategory){
            if(subCategory.Text == $("#SelectSubCategoryId").val()){
                id = subCategory.Value;
            }
        });
        $('#SubCategoryId option:selected').removeAttr('selected');
        $("#SubCategoryId option[value='" + id + "']").attr("selected", true);
        $('#SubCategoryId').val(id);
    }


    $("#SelectSubCategoryId").autocomplete({
        source: autoPopulateSCList,

        change: function (event, ui) {

            if (!ui.item) {
                this.value = '';
                $('#SubCategoryId option:selected').removeAttr('selected');
                $("#SubCategoryId option[value='']").attr("selected", true);
            }
            else
            {
                var id = "";
                $(jsonSCList).each(function(index, subCategory){
                    if(subCategory.Name==ui.item.value){
                        id = subCategory.Id;
                    }
                });
                $('#SubCategoryId option:selected').removeAttr('selected');
                $("#SubCategoryId option[value='" + id + "']").attr("selected", true);
                $('#SubCategoryId').val(id);
            }
        }
    });

    setInterval(function () {

        autoPopulateSCList = [];
        $(jsonSCList).each(function(index, subCategory){
            autoPopulateSCList.push(subCategory.Name);
        });
        $('#SelectSubCategoryId').autocomplete("option", { source: autoPopulateSCList });
    }, 1000);


    /************************** Reset Sub Category ****************************/
    function reset_subcategoryddl() {

        $('#SelectSubCategoryId').val('');
        $('#SubCategoryId').html('');
        $('#SubCategoryId').append($("<option />", {
            value: '',
            text: 'Select Product SubCategory'
        }));
    }
});