﻿var imageStatus = false;
var flag = false;
function validateOfferImage(element) {

    var file = getNameFromPath($(element).val());

    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                flag = true;
                break;
            default:
                flag = false;
        }
    }

    if (flag == false) {
        debugger;
        var str = element.name;
        var res = str.split("_");
        var data = "_val" + res[1];
        $('#' + element.id).next().html('You can upload only jpg, jpeg, png extension file Only');
        imageStatus = flag;
        return false;
    }

    else {

        //this.files[0].size gets the size of your file.
        var fileUpload = $("#" + element.id)[0];
        //var fileUpload = element.files[0];
        //reader code for temp use//
        var reader = new FileReader();
        reader.readAsDataURL(fileUpload.files[0]);

        reader.onload = function (e) {
            //Initiate the JavaScript Image object.
            var image = new Image();
            //Set the Base64 string return from FileReader as source.
            image.src = e.target.result;
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                if (height != 400 && width != 400) {
                    $('#' + element.id).next().html('Image Height and Width must be 400*400 pixels.');
                    flag = false;
                    imageStatus = flag;
                    return flag;
                }
                else {
                    $('#' + element.id).next().html('');
                }
            };
            var size = (element.files[0].size / 1024);
            if (size > 400) {
                $('#' + element.id).next().html('You can upload file size up to 400 KB.');
                flag = false;
                imageStatus = flag;
                return flag;
            }
            else {
                $('#' + element.id).next().html('');
            }
        }
        flag = true;
        imageStatus = flag;
        return flag;
    }
}

//get file path from client system
function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}

function getOfferImageStatus() {

    return imageStatus;
}