﻿var promocodeStatus = false;

function validateOfferForm() {
    debugger;
    if(!promocodeStatus)
        isPromocodeExists();

    if (promocodeStatus && validateStartDate() && validateEndDate() && imageStatus) {
        return true;
    }
    else {
        return false;
    }
}


function isPromocodeExists() {
    debugger;
    var promocode = $('#Promocode').val();
    if (promocode !== '') {
        $.ajax({
            url: window.location.origin + '/WebApi/Offer/IsPromocodeExists',
            data: { 'promocode': promocode },
            type: "get",
            success: function (data) {
                if (data) {
                    $("#Promocode").next().html('Promocode is already Exist');
                    promocodeStatus = false;
                }
                else {
                    $("#Promocode").next().html('');
                    promocodeStatus = true;
                }
            },
            error: function (error) {
                alert('error' + error);
            }
        });
    }
    else {
        $('#Promocode').html('');
    }
}

// Subcategory View Binding
$("#MainCategoryId").change(function () {
    
    var maincategory_id = $('#MainCategoryId option:selected').val();
    if (maincategory_id !== '') {
        $('#subcategorydiv').html('');
        $.ajax({
            url: window.location.origin + '/WebApp/Offers/SubCategoryView',
            data: { 'MainCategoryId': maincategory_id },
            type: "get",
            success: function (data) {
                $('#subcategorydiv').html(data);
            },
            error: function (error) {
                alert('error' + error);
            }
        });
    }
    else {
        $('#subcategorydiv').html('');
    }
});

// Start date
function validateStartDate() {

    var dateVal = $('#StartDate').val();
    if (isDate(dateVal)) {
        $("#StartDate").next().html('');
        return true;
    }
    else {
        $("#StartDate").next().html('Please Select valid Date');
        return false;
    }
}

// End date
function validateEndDate() {

    var dateVal = $('#EndDate').val();
    if (isDate(dateVal)) {
        $("#EndDate").next().html('');
        return true;
    }
    else {
        $("#EndDate").next().html('Please Select valid Date');
        return false;
    }
    validateButton();
}

function removeValidationMessage(id) {
    $('#' + id).next().html('');
}


function isDate(dateVal) {
    var currVal = dateVal;
    if (currVal == '')
        return false;
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}
