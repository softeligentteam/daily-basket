﻿
function validateAndroidSliderImage(value, number) {
    
    var flag = false;
    var file = getNameFromPath($(value).val());

    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                flag = true;
                break;
            default:
                flag = false;
        }
    }

    if (flag == false) {
        
        var str = value.name;
        var res = str.split("_");
        var data = "_val" + res[1];
        $('#' + value.id).next().html('You can upload only jpg, jpeg, png extension file Only');
        $('#up' + number).show();
        $('#upip' + number).hide();
        $('#save' + number).hide();
        closebtn(number);
        return false;
    }

    else {

        //this.files[0].size gets the size of your file.
        var fileUpload = $("#" + value.id)[0];
        //var fileUpload = value.files[0];
        //reader code for temp use//
        var reader = new FileReader();
        reader.readAsDataURL(fileUpload.files[0]);

        reader.onload = function (e) {
            //Initiate the JavaScript Image object.
            var image = new Image();
            //Set the Base64 string return from FileReader as source.
            image.src = e.target.result;
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                
                if (height != 700 && width != 900) {
                    $('#' + value.id).next().html('Image Height and Width must be 900(w)*700(h) pixels.');
                    $('#up' + number).show();
                    $('#upip' + number).hide();
                    $('#save' + number).hide();
                    flag = false;
                    return flag;
                }
                else {
                    $('#' + value.id).next().html('');
                }
            };
            var size = (value.files[0].size / 1024);
            if (size > 500) {
                $('#' + value.id).next().html('You can upload file size up to 500 KB.');
                $('#up' + number).show();
                $('#upip' + number).hide();
                $('#save' + number).hide();
                flag = false;
                return flag;
            }
            else {
                $('#' + value.id).next().html('');
            }
        }
        dispSave(number);
        flag = true;
        return flag;
    }
}

//get file path from client system
function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}


var srcdefault = [];
function upBtnToggle(id) {
    //$('.up').slideUp();
    
    var shown = $('#up' + id).css('display');
    if (shown == 'none') { $('#up' + id).slideDown(); }
    else { $('#up' + id).slideUp(); }
}
function closebtn(id) {
    
    $('#img' + id).attr('src', srcdefault[id]);
    $('#img' + id).attr('alt', '');
    $('#close' + id).hide();
    $('#edit' + id).show();
    $('#save' + id).hide();
    $('#upip' + id).show();
    $('#upip' + id).val('');
    $('#errorspan' + id).html('');
}

function dispSave(id) {
    
    $('#edit' + id).hide();
    $('#save' + id).show();
    srcdefault[id] = $('#img' + id).attr('src');
    var prev = 'img' + id;
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById(prev);
        output.src = reader.result;
        $('#close' + id).show();

    }
    reader.readAsDataURL(event.target.files[0]);
    //var filename = $('#upip'+id).val('');
    //GetWebImage(filename);
}
