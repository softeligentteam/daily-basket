﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Areas.Stock.BLL;
using DailyBasket.Warehouse.Areas.Stock.Models.ViewModels;
using DailyBasket.Warehouse.Core.Interface.BLL.Stock;
using DailyBasket.Warehouse.Core.Model.Stock;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Stock.Controllers
{
    [Authorize]
    public class StockProductController : Controller
    {
        // GET: Sale/StockProduct
        IStockProduct istockProductBLL = null;
        public StockProductController()
        {
            istockProductBLL = new StockProductBLL();
        }

        public ActionResult Index()
        {
            return RedirectToAction("AvailableStocks");
        }

        public ActionResult AvailableStocks()
        {
            List<StockProductViewModel> stockProductVMList = istockProductBLL.GetProductsStock();
            return View("ProductsStock", stockProductVMList);
        }


        public ActionResult ExpiryWiseProductStock(ulong ProductId = 0)
        {
            if (ProductId != 0)
            {
                List<StockProductViewModel> stockProductVMList = istockProductBLL.GetExpiryWiseProductsStock(ProductId);
                return View("ExpiryWiseProductStock", stockProductVMList);
            }
            return RedirectToAction("Index");
        }

        public ActionResult BatchWiseProductStock(ulong ProductId = 0)
        {
            List<StockProductViewModel> stockProductVMList = null;
            if (ProductId != 0)
            {
                stockProductVMList = istockProductBLL.GetBatchWiseProductStock(ProductId);
            }
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return View(stockProductVMList);
        }

        public ActionResult ViewExpiryWiseConvetableProducts(ulong ProductId = 0)
        {
            List<ECStockProductViewModel> ecsStockProductVMList = null;
            ProductCategoryBLL productCategoryBLL = new ProductCategoryBLL();
            if (ProductId != 0)
            {
                ecsStockProductVMList = istockProductBLL.GetExpiryWiseConvertableProduct(ProductId);
            }
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return View(ecsStockProductVMList);
        }

        [HttpGet]
        public ActionResult MigrateProduct(ulong spid = 0, ulong pid = 0)
        {
            ConvertableProductModel convertableProductModel = istockProductBLL.GetStockProductToConvert(spid, pid);
            if (convertableProductModel != null)
            {
                //ViewBag.ProductDdlList = new ProductBLL().GetConvertableProductListDdl(mcid, scid, pid);
                ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
                return View(convertableProductModel);
            }
            return RedirectToAction("ViewExpiryWiseConvetableProducts");
        }

        [HttpPost]
        public ActionResult MigrateProduct(ConvertableProductModel convertableProductModel, string SelectProductId)
        {
            if (ModelState.IsValid)
            {
                var spId = istockProductBLL.MigrateStockProduct(convertableProductModel, SessionManager.CurrentUserId);
                if (spId != 0)
                    return RedirectToAction("ViewExpiryWiseConvetableProducts", new { ProductId = convertableProductModel.RefProductId });
                else
                    TempData["Message"] = "Oop's error occured! Try again";
            }
            ConvertableProductModel convertableProductModelbackup = istockProductBLL.GetStockProductToConvert(convertableProductModel.RefStockProductId, convertableProductModel.RefProductId);
            if (convertableProductModelbackup != null)
            {
                convertableProductModel.ProductName = convertableProductModelbackup.ProductName;
                convertableProductModel.MainCategoryName = convertableProductModelbackup.MainCategoryName;
                convertableProductModel.SubCategoryName = convertableProductModelbackup.SubCategoryName;
                convertableProductModel.Quantity = convertableProductModelbackup.Quantity;
                convertableProductModel.QuantityType = convertableProductModelbackup.QuantityType;
                convertableProductModel.AvailableStock = convertableProductModelbackup.AvailableStock;
                convertableProductModel.ExistRackNo = convertableProductModelbackup.ExistRackNo;
                convertableProductModel.PurchaseRate = convertableProductModelbackup.PurchaseRate;
                convertableProductModel.ExpiryDate = convertableProductModelbackup.ExpiryDate;

                //ViewBag.ProductDdlList = new ProductBLL().GetConvertableProductListDdl(0, 0, convertableProductModel.RefProductId);
                ViewBag.SelectedProductId = SelectProductId;
                ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
                return View(convertableProductModel);
            }
            return RedirectToAction("ViewExpiryWiseConvetableProducts", new { ProductId = convertableProductModel.RefProductId });
        }

        [HttpGet]
        public ActionResult UpdateProductSaleRate(ulong popId = 0, ulong spId = 0)
        {
            if (spId != 0)
            {
                StockProductSaleRateModel stockProductModel = istockProductBLL.GetBatchWiseProductSaleRateToUpdate(popId, spId);
                return View("UpdateBatchWiseProductSaleRate", stockProductModel);
            }
            return RedirectToAction("BatchWiseProductStock");
        }

        [HttpPost]
        public ActionResult UpdateProductSaleRate(StockProductSaleRateModel stockProductModel)
        {
            if (ModelState.IsValid)
            {
                if (istockProductBLL.UpdateBatchWiseProductSaleRate(stockProductModel, SessionManager.CurrentUserId) != 0)
                    return RedirectToAction("BatchWiseProductStock", new { ProductId = stockProductModel.ProductId});
                TempData["Message"] = "Oop's error occured! Try again";
            }
            StockProductSaleRateModel stockProductModelBackup = istockProductBLL.GetBatchWiseProductSaleRateToUpdate(stockProductModel.PopId, stockProductModel.SpId);
            if(stockProductModelBackup != null)
            {
                stockProductModel.ProductName = stockProductModelBackup.ProductName;
                stockProductModel.PurchaseRate = stockProductModelBackup.PurchaseRate;
                stockProductModel.AvailableStock = stockProductModelBackup.AvailableStock;
                stockProductModel.BatchNo = stockProductModelBackup.BatchNo;
                stockProductModel.ExpiryDate = stockProductModelBackup.ExpiryDate;
                stockProductModel.PackagingDate = stockProductModelBackup.PackagingDate;
                return View("UpdateBatchWiseProductSaleRate", stockProductModel);
            }
            return RedirectToAction("BatchWiseProductStock", new { ProductId = stockProductModel.ProductId });
        }

        public ActionResult ReorderProductsSystem(string startDate, string endDate, ulong brandId = 0)
        {
            List<ReorderProductViewModel> ReorderProductVMList = null;
            if (brandId != 0)
            {
                ReorderProductVMList = istockProductBLL.GetBrandWiseProductStock(brandId, startDate, endDate);
            }
            ViewBag.BrandDdlList = new BrandBLL().GetActiveBrandListDdl();
            return View(ReorderProductVMList);

        }
    }
}