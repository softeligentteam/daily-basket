﻿using DailyBasket.Warehouse.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Stock.Models.ViewModels
{
    public class ConvertableProductViewModel
    {
        public ulong StockProductId { get; set; }
        // Old Stock Id
        public ulong RefStockProductId { get; set; }

        public ulong ProductId { get; set; }
        
        public ulong BatchId { get; set; }
        

        public float Quantity { get; set; }
        public QuantityType QuantityType { get; set; }

        
        
        public string RackNo { get; set; }
    }
}