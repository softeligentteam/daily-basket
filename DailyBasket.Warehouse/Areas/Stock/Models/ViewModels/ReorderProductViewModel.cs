using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Stock.Models.ViewModels
{
    public class ReorderProductViewModel
    {
        public uint Id { get; set; }
        public string ProductName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public float SoldStock { get; set; }
        public float CurrentStock { get; set; }
        public float Order_Quantity { get; set; }
    }
}