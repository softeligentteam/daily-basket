﻿using DailyBasket.Warehouse.Core.Interface.BLL.Stock;
using System;
using System.Collections.Generic;
using System.Data;
using DailyBasket.Warehouse.Repository.Stock;
using DailyBasket.Warehouse.Areas.Stock.Models.ViewModels;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.Stock;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Helper;

namespace DailyBasket.Warehouse.Areas.Stock.BLL
{
    public class StockProductBLL : IStockProduct
    {
        Core.Interface.DAL.Stock.IStockProduct istockProductDAL;
        public StockProductBLL()
        {
            istockProductDAL = new StockProductRepository();
        }

        public dynamic GetBatchWiseProductStock(ulong productId)
        {
            DataTable dataTable = istockProductDAL.GetBatchWiseProductStock(productId);
            if (dataTable != null || dataTable.Rows.Count > 0)
            {
                List<StockProductViewModel> stockProductVMList = new List<StockProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    stockProductVMList.Add(new StockProductViewModel
                    {
                        SpId = Convert.ToUInt32(row["sp_id"]),
                        PopId = Convert.ToUInt32(row["po_prod_id"]),
                        ProductId = Convert.ToUInt32(row["prod_id"]),
                        ProductName = row["prod_name"].ToString(),
                        Quantity = row["quantity"].ToString(),
                        //QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString()),
                        OnlineSaleRate = row["online_sale_rate"].ToString(),
                        SaleRateA = row["sale_rate_a"].ToString(),
                        SaleRateB = row["sale_rate_b"].ToString(),
                        BatchNo = row["batch_no"].ToString()

                    });
                }
                return stockProductVMList;
            }

            return null;
        }

        public dynamic GetExpiryWiseConvertableProduct(ulong productId)
        {
            DataTable dataTable = istockProductDAL.GetExpiryWiseConvertableProduct(productId);
            if(dataTable != null)
            {
                List<ECStockProductViewModel> ecsStockProductVMList = new List<ECStockProductViewModel>();
                foreach(DataRow row in dataTable.Rows)
                {   
                    ECStockProductViewModel ecsStockProduct = new ECStockProductViewModel();
                    ecsStockProduct.StockProductId = Convert.ToUInt64(row["sp_id"].ToString());
                    ecsStockProduct.ProductId = Convert.ToUInt64(row["prod_id"].ToString());
                    ecsStockProduct.ProductName = row["prod_name"].ToString();
                    ecsStockProduct.AvailableQuantity = Convert.ToSingle(row["quantity"].ToString());
                    ecsStockProduct. QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString());
                    ecsStockProduct.BatchNo = row["batch_no"].ToString();
                    ecsStockProduct.ExpiryDate = row["expiry_date"].ToString();
                    ecsStockProduct.RackNo = row["rack_no"].ToString();
                    ecsStockProductVMList.Add(ecsStockProduct);
                }
                return ecsStockProductVMList;
            }
            return null;
        }

        public dynamic GetExpiryWiseProductsStock(ulong productId)
        {
            DataTable dataTable = istockProductDAL.GetExpiryWiseProductsStock(productId);
            if (dataTable != null)
            {
                List<StockProductViewModel> stockProductVMList = new List<StockProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {

                    stockProductVMList.Add(new StockProductViewModel
                    {
                        //Id = Convert.ToUInt32(row["sp_id"]),
                        ProductId = Convert.ToUInt32(row["prod_id"]),
                        ProductName = row["prod_name"].ToString(),
                        Quantity = row["quantity"].ToString(),
                        //QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString()),
                        //BatchNo = row["batch_no"].ToString(),
                        ExpiryDate = row["expiry_date"].ToString()
                        
                    });
                }
                return stockProductVMList;
            }

            return null;
        }

        public dynamic GetProductsStock()
        {
            DataTable dataTable = istockProductDAL.GetProductsStock();

            if (dataTable != null)
            {
                List<StockProductViewModel> stockProductVMList = new List<StockProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {

                    stockProductVMList.Add(new StockProductViewModel
                    {
                        ProductId = Convert.ToUInt64(row["prod_id"]),
                        ProductName = row["prod_name"].ToString(),
                        Quantity = row["available_stock"].ToString(),
                        QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString())
                    });
                }
                return stockProductVMList;
            }
            return null;
        }

        public ConvertableProductModel GetStockProductToConvert(ulong StockProductId, ulong ProductId)
        {
            DataTable dataTable = istockProductDAL.GetStockProductToConvert(StockProductId, ProductId);
            if (dataTable.Rows.Count > 0)
            {
                ConvertableProductModel convertableProductModel = new ConvertableProductModel();
                convertableProductModel.RefStockProductId = Convert.ToUInt64(dataTable.Rows[0]["sp_id"].ToString());
                convertableProductModel.RefProductId = Convert.ToUInt64(dataTable.Rows[0]["prod_id"].ToString());
                convertableProductModel.ProductName = dataTable.Rows[0]["prod_name"].ToString();
                convertableProductModel.Quantity = Convert.ToSingle(dataTable.Rows[0]["packaging_size"].ToString());
                convertableProductModel.QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), dataTable.Rows[0]["quantity_type"].ToString());
                convertableProductModel.MainCategoryName = dataTable.Rows[0]["main_category"].ToString();
                convertableProductModel.SubCategoryName = dataTable.Rows[0]["sub_category"].ToString();
                convertableProductModel.BatchId = Convert.ToUInt64(dataTable.Rows[0]["pb_id"].ToString());
                convertableProductModel.BatchNo = dataTable.Rows[0]["batch_no"].ToString();
                convertableProductModel.PackagingDate = dataTable.Rows[0]["packaging_date"].ToString();
                convertableProductModel.ExpiryDate = dataTable.Rows[0]["expiry_date"].ToString();
                convertableProductModel.AvailableStock = Convert.ToSingle(dataTable.Rows[0]["available_stock"].ToString());
                convertableProductModel.PurchaseRate = Convert.ToSingle(dataTable.Rows[0]["purchase_rate"].ToString());
                convertableProductModel.ExistRackNo = dataTable.Rows[0]["rack_no"].ToString();
                return convertableProductModel;
            }
            return null;
        }
        
        public ulong MigrateStockProduct(ConvertableProductModel convertableProductModel, ulong userId)
        {
            convertableProductModel.AutoGenerateBarcode = true;
            convertableProductModel.ProductBarcodeId = BarcodeHelper.AutoGenerateBarcode(convertableProductModel.ProductId);
            return istockProductDAL.MigrateStockProduct(convertableProductModel, userId);
        }

        public dynamic GetBatchWiseProductSaleRateToUpdate(ulong popId, ulong spId)
        {
            DataTable dataTable = istockProductDAL.GetBatchWiseProductSaleRateToUpdate(popId, spId);
            if (dataTable != null || dataTable.Rows.Count > 0)
            {
                StockProductSaleRateModel stockProductModel = new StockProductSaleRateModel();
                stockProductModel.SpId = Convert.ToUInt64(dataTable.Rows[0]["sp_id"].ToString());
                stockProductModel.PopId = Convert.ToUInt64(dataTable.Rows[0]["po_prod_id"].ToString());
                stockProductModel.ProductId = Convert.ToUInt64(dataTable.Rows[0]["prod_id"].ToString());
                stockProductModel.ProductName = dataTable.Rows[0]["prod_name"].ToString();
                stockProductModel.AvailableStock = Convert.ToSingle(dataTable.Rows[0]["available_stock"]);
                stockProductModel.PurchaseRate = Convert.ToSingle(dataTable.Rows[0]["purchase_rate"]);
                stockProductModel.MRP = Convert.ToSingle(dataTable.Rows[0]["mrp"]);
                stockProductModel.OnlineSaleRate = Convert.ToSingle(dataTable.Rows[0]["online_sale_rate"]);
                stockProductModel.SaleRateA = Convert.ToSingle(dataTable.Rows[0]["sale_rate_a"]);
                stockProductModel.SaleRateB = Convert.ToSingle(dataTable.Rows[0]["sale_rate_b"]);
                stockProductModel.BatchNo = dataTable.Rows[0]["batch_no"].ToString();
                stockProductModel.ExpiryDate = dataTable.Rows[0]["expiry_date"].ToString();
                stockProductModel.PackagingDate = dataTable.Rows[0]["packaging_date"].ToString();
                return stockProductModel;
            }
            else
            {
                return null;
            }
        }

        public int UpdateBatchWiseProductSaleRate(StockProductSaleRateModel stockProductModel, ulong userId)
        {
            return istockProductDAL.UpdateBatchWiseProductSaleRate(stockProductModel, userId);
        }

        public dynamic GetBrandWiseProductStock(ulong brandId, string startDate, string endDate)
        {
            DataTable dataTable = istockProductDAL.GetBrandWiseProductStock(brandId, startDate, endDate);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                List<ReorderProductViewModel> ReorderProductVM = new List<ReorderProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    ReorderProductVM.Add(new ReorderProductViewModel
                    {
                        ProductName = row["prod_name"].ToString(),
                        SoldStock = Convert.ToSingle(row["SoldStock"].ToString()),
                        CurrentStock = Convert.ToSingle(row["CurrentStock"].ToString()),
                        Order_Quantity = Math.Max(Convert.ToSingle(row["SoldStock"].ToString()) - Convert.ToSingle(row["CurrentStock"].ToString()), 0)
                    });
                }
                return ReorderProductVM;
            }
            return null;
        }
    }
}