﻿using DailyBasket.Warehouse.Core.Interface.BLL.Delivery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Repository.Delivery;
using System.Data;
using DailyBasket.Warehouse.Areas.Delivery.Models.ViewModels;
using DailyBasket.Warehouse.Core.Model.Delivery;

namespace DailyBasket.Warehouse.Areas.Delivery.BLL
{
    public class DeliveryBLL : IDelivery
    {
        Core.Interface.DAL.Delivery.IDelivery iDelivery;
        public DeliveryBLL()
        {
            iDelivery = new DeliveryRepository();
        }

        public ulong CreateDeliveryTrip(Trip tripModel, ulong userId)
        {
            return iDelivery.CreateDeliveryTrip(tripModel, userId);
        }

        public List<Trip> GetOnHoldDeliveryTrips()
        {
            DataTable dataTable = iDelivery.GetOnHoldDeliveryTrips();
            return BindTrips(dataTable);
        }

        public dynamic GetOnHoldDeliveryTripWithOrders(ulong tripId)
        {
            DataSet dataSet = iDelivery.GetOnHoldDeliveryTripWithOrders(tripId);
            return BindTripWithOrders(dataSet);
        }

        public dynamic GetOrdersToAssignDeliveryTrip()
        {
            DataTable dataTable = iDelivery.GetOrdersToAssignDeliveryTrip();
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                List<TripOrderViewModel> TripOrderVMList = new List<TripOrderViewModel>();
                foreach(DataRow record in dataTable.Rows)
                {
                    TripOrderViewModel tripOrderVM = new TripOrderViewModel();
                    tripOrderVM.OrderId = Convert.ToUInt64(record["corder_id"].ToString());
                    tripOrderVM.CustomerName = record["first_name"].ToString() + " " + record["last_name"].ToString();
                    tripOrderVM.Address = record["address"].ToString() + " " + record["locality"].ToString() + " " + record["city"].ToString();
                    tripOrderVM.OrderAmount = Convert.ToDouble(record["total_order_amount"].ToString());
                    TripOrderVMList.Add(tripOrderVM);
                }
                return TripOrderVMList;
            }
            return null;
        }

        public dynamic GetTripHistoryWithOrders(ulong tripId)
        {
            DataSet dataSet = iDelivery.GetTripHistoryWithOrders(tripId);
            return BindTripWithOrders(dataSet);
        }

        public List<Trip> GetTripsHistory()
        {
            DataTable dataTable = iDelivery.GetTripsHistory();
            return BindTrips(dataTable);
        }

        private List<Trip> BindTrips(DataTable dataTable)
        {
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                List<Trip> tripList = new List<Trip>();
                foreach (DataRow record in dataTable.Rows)
                {
                    Trip tripModel = new Trip();
                    tripModel.TripId = Convert.ToUInt32(record["trip_id"].ToString());
                    tripModel.TripDate = record["trip_date"].ToString();
                    tripModel.Vehicle = record["vehicle"].ToString();
                    tripModel.DeliveryBoyName = record["user_name"].ToString();
                    tripList.Add(tripModel);
                }
                return tripList;
            }
            return null;
        }

        private TripViewModel BindTripWithOrders(DataSet dataSet)
        {
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                TripViewModel tripViewModel = new TripViewModel();
                tripViewModel.TripId = Convert.ToUInt32(dataSet.Tables[0].Rows[0]["trip_id"].ToString());
                tripViewModel.TripDate = dataSet.Tables[0].Rows[0]["trip_date"].ToString();
                tripViewModel.DeliveryBoyName = dataSet.Tables[0].Rows[0]["user_name"].ToString();
                tripViewModel.Vehicle = dataSet.Tables[0].Rows[0]["vehicle"].ToString();

                if(dataSet.Tables[1] != null && dataSet.Tables[1].Rows.Count > 0)
                {
                    List<TripOrderViewModel> tripOrderVMList = new List<TripOrderViewModel>();

                    foreach (DataRow record in dataSet.Tables[1].Rows)
                    {
                        TripOrderViewModel tripOrderVM = new TripOrderViewModel();
                        tripOrderVM.OrderId = Convert.ToUInt64(record["corder_id"].ToString());
                        tripOrderVM.CustomerName = record["first_name"].ToString() + " " + record["middle_name"].ToString() + " " + record["last_name"].ToString();
                        tripOrderVM.Address = record["address"].ToString() + ", " + record["locality"].ToString() + ", " + record["city"].ToString();
                        tripOrderVM.OrderStatus = record["order_status"].ToString();
                        tripOrderVM.OrderAmount = Convert.ToDouble(record["total_order_amount"].ToString());
                        tripViewModel.GrandTotal = tripViewModel.GrandTotal + tripOrderVM.OrderAmount;
                        tripOrderVMList.Add(tripOrderVM);
                    }
                    tripViewModel.TripOrderList = tripOrderVMList;
                }
                return tripViewModel;
            }
            return null;
        }
    }
}