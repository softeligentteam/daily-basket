﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Delivery.Models.ViewModels
{
    public class TripViewModel
    {
        public ulong TripId { get; set; }
        public string TripDate { get; set; }
        public string DeliveryBoyName { get; set; }
        public string Vehicle { get; set; }
        public double GrandTotal { get; set; }
        public List<TripOrderViewModel> TripOrderList { get; set; }
    }
}