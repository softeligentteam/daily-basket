﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Delivery.Models.ViewModels
{
    public class TripOrderViewModel
    {
        public ulong OrderId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public double OrderAmount { get; set; }
        public string OrderStatus { get; set; }
    }
}