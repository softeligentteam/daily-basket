﻿using DailyBasket.Warehouse.Areas.Delivery.BLL;
using DailyBasket.Warehouse.Areas.Delivery.Models.ViewModels;
using DailyBasket.Warehouse.Areas.Users.BLL;
using DailyBasket.Warehouse.Core.Model.Delivery;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Delivery.Controllers
{
    public class TripController : Controller
    {
        Core.Interface.BLL.Delivery.IDelivery iDelivery;
        public TripController()
        {
            iDelivery = new DeliveryBLL();
        }

        // GET: Delivery/Trip
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateTrip()
        {
            BindTripData();
            return View();
        }

        [HttpPost]
        public ActionResult CreateTrip(Trip tripModel)
        {
            if (ModelState.IsValid)
            {
                if (iDelivery.CreateDeliveryTrip(tripModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Delivery Trip is created successfully.";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("CreateTrip");
                }
                else
                {
                    TempData["Message"] = "Delivery Trip is not created successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            BindTripData();
            return View(tripModel);
        }

        private void BindTripData()
        {
            ViewBag.DeliveryBoyDdlList = new UserHandlerBLL().GetWarehouseDeliveryBoyList();
            ViewBag.TripOrderViewModelList = iDelivery.GetOrdersToAssignDeliveryTrip();
        }

        public ActionResult OnHoldDeliveryTrips()
        {
            return View(iDelivery.GetOnHoldDeliveryTrips());
        }

        public ActionResult OnHoldDeliveryTripOrders(ulong tripId = 0)
        {
            if(tripId != 0)
            {
                TripViewModel tripViewModel = iDelivery.GetOnHoldDeliveryTripWithOrders(tripId);
                if (tripViewModel != null)
                    return View(tripViewModel);
            }
            return RedirectToAction("OnHoldDeliveryTrips");
        }

        public ActionResult TripsHistory()
        {
            return View(iDelivery.GetTripsHistory());
        }

        public ActionResult TripHistory(ulong tripId = 0)
        {
            if(tripId != 0)
            {
                TripViewModel tripViewModel = iDelivery.GetTripHistoryWithOrders(tripId);
                if (tripViewModel != null)
                    return View(tripViewModel);
            }
            return RedirectToAction("TripsHistory");
        }
    }
}