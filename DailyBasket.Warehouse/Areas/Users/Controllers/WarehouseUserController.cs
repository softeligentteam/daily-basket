﻿using DailyBasket.Warehouse.Areas.Users.BLL;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.Users;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Users.Controllers
{
    [Authorize(Roles = "Master")]
    public class WarehouseUserController : Controller
    {
        UserHandlerBLL warehouseUserBLL = new UserHandlerBLL();

        public ActionResult Index()
        {
            return RedirectToAction("WarehouseUserList");
        }

        public ActionResult WarehouseUserList()
        {
            return View("ViewWarehouseUsers", warehouseUserBLL.GetWarehouseUserList());
            
        }

        [HttpGet]
        public ActionResult RegisterWarehouseUser()
        {
            BindUserTypeDropdownList();
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterWarehouseUser(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                if (warehouseUserBLL.RegisterWarehouseUser(userModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Warehouse User is added successfully.";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("RegisterWarehouseUser");
                }
                else
                {
                    TempData["Message"] = "Warehouse User is not added successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            BindUserTypeDropdownList();
            return View(userModel);
        }
        private void BindUserTypeDropdownList()
        {
            ViewBag.UserTypeDdlList = warehouseUserBLL.GetActiveUserTypeDdlList();
            ViewBag.GenderDdlList = EnumDropdownlistHelper.GetEnumGenderDdlList();
        }

        [HttpGet]
        public ActionResult Update(ulong userId = 0)
        {
            if (userId != 0)
            {
                UserModel userModel = warehouseUserBLL.GetWarehouseUserToUpdate(userId);
                if (userModel != null)
                {
                    BindUserTypeDropdownList();
                    return View("UpdateWarehouseUser", userModel);
                }
            }
            return RedirectToAction("WarehouseUserList");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                if (warehouseUserBLL.UpdateWarehouseUser(userModel, SessionManager.CurrentUserId) != 0)
                {
                    return RedirectToAction("WarehouseUserList");
                }
                else
                {
                    TempData["Message"] = "Warehouse User is not updated successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            BindUserTypeDropdownList();
            return View("UpdateWarehouseUser", userModel);
        }

        [HttpGet]
        public ActionResult RegisterWarehouseFranchise()
        {
            //BindUserTypeDropdownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterWarehouseFranchisee(FranchiseeModel franchiseeModel)
        {
            if (ModelState.IsValid)
            {
                if (franchiseeModel != null)
                {
                    if (warehouseUserBLL.RegisterWarehouseFranchisee(franchiseeModel, SessionManager.CurrentUserId) != 0)
                    {
                        TempData["Message"] = "Franchisee added successfully";
                        TempData["CssClass"] = "text-success";
                        return RedirectToAction("RegisterWarehouseFranchise");
                    }
                    else
                    {
                        TempData["Message"] = "Franchisee is not added successfully, Please try again!";
                        TempData["CssClass"] = "text-danger";
                        return RedirectToAction("RegisterWarehouseFranchise");
                    }
                }
            }
            return View("RegisterWarehouseFranchise", franchiseeModel);
        }

        public ActionResult FranchiseeList()
        {
            return View("ViewFranchisees", warehouseUserBLL.GetFranchiseesList());

        }

        [HttpGet]
        public ActionResult UpdateFranchisee(ulong franchiseeId = 0)
        {
            if (franchiseeId != 0)
            {
                FranchiseeModel franchiseeModel = warehouseUserBLL.GetFranchiseeDetailsToUpdate(franchiseeId);
                if (franchiseeModel != null)
                {
                    return View("UpdateFranchisee", franchiseeModel);
                }
            }
            return RedirectToAction("FranchiseeList");
        }

        [HttpPost]
        public ActionResult UpdateFranchisee(FranchiseeModel franchiseeModel)
        {
            if (franchiseeModel != null)
            {
              
                if (warehouseUserBLL.UpdateFranchisee(franchiseeModel, SessionManager.CurrentUserId) != 0)
                {
                    return RedirectToAction("FranchiseeList");
                }
                else
                {
                    TempData["Message"] = "Franchisee details not updated successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                    return RedirectToAction("UpdateFranchisee", new { franchiseeId = franchiseeModel.Id });
                }
            }
            return RedirectToAction("FranchiseeList");
        }
    }
}