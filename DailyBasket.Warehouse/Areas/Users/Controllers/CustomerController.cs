﻿using DailyBasket.Warehouse.Areas.Users.BLL;
using DailyBasket.Warehouse.Areas.Users.Models.ViewModels;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Interface.BLL.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Users.Controllers
{
    [Authorize(Roles = "Master")]
    public class CustomerController : Controller
    {
        ICustomerHandler iCustomerBLL = new CustomerHandlerBLL();
            // GET: Users/Customer
        public ActionResult Index()
        {
            return RedirectToAction("ActiveCustomers");
        }

        public ActionResult ActiveCustomers()
        {
            return View("CustomerList", iCustomerBLL.GetCustomerList(Status.Active));
        }
    }
}