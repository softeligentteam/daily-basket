﻿using DailyBasket.Warehouse.Core.Interface.BLL.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.Login;
using DailyBasket.Warehouse.Core.Model.Users;
using DailyBasket.Warehouse.Repository.User;
using DailyBasket.Warehouse.Core.Interface.BLL.User;
using System.Data;
using System.Web.Mvc;
using DailyBasket.Warehouse.Helper;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Core.Helper;
using DailyBasket.Warehouse.Areas.Users.Models.ViewModels;

namespace DailyBasket.Warehouse.Areas.Users.BLL
{
    public class UserHandlerBLL : IUserHandler
    {
        Core.Interface.DAL.User.IUserHandler iUserHandlerRepository;
        public UserHandlerBLL()
        {
            iUserHandlerRepository = new UserHandlerRepository();
        }
        public int RegisterWarehouseUser(UserModel userModel, ulong userId)
        {
            userModel.Password = CryptographyHelper.Encrypt(userModel.Password);
            return iUserHandlerRepository.RegisterWarehouseUser(userModel, userId);
        }
        
        public SelectList GetActiveUserTypeDdlList()
        {
            return DropdownlistHelper.GetIntStringDropDownList(iUserHandlerRepository.GetActiveUserTypeDdlList());
        }

        public dynamic GetWarehouseUserList()
        {
            DataTable dataTable = iUserHandlerRepository.GetWarehouseUserList();
            if (dataTable != null)
            {
                List<WarehouseUserViewModel> warehouseuserModelList = new List<WarehouseUserViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    WarehouseUserViewModel warehouseuserVM = new WarehouseUserViewModel();
                    warehouseuserVM.Id = Convert.ToUInt64(row["user_id"].ToString());
                    warehouseuserVM.UserType= row["user_type"].ToString();
                    warehouseuserVM.Name = row["user_name"].ToString();
                    warehouseuserVM.Address= row["address"].ToString();
                    warehouseuserVM.Mobile = Convert.ToUInt64(row["mobile"]);
                    warehouseuserVM.Email= row["email"].ToString();
                    
                    warehouseuserModelList.Add(warehouseuserVM);
                }
                return warehouseuserModelList;
            }
            return null;
        }

        public UserModel GetWarehouseUserToUpdate(ulong userId)
        {
            DataTable dataTable = iUserHandlerRepository.GetWarehouseUserToUpdate(userId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                UserModel userModel = new UserModel();
                userModel.Id = Convert.ToUInt64(dataTable.Rows[0]["user_id"].ToString());
                userModel.UserType = Convert.ToUInt32(dataTable.Rows[0]["utype_id"].ToString());
                userModel.Name = dataTable.Rows[0]["user_name"].ToString();
                userModel.Address = dataTable.Rows[0]["address"].ToString();
                userModel.DateOfBirth = dataTable.Rows[0]["dob"].ToString();
                userModel.Gender = dataTable.Rows[0]["gender"].ToString();
                userModel.Mobile = Convert.ToUInt64(dataTable.Rows[0]["mobile"].ToString());
                userModel.Email = dataTable.Rows[0]["email"].ToString();
                userModel.AltMobile = Convert.ToUInt64(dataTable.Rows[0]["alt_mobile"].ToString());
                userModel.Status = Convert.ToByte(dataTable.Rows[0]["status"].ToString());
                return userModel;
            }
            return null;
        }

        public int UpdateWarehouseUser(UserModel userModel, ulong userId)
        {
            if(userModel.Password != null)
                userModel.Password = CryptographyHelper.Encrypt(userModel.Password);
            return iUserHandlerRepository.UpdateWarehouseUser(userModel, userId); 
        }

        public SelectList GetWarehouseDeliveryBoyList()
        {
            return DropdownlistHelper.GetLongStringDropDownList(iUserHandlerRepository.GetWarehouseDeliveryBoyList(Core.Enum.UserType.Delivery));
        }

        public int RegisterWarehouseFranchisee(FranchiseeModel franchiseeModel, ulong userId)
        {
            franchiseeModel.Password = CryptographyHelper.Encrypt(franchiseeModel.Password);
            return iUserHandlerRepository.RegisterWarehouseFranchisee(franchiseeModel, userId);
        }

         public dynamic GetFranchiseesList()
        {
            DataTable dataTable = iUserHandlerRepository.GetFranchiseesList();
            if (dataTable != null)
            {
                List<FranchiseeViewModel> franchiseeVMList = new List<FranchiseeViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    FranchiseeViewModel franchiseeVM = new FranchiseeViewModel();
                    franchiseeVM.Id = Convert.ToUInt64(row["fuser_id"].ToString());
                    franchiseeVM.CompanyName = row["company_name"].ToString();
                    franchiseeVM.FranchiseName = row["franchisee_name"].ToString();
                    franchiseeVM.OwnerName = row["owner_name"].ToString();
                    //FranchiseeVM.Phone = row["phone"].ToString();
                    franchiseeVM.Mobile = Convert.ToUInt64(row["mobile"]);
                    //FranchiseeVM.AlteMobile = Convert.ToUInt64(row["alt_mobile"].ToString());
                    //FranchiseeVM.OfficialEmailId = row["official_email_id"].ToString();
                    franchiseeVM.Address = row["address"].ToString();
                    //FranchiseeVM.State = row["state"].ToString();
                    //FranchiseeVM.City = row["city"].ToString();
                    //FranchiseeVM.Pincode = Convert.ToInt32(row["pincode"]);
                    //FranchiseeVM.ContactPersonName = row["cp_name"].ToString();
                    //FranchiseeVM.CPMobileNo = Convert.ToUInt64(row["cp_mobile"].ToString());
                    //FranchiseeVM.CPEmail = row["cp_email"].ToString();
                    //FranchiseeVM.Password = row["password"].ToString();
                    //FranchiseeVM.GSTNO = row["email"].ToString();

                    franchiseeVMList.Add(franchiseeVM);
                }
                return franchiseeVMList;
            }
            return null;
        }

        public dynamic GetFranchiseeDetailsToUpdate(ulong frachiseeId)
        {
            DataTable dataTable = iUserHandlerRepository.GetFranchiseeDetailsToUpdate(frachiseeId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                    FranchiseeModel franchiseeModel = new FranchiseeModel();
                    franchiseeModel.Id = Convert.ToUInt64(dataTable.Rows[0]["fuser_id"].ToString());
                    franchiseeModel.CompanyName = dataTable.Rows[0]["company_name"].ToString();
                    franchiseeModel.FranchiseName = dataTable.Rows[0]["franchisee_name"].ToString();
                    franchiseeModel.OwnerName = dataTable.Rows[0]["owner_name"].ToString();
                    franchiseeModel.Phone = dataTable.Rows[0]["phone"].ToString();
                    franchiseeModel.Mobile = Convert.ToUInt64(dataTable.Rows[0]["mobile"]);
                    franchiseeModel.AlteMobile = Convert.ToUInt64(dataTable.Rows[0]["alt_mobile"].ToString());
                    franchiseeModel.OfficialEmailId = dataTable.Rows[0]["official_email_id"].ToString();
                    franchiseeModel.Address = dataTable.Rows[0]["address"].ToString();
                    franchiseeModel.State = dataTable.Rows[0]["state"].ToString();
                    franchiseeModel.City = dataTable.Rows[0]["city"].ToString();
                    franchiseeModel.Pincode = Convert.ToInt32(dataTable.Rows[0]["pincode"]);
                    franchiseeModel.ContactPersonName = dataTable.Rows[0]["cp_name"].ToString();
                    franchiseeModel.CPMobileNo = Convert.ToUInt64(dataTable.Rows[0]["cp_mobile"].ToString());
                    franchiseeModel.CPEmail = dataTable.Rows[0]["cp_email"].ToString();
                    //franchiseeModel.Password = dataTable.Rows[0]["password"].ToString();
                    franchiseeModel.GSTNO = dataTable.Rows[0]["gstin"].ToString();
                    return franchiseeModel;
            }
            return null;
        }

        public int UpdateFranchisee(FranchiseeModel franchiseeModel, ulong userId)
        {
            franchiseeModel.Password = CryptographyHelper.Encrypt(franchiseeModel.Password);
            return iUserHandlerRepository.UpdateFranchisee(franchiseeModel, userId);
        }
    }
}