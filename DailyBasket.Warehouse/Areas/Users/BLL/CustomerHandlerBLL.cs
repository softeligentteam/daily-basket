﻿using DailyBasket.Warehouse.Core.Interface.BLL.User;
using DailyBasket.Warehouse.Repository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Enum;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Areas.Users.Models.ViewModels;
using System.Data;

namespace DailyBasket.Warehouse.Areas.Users.BLL
{
    public class CustomerHandlerBLL : ICustomerHandler
    {
        Core.Interface.DAL.User.ICustomerHandler iCustomerHandlerRepository;
        public CustomerHandlerBLL()
        {
            iCustomerHandlerRepository = new CustomerHandlerRepository();
        }
        
        public dynamic GetCustomerList(Status status)
        {
            DataTable dataTable = iCustomerHandlerRepository.GetCustomerList(status);
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                List<CustomerViewModel> customerMVList = new List<CustomerViewModel>();
                foreach(DataRow record in dataTable.Rows)
                {
                    CustomerViewModel customer = new CustomerViewModel();
                    customer.Id = Convert.ToUInt64(record["cust_id"].ToString());
                    customer.Name = record["first_name"].ToString()+" "+record["middle_name"].ToString()+" "+ record["last_name"].ToString();
                    customer.Mobile = Convert.ToUInt64(record["mobile"].ToString());
                    customer.Email = record["email"].ToString();
                    customer.ReferralCode = record["referral_code"].ToString();
                    customer.RefReferralCode = record["referred_referral_code"].ToString();
                    customer.GSTIN = record["gstin"].ToString();
                    customer.LoyaltyPoints = record["loyalty_points"].ToString();
                    customerMVList.Add(customer);
                }
                return customerMVList;
            }
            return null;
        }
    }
}