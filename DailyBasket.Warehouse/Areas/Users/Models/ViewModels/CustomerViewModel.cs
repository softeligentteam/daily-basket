﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Users.Models.ViewModels
{
    public class CustomerViewModel
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public ulong Mobile { get; set; }
        public string Email { get; set; }
        public string CustomerType { get; set; }
        public string ReferralCode { get; set; }
        public string RefReferralCode { get; set; }
        public string GSTIN { get; set; }
        public string LoyaltyPoints { get; set; }
        public byte Status { get; set; }
    }
}