﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Users.Models.ViewModels
{
    public class WarehouseUserViewModel
    {
        public ulong Id { get; set; }
        public string UserType { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public ulong Mobile { get; set; }
        public string Email { get; set; }
    }
}