﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Users.Models.ViewModels
{
    public class FranchiseeViewModel
    {
        public ulong Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Franchise Name")]
        public string FranchiseName { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Owner Name")]
        public string OwnerName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "State")]
        public string State { get; set; }

        [StringLength(15)]
        [Display(Name = "Phone No.")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        public ulong? Mobile { get; set; }

        [Display(Name = "Alternate Mobile")]
        public ulong? AlteMobile { get; set; }

        [Required]
        [Display(Name = "Pincode")]
        public int? Pincode { get; set; }

        [StringLength(45)]
        [Display(Name = "Official email Id")]
        public string OfficialEmailId { get; set; }

        [Required]
        [StringLength(15)]
        [Display(Name = "GST Identification Number")]
        public string GSTNO { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Contact Person Name")]
        public string ContactPersonName { get; set; }

        [Required]
        [Display(Name = "Contact Mobile No.")]
        public ulong? CPMobileNo { get; set; }

        [StringLength(45)]
        [Display(Name = "Contact Email Id")]
        public string CPEmail { get; set; }

        [MinLength(6, ErrorMessage = "Password length must be between 6 to 15 characters")]
        [MaxLength(15, ErrorMessage = "Password length must be between 6 to 15 characters")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[Required]
        [MinLength(6)]
        [MaxLength(15)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }
}