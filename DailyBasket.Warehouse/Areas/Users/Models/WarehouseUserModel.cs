﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Users.Models
{
    public class WarehouseUserModel
    {
        public uint? Id { get; set; }

        [Required] 
        [Display(Name = "User Type")]
        public uint UserType { get; set; }

        [Required]
        [Display(Name = "User Name")]
        [StringLength(30)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(15)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(15)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set;}

        [Required]
        [Display(Name = "Date Of Birth")]
        public string DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Mobile No")]
        public ulong Mobile { get; set; }

        [Display(Name = "Alternate Mobile No")]
        public ulong AltMobile { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(50)]
        public string Address { get; set; }

    }
}