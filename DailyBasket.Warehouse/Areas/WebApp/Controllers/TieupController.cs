using DailyBasket.Warehouse.Areas.WebApp.BLL;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.WebApp;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.WebApp.Controllers
{
    public class TieupController : Controller
    {
        Core.Interface.BLL.WebApp.ITieup iTieup;
        public TieupController()
        {
            iTieup = new TieupBLL();
        }
        // GET: WebApp/Tieup
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UpdateTieupSlider()
        {
            return View(iTieup.GetTieupImagesToUpdate());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateTieupSlider(TieupModel tieupModel, HttpPostedFileBase TieupImage)
        {
            if (ModelState.IsValid)
            {
                tieupModel.TieupImage = ImageUploadHelper.UploadTieupSliderImage(TieupImage);
                int status = iTieup.UpdateTieupImage(tieupModel, SessionManager.CurrentUserId);
                if (status != 0)
                {
                    ImageUploadHelper.DeleteImage(tieupModel.TieupImagePath);
                    TempData["Message"] = "Tieup Image is added successfully";
                    TempData["cssClass"] = "text-success";
                    return RedirectToAction("UpdateTieupSlider");
                }
                else
                {
                    TempData["Message"] = "Tieup image is not added. Try again.";
                    TempData["cssClass"] = "text-danger";
                }
            }
            return View(iTieup.GetTieupImagesToUpdate());
        }

        public ActionResult UpdateTieupSliderStatus(uint sliderId = 0, uint status = 0)
        {
            if (sliderId != 0)
            {
                if (status == (uint)Status.Active)
                    status = (uint)Status.Inactive;
                else
                    status = (uint)Status.Active;
                iTieup.UpdateTieupStatus(sliderId, status, SessionManager.CurrentUserId);
            }
            return RedirectToAction("UpdateTieupSlider");
        }
    }
}