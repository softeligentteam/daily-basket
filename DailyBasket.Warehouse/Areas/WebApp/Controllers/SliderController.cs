﻿using DailyBasket.Warehouse.Areas.WebApp.BLL;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.WebApp;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.WebApp.Controllers
{
    public class SliderController : Controller
    {
        // GET: WebApp/Slider
        public ActionResult Index()
        {
            return View();
        }

        Core.Interface.BLL.WebApp.ISlider iSlider;
        public SliderController()
        {
            iSlider = new SliderBLL();
        }
        // GET: WebApp/Slider
        [HttpGet]
        public ActionResult UpdateWebSlider()
        {
            return View(iSlider.GetWebSliderImagesToUpdate());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateWebSlider(SliderModel SliderModel, HttpPostedFileBase SliderImage)
        {
            if (ModelState.IsValid)
            {
                SliderModel.SliderImage = ImageUploadHelper.UploadWebSliderImage(SliderImage);
                int status = iSlider.UpdateWebSliderImage(SliderModel, SessionManager.CurrentUserId);
                if (status != 0)
                {
                    ImageUploadHelper.DeleteImage(SliderModel.SliderImagePath);
                    TempData["Message"] = "Slider Image is added successfully";
                    TempData["cssClass"] = "text-success";
                    return RedirectToAction("UpdateWebSlider");
                }
                else
                {
                    TempData["Message"] = "Slider image is not added. Try again.";
                    TempData["cssClass"] = "text-danger";
                }
            }
            return View(iSlider.GetWebSliderImagesToUpdate());
        }

        [HttpGet]
        public ActionResult UpdateAndroidSlider()
        {
            return View(iSlider.GetAndroidSliderImagesToUpdate());
        }

        [HttpPost]
        public ActionResult UpdateAndroidSlider(SliderModel SliderModel, HttpPostedFileBase SliderImage)
        {
            if (ModelState.IsValid)
            {
                SliderModel.SliderImage = ImageUploadHelper.UploadAndroidSliderImage(SliderImage);
                int status = iSlider.UpdateAndroidSliderImage(SliderModel, SessionManager.CurrentUserId);
                if (status != 0)
                {
                    ImageUploadHelper.DeleteImage(SliderModel.SliderImagePath);
                    TempData["Message"] = "Slider Image is added successfully";
                    TempData["cssClass"] = "text-success";
                    return RedirectToAction("UpdateAndroidSlider");
                }
                else
                {
                    TempData["Message"] = "Slider image is not added. Try again.";
                    TempData["cssClass"] = "text-danger";
                }
            }
            return View(iSlider.GetAndroidSliderImagesToUpdate());
        }

        public ActionResult UpdateAndroidSliderStatus(uint sliderId = 0, uint status = 0)
        {
            if(sliderId != 0)
            {
                if(status == (uint)Status.Active)
                    status = (uint)Status.Inactive;
                else
                    status = (uint)Status.Active;
                iSlider.UpdateSliderStatus(sliderId, status, SessionManager.CurrentUserId);
            }
            return RedirectToAction("UpdateAndroidSlider");
        }

        public ActionResult UpdateWebSliderStatus(uint sliderId = 0, uint status = 0)
        {
            if (sliderId != 0)
            {
                if (status == (uint)Status.Active)
                    status = (uint)Status.Inactive;
                else
                    status = (uint)Status.Active;
                iSlider.UpdateSliderStatus(sliderId, status, SessionManager.CurrentUserId);
            }
            return RedirectToAction("UpdateWebSlider");
        }
    }
}