﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Areas.WebApp.BLL;
using DailyBasket.Warehouse.Core.Interface.BLL.WebApp;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Core.Model.WebApp;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.WebApp.Controllers
{
    [Authorize(Roles = "Master")]
    public class OffersController : Controller
    {
        IOffer iOffer = new OfferBLL();
        // GET: WebApp/Offers
        public ActionResult Index()
        {
            return RedirectToAction("ActiveOffers");
        }

        [HttpGet]
        public ActionResult AddOffer()
        {
            BindOffersDropDownList();
            return View();
        }

        [HttpPost]
        public ActionResult AddOffer(OfferModel offerModel, HttpPostedFileBase Image)
        {
            if(ModelState.IsValid)
            {
                offerModel.Image = ImageUploadHelper.UploadOfferImage(Image);
                if (iOffer.AddOffer(offerModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Offer is added successfully";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("AddOffer");
                }
                TempData["Message"] = "Oops error Occured, Please try again!";
                TempData["CssClass"] = "text-danger";
            }
            BindOffersDropDownList();
            return View(offerModel);
        }
        
        public ActionResult ActiveOffers()
        {
            return View(iOffer.GetOfferList(Core.Enum.Status.Active));
        }

        public ActionResult UpdateOfferStatus(uint offerId)
        {
            iOffer.UpdateOfferStatus(offerId, Core.Enum.Status.Inactive, SessionManager.CurrentUserId);
            return RedirectToAction("ActiveOffers");
        }

        private void BindOffersDropDownList()
        {
            ViewBag.OfferTypeDdlList = iOffer.GetOfferTypesDdl();
            ViewBag.SubCategoryList = new ProductCategoryBLL().GetActiveSubCategoryModelList();
        }
    }
}