﻿using DailyBasket.Warehouse.Core.Interface.BLL.WebApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.WebApp;
using System.Web.Mvc;
using DailyBasket.Warehouse.Helper;
using DailyBasket.Warehouse.Repository.WebApp;
using System.Data;

namespace DailyBasket.Warehouse.Areas.WebApp.BLL
{
    public class OfferBLL : IOffer
    {
        Core.Interface.DAL.WebApp.IOffer iOffer;
        public OfferBLL()
        {
            iOffer = new OfferRepository();
        }
        public int AddOffer(OfferModel offerModel, ulong userId)
        {
            return iOffer.AddOffer(offerModel, userId);
        }

        public List<OfferModel> GetOfferList(Status status)
        {
            DataTable dataTable = iOffer.GetOfferList(status);
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                List<OfferModel> offerList = new List<OfferModel>();
                foreach(DataRow record in dataTable.Rows)
                {
                    OfferModel offerModel = new OfferModel();
                    offerModel.OfferId = Convert.ToUInt32(record["coffer_id"].ToString());
                    offerModel.OfferType = record["offer_type"].ToString();
                    offerModel.Promocode = record["promocode"].ToString();
                    offerModel.ApplicableAmount = Convert.ToDouble(record["min_applicable_amount"].ToString());
                    offerModel.OfferAmount = Convert.ToSingle(record["offer_amount"].ToString());
                    offerModel.StartDate = record["start_date"].ToString();
                    offerModel.EndDate = record["end_date"].ToString();
                    //offerModel.Status = Convert.ToUInt16(record["status"].ToString());
                    offerList.Add(offerModel);
                }
                return offerList;
            }
            return null;
        }

        public OfferModel GetOfferToUpdate(uint offerId)
        {
            throw new NotImplementedException();
        }

        public SelectList GetOfferTypesDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(iOffer.GetOfferTypesDdl());
        }

        public int UpdateOfferStatus(uint offerId, Status status, ulong userId)
        {
            return iOffer.UpdateOfferStatus(offerId, status, userId);
        }
    }
}