﻿using DailyBasket.Warehouse.Core.Interface.BLL.WebApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.WebApp;
using DailyBasket.Warehouse.Repository.WebApp;
using DailyBasket.Warehouse.Core.Enum;
using System.Data;

namespace DailyBasket.Warehouse.Areas.WebApp.BLL
{
    public class SliderBLL : ISlider
    {
        Core.Interface.DAL.WebApp.ISlider iSlider;
        public SliderBLL()
        {
            iSlider = new SliderRepository();
        }

        public List<SliderModel> GetAndroidSliderImagesToUpdate()
        {
            DataTable dataTable = iSlider.GetSliderImagesToUpdate(SliderType.Android);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                List<SliderModel> sliderList = new List<SliderModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    sliderList.Add(new SliderModel { SliderImageId = Convert.ToUInt32(row["slider_img_id"].ToString()), SliderImage = row["slider_image"].ToString(), Status = Convert.ToUInt16(row["status"]) });
                }
                return sliderList;
            }
            return null;
        }

        public List<SliderModel> GetWebSliderImagesToUpdate()
        {
            DataTable dataTable = iSlider.GetSliderImagesToUpdate(SliderType.Web);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                List<SliderModel> sliderList = new List<SliderModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    sliderList.Add(new SliderModel { SliderImageId = Convert.ToUInt32(row["slider_img_id"].ToString()), SliderImage = row["slider_image"].ToString(), Status = Convert.ToUInt16(row["status"]) });
                }
                return sliderList;

            }
            return null;
        }

        public int UpdateAndroidSliderImage(SliderModel sliderModel, ulong userId)
        {
            sliderModel.sliderType = SliderType.Android.ToString();
            return iSlider.UpdateSliderImage(sliderModel, userId);
        }

        public int UpdateWebSliderImage(SliderModel sliderModel, ulong userId)
        {
            sliderModel.sliderType = SliderType.Web.ToString();
            int status = iSlider.UpdateSliderImage(sliderModel, userId);
            return status;
        }

        public int UpdateSliderStatus(uint sliderId, uint status, ulong userId)
        {
            return iSlider.UpdateSliderStatus(sliderId, status, userId);
        }
    }
}