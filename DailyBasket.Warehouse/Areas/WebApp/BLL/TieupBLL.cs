using DailyBasket.Warehouse.Core.Interface.BLL.WebApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.WebApp;
using DailyBasket.Warehouse.Repository.WebApp;
using System.Data;

namespace DailyBasket.Warehouse.Areas.WebApp.BLL
{
    public class TieupBLL : ITieup
    {
        Core.Interface.DAL.WebApp.ITieup iTieup;
        public TieupBLL()
        {
            iTieup = new TieupRepository();
        }

        public List<TieupModel> GetTieupImagesToUpdate()
        {
            DataTable dataTable = iTieup.GetTieupImagesToUpdate();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                List<TieupModel> sliderList = new List<TieupModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    sliderList.Add(new TieupModel { TieupId = Convert.ToUInt32(row["tieup_id"].ToString()), TieupImage= row["tieup_image"].ToString(), Status = Convert.ToUInt16(row["status"]) });
                }
                return sliderList;

            }
            return null;
        }

        public int UpdateTieupImage(TieupModel tieupModel, ulong userId)
        {
            int status = iTieup.UpdateTieupImage(tieupModel, userId);
            return status;
        }

        public int UpdateTieupStatus(uint tieupId, uint status, ulong userId)
        {
            return iTieup.UpdateTieupStatus(tieupId, status, userId);
        }
    }
}