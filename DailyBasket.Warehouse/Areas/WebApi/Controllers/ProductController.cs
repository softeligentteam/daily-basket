﻿using DailyBasket.Warehouse.Areas.Purchase.Models.ViewModels;
using DailyBasket.Warehouse.Areas.Stock.Models.ViewModels;
using DailyBasket.Warehouse.Areas.WebApi.BLL;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class ProductController : Controller
    {
        [WebMethod]
        public JsonResult GetProductsDdlListByMSCat(uint MainCategoryId=0, uint SubCategoryId=0)
        {
            List<LongStringDdlModel> productList = null;
            if (MainCategoryId != 0 && SubCategoryId != 0)
            {
                productList = new ProductMasterBLL().GetProductsDdlList(MainCategoryId, SubCategoryId);
            }
            return Json(productList, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult GetSubCategoryDdlList(uint MainCategoryId=0)
        {
            List<IntStringDdlModel> subCategoryList = null;
            if (MainCategoryId != 0)
            {
                subCategoryList = new ProductMasterBLL().GetSubCategoryDdlList(MainCategoryId);
            }
            return Json(subCategoryList, JsonRequestBehavior.AllowGet);
        }
        
        [WebMethod]
        public JsonResult IsProductNameExists(string productName)
        {
            bool status = new ProductMasterBLL().IsProductNameExists(productName);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsMainCategoryNameExists(string mainCategory)
        {
            bool status = new ProductMasterBLL().IsProductMainCategoryExists(mainCategory);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsSubCategoryNameExists(string subCategory)
        {
            bool status = new ProductMasterBLL().IsProductSubCategoryExists(subCategory);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsBrandNameExists(string brandName)
        {
            bool status = new ProductMasterBLL().IsProductBrandNameExists(brandName);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult ProductSaleRate(ulong productId)
        {
            ProductSaleRateViewModel productSaleRateVM = null;
            if (productId != 0)
            {
                productSaleRateVM = new ProductMasterBLL().GetProductSaleRate(productId);
            }
            return Json(productSaleRateVM, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsProductHsnNumberExists(string productHsnNumber)
        {
            bool status = new ProductMasterBLL().IsProductHsnNumberExists(productHsnNumber);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult GetConvertableProductPackagingSizeAndSaleRate(ulong ProductId=0)
        {
            Dictionary<string, object> productData = new Dictionary<string, object>();
            //productData.Add("PackagingSize", new ProductMasterBLL().GetConvertableProductPackagingSize(ProductId));
            productData.Add("SaleRates", new StockProductBLL().GetStockProductSaleRate(ProductId));
            return Json(productData, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]

        public JsonResult IsStockAvailable(ulong productId = 0)
        {
            float stock  = new ProductMasterBLL().IsProductStockAvailable(productId);
            return Json(stock, JsonRequestBehavior.AllowGet);
        }
    }
}
