﻿using DailyBasket.Warehouse.Areas.WebApi.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class AddressController : Controller
    {
        Core.Interface.BLL.WebApi.IAddressMaster iAddressMaster;
        public AddressController()
        {
            iAddressMaster = new AddressMasterBLL();
        }
        // GET: WebApi/Address
        [WebMethod]
        public ActionResult AllCityList()
        {
            return Json(iAddressMaster.GetAllCityListDdl(), JsonRequestBehavior.AllowGet);
        }
    }
}
