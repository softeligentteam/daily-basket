﻿using DailyBasket.Warehouse.Areas.WebApi.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class OfferController : Controller
    {
        // GET: WebApi/Offer
        [WebMethod]
        public JsonResult IsPromocodeExists(string promocode)
        {
            bool status = new OfferBLL().IsPromocodeExists(promocode);
            return Json(status, JsonRequestBehavior.AllowGet);
        }
    }
}