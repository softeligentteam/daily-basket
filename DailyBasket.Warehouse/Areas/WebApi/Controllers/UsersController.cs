using DailyBasket.Warehouse.Areas.WebApi.BLL;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class UsersController : Controller
    {
        // GET: WebApi/Users
        [WebMethod]
        public JsonResult IsWarehouseUserMobileNoExists(string WarehouseUserMobile)
        {
            bool status = new UsersBLL().IsWarehouseUserMobileNoExists(WarehouseUserMobile);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        
        [WebMethod]
        public JsonResult IsWarehouseUserEmailIdExists(string UserEmail)
        {
            bool status = new UsersBLL().IsWarehouseUserEmailIdExists(UserEmail);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsFranchiseeMobileNoExists(string franchiseeMobile)
        {
            bool status = new UsersBLL().IsFranchiseeMobileNoExists(franchiseeMobile);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsFranchiseeGSTINExists(string gstinumber)
        {
            bool status = new UsersBLL().IsFranchiseeGSTINExists(gstinumber);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsFranchiseeEmailIdExists(string UserEmail)
        {
            bool status = new UsersBLL().IsFranchiseeEmailIdExists(UserEmail);
            return Json(status, JsonRequestBehavior.AllowGet);
        }
    }
}