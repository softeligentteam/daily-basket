﻿using DailyBasket.Warehouse.Areas.WebApi.BLL;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class VendorController : Controller
    {
        // GET: WebApi/Vendor
        [WebMethod]
        public JsonResult IsVendorNameExists(string vendorName)
        {
            bool status = new VendorBLL().IsVendorNameExists(vendorName);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsVendorEmailIdExists(string vendorEmail)
        {
            bool status = new VendorBLL().IsVendorEmailIdExists(vendorEmail);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsVendorMobileNoExists(string vendorMobile)
        {
            bool status = new VendorBLL().IsVendorMobileNoExists(vendorMobile);
            return Json(status, JsonRequestBehavior.AllowGet);
        }
        
        [WebMethod]
        public JsonResult IsVendorGSTINExists(string gstinumber)
        {
            bool status = new VendorBLL().IsVendorGSTINExists(gstinumber);
            return Json(status, JsonRequestBehavior.AllowGet);
        }
    }
}