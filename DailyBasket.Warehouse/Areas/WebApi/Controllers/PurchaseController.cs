using DailyBasket.Warehouse.Areas.WebApi.BLL;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class PurchaseController : Controller
    {
        // GET: WebApi/Purchase
        [WebMethod]
        public JsonResult UpdateBillAmount(float updatedBillAmount = 0, ulong purchaseOrderId = 0)
        {
            if (purchaseOrderId != 0)
            {
                int status = new PurchaseOrderBLL().UpdateBillAmount(updatedBillAmount, purchaseOrderId, SessionManager.CurrentUserId);
                if (status == 1)
                {
                    SessionManager.UpdatePurchaseTotalBillAmountCookie(updatedBillAmount);
                    return Json(status, JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }

        public JsonResult DeletePendingPurchaseEntry(ulong poId = 0)
        {
            if (poId != 0)
            {
                int status = new PurchaseOrderBLL().DeletePendingPurchaseEntry(poId);
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }
    }
}