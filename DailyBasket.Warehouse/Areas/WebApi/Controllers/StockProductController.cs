﻿using DailyBasket.Warehouse.Areas.Stock.Models.ViewModels;
using DailyBasket.Warehouse.Areas.WebApi.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace DailyBasket.Warehouse.Areas.WebApi.Controllers
{
    public class StockProductController : Controller
    {
        // GET: WebApi/StockProduct
        [WebMethod]
        public JsonResult StockProductSaleRate(ulong productId)
        {

            ProductSaleRateViewModel productSaleRateVM = null;
            if (productId != 0)
            {
                productSaleRateVM = new StockProductBLL().GetStockProductSaleRate(productId);
            }
            return Json(productSaleRateVM, JsonRequestBehavior.AllowGet);
        }
    }
}