﻿using DailyBasket.Warehouse.Core.Interface.BLL.WebApi;
using DailyBasket.Warehouse.Repository.WebApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class OfferBLL : IOffer
    {
        public bool IsPromocodeExists(string promocode)
        {
            return new OfferRepository().IsPromocodeExists(promocode);
        }
    }
}