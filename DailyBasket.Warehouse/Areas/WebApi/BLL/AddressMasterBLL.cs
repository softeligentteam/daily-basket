﻿using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Interface.BLL.WebApi;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Core.Model.Users;
using DailyBasket.Warehouse.Helper;
using DailyBasket.Warehouse.Repository.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class AddressMasterBLL : IAddressMaster
    {
        AddressRepository addressRepository = new AddressRepository();

        public List<IntStringDdlModel> GetAllCityListDdl()
        {
            return DropdownlistHelper.GetIntStringModelDropDownList(addressRepository.GetCityListDdl());
        }
        
    }
}