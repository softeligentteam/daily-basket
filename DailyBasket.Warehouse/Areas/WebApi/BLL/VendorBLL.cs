﻿using DailyBasket.Warehouse.Core.Interface.BLL.WebApi;
using DailyBasket.Warehouse.Repository.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class VendorBLL : IVendor
    {
        public bool IsVendorNameExists(string vendorName)
        {
            return new VendorRepository().IsVendorNameExists(vendorName);
        }

        public bool IsVendorEmailIdExists(string vendorEmail)
        {
            return new VendorRepository().IsVendorEmailIdExists(vendorEmail);
        }

        public bool IsVendorMobileNoExists(string vendorMobile)
        {
            return new VendorRepository().IsVendorMobileNoExists(vendorMobile);
        }

        public bool IsVendorGSTINExists(string gstinumber)
        {
            return new VendorRepository().IsVendorGSTINExists(gstinumber);
        }
    }
}