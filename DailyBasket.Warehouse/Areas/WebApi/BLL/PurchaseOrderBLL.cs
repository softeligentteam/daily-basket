using DailyBasket.Warehouse.Core.Interface.BLL.WebApi;
using DailyBasket.Warehouse.Repository.Purchase;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class PurchaseOrderBLL : IPurchaseProduct
    {
        public int UpdateBillAmount(float updatedBillAmount, ulong purchaseOrderId, ulong userId)
        {
            return new PurchaseProductRepository().UpdateBillAmount(updatedBillAmount, purchaseOrderId, userId); 
        }

        public int DeletePendingPurchaseEntry(ulong purchaseOrderId = 0)
        {
            if (purchaseOrderId != 0)
                return new PurchaseProductRepository().DeletePendingPurchaseEntry(purchaseOrderId);
            return 0;
        }
    }
}