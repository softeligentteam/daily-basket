﻿using DailyBasket.Warehouse.Areas.Stock.Models.ViewModels;
using DailyBasket.Warehouse.Repository.Stock;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class StockProductBLL
    {
        Core.Interface.DAL.Stock.IStockProduct istockProductDAL;
        public StockProductBLL()
        {
            istockProductDAL = new StockProductRepository();
        }
        public dynamic GetStockProductSaleRate(ulong ProductId)
        {
            DataTable dataTable = istockProductDAL.GetStockProductSaleRate(ProductId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                ProductSaleRateViewModel productSaleRateVM = new ProductSaleRateViewModel();
                productSaleRateVM.ProductId = Convert.ToUInt32(dataTable.Rows[0]["prod_id"].ToString());
                productSaleRateVM.Mrp = Convert.ToSingle(dataTable.Rows[0]["mrp"]);
                productSaleRateVM.OnlineSaleRate = Convert.ToSingle(dataTable.Rows[0]["online_sale_rate"]);
                productSaleRateVM.SaleRateA = Convert.ToSingle(dataTable.Rows[0]["sale_rate_a"]);
                productSaleRateVM.SaleRateB = Convert.ToSingle(dataTable.Rows[0]["sale_rate_b"]);
                return productSaleRateVM;
            }
            return null;
        }
    }
}