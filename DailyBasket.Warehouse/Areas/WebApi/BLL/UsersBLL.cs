using DailyBasket.Warehouse.Repository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class UsersBLL
    {
        public bool IsWarehouseUserMobileNoExists(string userMobile)
        {
            return new UserHandlerRepository().IsWarehouseMobileNoExists(userMobile);
        }

        public bool IsWarehouseUserEmailIdExists(string userEmail)
        {
            return new UserHandlerRepository().IsWarehouseEmailIdExists(userEmail);
        }

        public bool IsFranchiseeMobileNoExists(string franchiseeMobile)
        {
            return new UserHandlerRepository().IsFranchiseeMobileNoExists(franchiseeMobile);
        }

        public bool IsFranchiseeGSTINExists(string franchiseeGstNo)
        {
            return new UserHandlerRepository().IsFranchiseeGSTINExists(franchiseeGstNo);
        }

        public bool IsFranchiseeEmailIdExists(string userEmail)
        {
            return new UserHandlerRepository().IsWarehouseEmailIdExists(userEmail);
        }
    }
}