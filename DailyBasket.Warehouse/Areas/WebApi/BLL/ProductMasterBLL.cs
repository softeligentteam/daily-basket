﻿using DailyBasket.Warehouse.Areas.Purchase.Models.ViewModels;
using DailyBasket.Warehouse.Areas.Stock.Models.ViewModels;
using DailyBasket.Warehouse.Areas.WebApi.BLL;
using DailyBasket.Warehouse.Core.Interface.BLL.WebApi;
using DailyBasket.Warehouse.Core.Interface.DAL.ProductMaster;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Helper;
using DailyBasket.Warehouse.Repository.ProductMaster;
using DailyBasket.Warehouse.Repository.Purchase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.WebApi.BLL
{
    public class ProductMasterBLL : IProductMaster
    {
        public List<LongStringDdlModel> GetProductsDdlList(uint mainCategoryId, uint subCategoryId)
        {
            return DropdownlistHelper.GetLongStringModelDropDownList(new ProductRepository().GetActiveProductListDdl(mainCategoryId, subCategoryId));
        }

        public List<IntStringDdlModel> GetSubCategoryDdlList(uint MainCategoryId)
        {
            return DropdownlistHelper.GetIntStringModelDropDownList(new ProductCategoryRepository().GetActiveSubCategoryListDdl(MainCategoryId));
        }

        public bool IsProductBrandNameExists(string brandName)
        {
            return new BrandRepository().IsProductBrandNameExists(brandName);
        }

        public bool IsProductMainCategoryExists(string mainCategoryName)
        {
            return new ProductCategoryRepository().IsProductMainCategoryExists(mainCategoryName);
        }

        public bool IsProductNameExists(string productName)
        {
            return new ProductRepository().IsProductNameExists(productName);
        }

        public bool IsProductSubCategoryExists(string subCategoryName)
        {
            return new ProductCategoryRepository().IsProductSubCategoryExists(subCategoryName);
        }

        public dynamic GetProductSaleRate(ulong ProductId)
        {
            DataTable dataTable = new ProductRepository().GetProductSaleRate(ProductId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                ProductSaleRateViewModel productSaleRateVM = new ProductSaleRateViewModel();
                productSaleRateVM.ProductId = Convert.ToUInt32(dataTable.Rows[0]["prod_id"].ToString());
                productSaleRateVM.OnlineSaleRate = Convert.ToSingle(dataTable.Rows[0]["online_sale_rate"]);
                productSaleRateVM.SaleRateA = Convert.ToSingle(dataTable.Rows[0]["sale_rate_a"]);
                productSaleRateVM.SaleRateB = Convert.ToSingle(dataTable.Rows[0]["sale_rate_b"]);
                return productSaleRateVM;
            }
            return null;
        }

        public bool IsProductHsnNumberExists(string productHsnNumber)
        {
            return new ProductRepository().IsProductHsnNumberExists(productHsnNumber);
        }

        public float GetConvertableProductPackagingSize(ulong productId)
        {
            DataTable dataTable = new ProductRepository().GetConvertableProductPackagingSize(productId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return Convert.ToSingle(dataTable.Rows[0]["quantity"].ToString());
            }
            return 0;
        }

        public float IsProductStockAvailable(ulong productId)
        {
            DataTable dataTable = new ProductRepository().IsProductStockAvailable(productId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return Convert.ToSingle(dataTable.Rows[0]["available_stock"].ToString());
            }
            return 0;
        }
    }
}