﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Areas.Reports.BLL;
using DailyBasket.Warehouse.Areas.Reports.Models;
using DailyBasket.Warehouse.Areas.Reports.Models.ViewModels;
using DailyBasket.Warehouse.Core.Model.Report.Stock;
using DailyBasket.Warehouse.Helper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ClosedXML.Excel;

namespace DailyBasket.Warehouse.Areas.Reports.Controllers
{
    public class StockController : Controller
    {
        StockReportBLL StockReportBLL = new StockReportBLL();
        PurchaseReportBLL purchaseReportBLL = new PurchaseReportBLL();
        public ActionResult Stock()
        {
            BindstockReportDropdownList();
            return View();
        }
        private void BindstockReportDropdownList()
        {
            ViewBag.PurchaseReportDdlList = EnumDropdownlistHelper.GetEnumStockReportList();
        }
        [HttpPost]
        public ActionResult Stock(StockReportList StockReportList)
        {
            BindstockReportDropdownList();
            var rptname = (StockReportList.Report).ToString();

            switch (rptname)
            {
                case "ClosingStock":

                    return RedirectToAction("ClosingStock");
                case "CurrentAvailableStock":

                    return RedirectToAction("CurrentAvailableStock");
                case "ExpireWiseStock":
                    return RedirectToAction("ExpireWiseStock");

                default:
                    return PartialView("Stock");
            }

        }
        [HttpGet]
        public ActionResult ClosingStock()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ClosingStockWise");
        }
        [HttpGet]
        public ActionResult CurrentAvailableStock()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("CurrentStockAvailableList");
        }

        [HttpGet]
        public ActionResult ExpireWiseStock()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ExpireWiseStock");
        }
        // GET: Reports/Stock
        [HttpPost]
        public ActionResult ExpireWiseStockReport(string Submit, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable closingStock = null;
                    closingStock = StockReportBLL.ExpireWiseStockforExcel(ProductId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(closingStock, "ExpireWiseStockReport");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=ExpireWiseStockReport.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("ExpireWiseStock");
                default:
                    List<ExpireWiseStockModel> expireWiseStockModel = null;
                    expireWiseStockModel = StockReportBLL.ExpireWiseStock(ProductId);
                    if (expireWiseStockModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(expireWiseStockModel, "ExpireWiseStockReport");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=ExpireWiseStockReport.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("ExpireWiseStock");
            }
        }
        [HttpPost]
        public ActionResult ClosingStock(string Submit, string From_Date, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable closingStock = null;
                    closingStock = StockReportBLL.GetClosingStockReportListforExcel(From_Date, ProductId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(closingStock, "ClosingStock");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=CloseStock Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("ClosingStock");
                default:
                    List<ClosingStockModel> closingStockModel = null;
                    closingStockModel = StockReportBLL.GetClosingStockReportList(From_Date, ProductId);
                    if (closingStockModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(closingStockModel, "ClosingStock");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=CloseStock Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("ClosingStock");
            }
        }

        [HttpPost]
        public ActionResult currentAvailableStockModel(string Submit, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable currentAvailableStock = null;
                    currentAvailableStock = StockReportBLL.CurrentAvailableStockforExcel(ProductId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(currentAvailableStock, "CurrentStock");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=CurrentStock Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("currentAvailableStockModel");
                default:
                    List<CurrentAvailableStockModel> currentAvailableStockModel = null;
                    currentAvailableStockModel = StockReportBLL.CurrentAvailableStock(ProductId);
                    if (currentAvailableStockModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(currentAvailableStockModel, "CurrentStock");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=CurrentStock Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("currentAvailableStockModel");
            }
        }
        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            var ms = new MemoryStream();
            var txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            var doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            var oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            var htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        public string RenderRazorViewToString(dynamic viewModel, string viewName)
        {

            ViewData["viewModel"] = viewModel;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        /****************************************Franchisee Report Section***************************************/
        private void BindFranchiseeList()
        {
            ViewBag.FranchiseeList = purchaseReportBLL.GetFranchiseeList();
        }

        public ActionResult FranchiseeStock()
        {
            BindstockReportDropdownList();
            return View();
        }

        [HttpPost]
        public ActionResult FranchiseeStock(StockReportList StockReportList)
        {
            BindstockReportDropdownList();
            var rptname = (StockReportList.Report).ToString();

            switch (rptname)
            {
                case "ClosingStock":
                    return RedirectToAction("FranchiseeClosingStock");

                case "CurrentAvailableStock":
                    return RedirectToAction("FranchiseeCurrentAvailableStock");

                case "ExpireWiseStock":
                    return RedirectToAction("FranchiseeExpireWiseStock");

                default:
                    return PartialView("FranchiseeStock");
            }

        }
        [HttpGet]
        public ActionResult FranchiseeClosingStock()
        {
            BindFranchiseeList();
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("FranchiseeClosingStockWise");
        }
        [HttpGet]
        public ActionResult FranchiseeCurrentAvailableStock()
        {
            BindFranchiseeList();
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("FranchiseeCurrentStockAvailableList");
        }

        [HttpGet]
        public ActionResult FranchiseeExpireWiseStock()
        {
            BindFranchiseeList();
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("FranchiseeExpireWiseStock");
        }

        [HttpPost]
        public ActionResult FranchiseeClosingStock(string Submit, string From_Date, uint ProductId = 0, ulong FranchiseeId = 0)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":
                        DataTable closingStock = null;
                        closingStock = StockReportBLL.GetFClosingStockReportListforExcel(From_Date, ProductId, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(closingStock, "FranchiseeClosingStock");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee CloseStock Report.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return RedirectToAction("FranchiseeClosingStock");
                    default:
                        ClosingStockReportModel closingStockModel = null;
                        closingStockModel = StockReportBLL.GetFClosingStockReportList(From_Date, ProductId, FranchiseeId);
                        if (closingStockModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(closingStockModel, "FranchiseeClosingStock");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {

                                var reader = new PdfReader(input);

                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee CloseStock Report.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return RedirectToAction("FranchiseeClosingStock");
                }
            }
            return RedirectToAction("FranchiseeClosingStock");
        }


        [HttpPost]
        public ActionResult FranchiseecurrentAvailableStock(string Submit, uint ProductId = 0, ulong FranchiseeId = 0)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":
                        DataTable currentAvailableStock = null;
                        currentAvailableStock = StockReportBLL.FCurrentAvailableStockforExcel(ProductId, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(currentAvailableStock, "FranchiseeCurrentStock");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee CurrentStock Report.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return RedirectToAction("FranchiseecurrentAvailableStock");
                    default:
                        CurrentAvailableStockReportModel currentAvailableStockModel = null;
                        currentAvailableStockModel = StockReportBLL.FCurrentAvailableStock(ProductId, FranchiseeId);
                        if (currentAvailableStockModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(currentAvailableStockModel, "FranchiseeCurrentStock");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {
                                var reader = new PdfReader(input);
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee CurrentStock Report.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();

                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return RedirectToAction("FranchiseecurrentAvailableStock");
                }
            }
            return RedirectToAction("FranchiseecurrentAvailableStock");
        }

        [HttpPost]
        public ActionResult FranchiseeExpireWiseStockReport(string Submit, uint ProductId = 0, ulong FranchiseeId = 0)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":
                        DataTable closingStock = null;
                        closingStock = StockReportBLL.FExpireWiseStockforExcel(ProductId, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(closingStock, "FranchiseeExpireWiseStockReport");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee ExpireWiseStockReport.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return RedirectToAction("FranchiseeExpireWiseStock");
                    default:
                        ExpireWiseStockReportModel expireWiseStockModel = null;
                        expireWiseStockModel = StockReportBLL.FExpireWiseStock(ProductId, FranchiseeId);
                        if (expireWiseStockModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(expireWiseStockModel, "FranchiseeExpireWiseStockReport");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {
                                var reader = new PdfReader(input);
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee ExpireWiseStockReport.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();

                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return RedirectToAction("FranchiseeExpireWiseStock");
                }
            }
            return RedirectToAction("FranchiseeExpireWiseStock");
        }

    }
}