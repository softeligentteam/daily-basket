﻿using DailyBasket.Warehouse.Areas.Reports.Models;
using DailyBasket.Warehouse.Areas.Reports.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Helper;
using DailyBasket.Warehouse.Areas.Reports.BLL;
using DailyBasket.Warehouse.Core.Model.Report.Purchase;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using ClosedXML.Excel;
namespace DailyBasket.Warehouse.Areas.Reports.Controllers
{
    public class PurchaseController : Controller
    {
        // GET: Reports/Purchase
        PurchaseReportBLL PurchaseReportBLL = new PurchaseReportBLL();
        public ActionResult Purchase()
        {
            BindPurchaseReportDropdownList();
            return View();
        }
        [HttpPost]
        public ActionResult Purchase(PurchaseReportList PurchaseReportList)
        {

            BindPurchaseReportDropdownList();
            var rptname = (PurchaseReportList.Report).ToString();
            switch (rptname)
            {
                case "BillWise":
                    return RedirectToAction("BillWise");
                case "DateWise":
                    return RedirectToAction("DateWise");
                case "VendorAndProductWise":
                    return RedirectToAction("VendorAndProductWise");
                case "VendorWise":
                    return RedirectToAction("VendorWise");
                default:
                    return RedirectToAction("Purchase");
            }
        }
        [HttpGet]
        public ActionResult BillWise()
        {
            return PartialView("BillWisePurchase");
        }
        [HttpGet]
        public ActionResult DateWise()
        {
            return PartialView("DateWisePurchase");
        }

        [HttpGet]
        public ActionResult VendorAndProductWise()
        {
            return PartialView("VendorAndProductWisePurchase");
        }
        [HttpGet]
        public ActionResult VendorWise()
        {
            return PartialView("VendorWise");
        }

        [HttpPost]
        public ActionResult BillWise(int Bill_No, string Submit)
        {

            switch (Submit)
            {
                case "Export To Excel":

                    DataTable billWisePurchase = null;
                    billWisePurchase = PurchaseReportBLL.GetBillWiseReportListforExcel(Bill_No.ToString());
                    //conbillWisePurchaseReport = ConvertBillWiseReportData(billWisePurchase);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(billWisePurchase, "BillWisePurchaseReport");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename= BillWise Purchase Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }

                    return RedirectToAction("BillWise");
                default:
                    BillWisePurchaseModel billWisePurchaseModel = null;
                    billWisePurchaseModel = PurchaseReportBLL.GetBillWiseReportList(Bill_No.ToString());
                    if (billWisePurchaseModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(billWisePurchaseModel, "BillWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=BillWise Purchase Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("BillWise");
            }
            //return RedirectToAction("BillWise");
        }

        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            var ms = new MemoryStream();
            var txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            var doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            var oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            var htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        public string RenderRazorViewToString(dynamic viewModel, string viewName)
        {

            ViewData["viewModel"] = viewModel;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }


        [HttpPost]
        public ActionResult DateWise(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":

                    DataTable dateWisePurchase = null;
                    dateWisePurchase = PurchaseReportBLL.GetDayWisReportListforExcel(From_Date, To_Date);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dateWisePurchase, "DateWisePurchase");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename= DateWise Purchase Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("DateWise");
                default:
                    List<DateWisePurchaseModel> dateWisePurchaseModel = null;
                    dateWisePurchaseModel = PurchaseReportBLL.GetDayWisReportList(From_Date, To_Date);
                    if (dateWisePurchaseModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(dateWisePurchaseModel, "DateWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {

                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=DateWise Purchase Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("DateWise");
            }
        }

        public ActionResult VendorAndProductWise(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":

                    DataTable vendorAndProductWisePurchase = null;
                    vendorAndProductWisePurchase = PurchaseReportBLL.GetVendorWiseProductWiseListforExcel(From_Date, To_Date);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(vendorAndProductWisePurchase, "VendorAndProductWise");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename= VendorAndProductWise Purchase Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("VendorAndProductWise");
                default:

                    List<VendorAndProductWisePurchaseModel> vendorAndProductWisePurchaseModel = null;
                    vendorAndProductWisePurchaseModel = PurchaseReportBLL.GetVendorWiseProductWiseList(From_Date, To_Date);
                    if (vendorAndProductWisePurchaseModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(vendorAndProductWisePurchaseModel, "VendorAndProductWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=VendorAndProductWise Purchase Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();

                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("VendorAndProductWise");
            }
        }
        [HttpPost]
        public ActionResult VendorWiseReport(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":

                    DataTable vendorWisePurchase = null;
                    vendorWisePurchase = PurchaseReportBLL.GetVendorWisePurchaseforExcel();
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(vendorWisePurchase, "VendorAndProductWise");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename= VendorAndProductWise Purchase Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return View(vendorWisePurchase);
                default:
                    List<VendorWisePurchaseModel> vendorWisePurchaseModel = null;
                    vendorWisePurchaseModel = PurchaseReportBLL.GetVendorWisePurchase();

                    if (vendorWisePurchaseModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(vendorWisePurchaseModel, "VendorWiseReport");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=VendorWise Purchase Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();

                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return View(vendorWisePurchaseModel);
            }
        }

        public ActionResult VendorWise(string From_Date, string To_Date)
        {
            return RedirectToAction("VendorWise");
        }

        private void BindPurchaseReportDropdownList()
        {
            ViewBag.PurchaseReportDdlList = EnumDropdownlistHelper.GetEnumPurchaseReportList();
        }

        /**************************** Franchisee Reports Start********************************/

        private void BindFranchiseePurchaseReportDropdownList()
        {
            ViewBag.FPurchaseReportDdlList = EnumDropdownlistHelper.GetEnumFranchiseePurchaseReportList();
        }
        public ActionResult FranchiseePurchase()
        {
            BindFranchiseePurchaseReportDropdownList();
            return View();
        }
        [HttpPost]
        public ActionResult FranchiseePurchase(PurchaseReportList PurchaseReportList)
        {
            BindFranchiseePurchaseReportDropdownList();
            var rptname = (PurchaseReportList.FReport).ToString();
            switch (rptname)
            {
                case "BillWise":
                    return RedirectToAction("FranchiseeBillWise");
                case "DateWise":
                    return RedirectToAction("FranchiseeDateWise");
                default:
                    return RedirectToAction("FranchiseePurchase");
            }
        }

        private void BindFranchiseeList()
        {
            ViewBag.FranchiseeList = PurchaseReportBLL.GetFranchiseeList();
        }
        [HttpGet]
        public ActionResult FranchiseeBillWise()
        {
            //BindFranchiseeList();
            return PartialView("FranchiseeBillWisePurchase");
        }

        [HttpGet]
        public ActionResult FranchiseeDateWise()
        {
            BindFranchiseeList();
            return PartialView("FranchiseeDateWisePurchase");
        }

        [HttpPost]
        public ActionResult FranchiseeBillWise(int Bill_No, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":

                    DataTable billWisePurchase = null;
                    billWisePurchase = PurchaseReportBLL.GetFranchiseeBillWiseReportListforExcel(Bill_No.ToString());
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(billWisePurchase, "FranchiseeBillWise");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename= Franchisee BillWise Purchase Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }

                    return RedirectToAction("FranchiseeBillWise");
                default:
                    BillWisePurchaseModel billWisePurchaseModel = null;
                    billWisePurchaseModel = PurchaseReportBLL.GetFranchiseeBillWiseReportList(Bill_No.ToString());
                    if (billWisePurchaseModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(billWisePurchaseModel, "FranchiseeBillWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee BillWise Purchase Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("FranchiseeBillWise");
            }
        }


        [HttpPost]
        public ActionResult FranchiseeDateWise(string From_Date, string To_Date, string Submit, ulong FranchiseeId = 0)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":

                        DataTable dateWisePurchase = null;
                        dateWisePurchase = PurchaseReportBLL.GetFranchiseeDayWisReportListforExcel(From_Date, To_Date, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dateWisePurchase, "FranchiseeDateWisePurchase");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee DateWise Purchase Report.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return RedirectToAction("FranchiseeDateWise");
                    default:
                        DateWisePurchaseReportModel dateWisePurchaseModel = null;
                        dateWisePurchaseModel = PurchaseReportBLL.GetFranchiseeDayWisReportList(From_Date, To_Date, FranchiseeId);
                        if (dateWisePurchaseModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(dateWisePurchaseModel, "FranchiseeDateWise");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {
                                var reader = new PdfReader(input);
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee DateWise Purchase Report.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return RedirectToAction("FranchiseeDateWise");
                }
            }
            return RedirectToAction("FranchiseeDateWise");
        }
    }
}