﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Areas.Reports.BLL;
using DailyBasket.Warehouse.Areas.Reports.Models.ViewModels;
using DailyBasket.Warehouse.Helper;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Mvc;
using DailyBasket.Warehouse.Core.Model.Report.Sales;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using ClosedXML.Excel;

namespace DailyBasket.Warehouse.Areas.Reports.Controllers
{
    public class SaleController : Controller
    {
        SalesReportBLL SalesReportBLL = new SalesReportBLL();
        PurchaseReportBLL PurchaseReportBLL = new PurchaseReportBLL();
        public ActionResult Sales()
        {
            BindPurchaseReportDropdownList();
            return View();
        }
        [HttpPost]
        public ActionResult Sales(SalesReportList SalesReportList)
        {
            BindPurchaseReportDropdownList();
            var rptname = (SalesReportList.Report).ToString();

            switch (rptname)
            {
                case "BillWise":
                    return RedirectToAction("BillWise");
                case "BrandWise":
                    return RedirectToAction("BrandWise");
                case "MainCategoryWise":

                    return RedirectToAction("MainCategoryWise");
                case "SubCategoryWise":

                    return RedirectToAction("SubCategoryWise");
                case "DateWiseCustomerOrder":
                    return RedirectToAction("DateWiseCustomerOrder");
                case "ProductWiseSales":

                    return RedirectToAction("ProductWiseSales");
                case "ProductWiseGSTReport":
                    return RedirectToAction("ProductWiseGSTReport");

                default:
                    return RedirectToAction("Sales");
            }

        }

        [HttpGet]
        public ActionResult BillWise()
        {
            return PartialView("BillWiseSales");
        }
        [HttpGet]
        public ActionResult BrandWise()
        {
            ViewBag.BrandDdlList = new BrandBLL().GetActiveBrandListDdl();
            return PartialView("BrandWiseSales");
        }

        [HttpGet]
        public ActionResult MainCategoryWise()
        {
            ViewBag.MainCategoryDdlList = new ProductCategoryBLL().GetActiveMainCategoryListDdl();
            return PartialView("MainCategoryWiseSales");
        }
        [HttpGet]
        public ActionResult SubCategoryWise()
        {
            ViewBag.SubCategoryDdlList = new ProductCategoryBLL().GetAllSubCatogoryListDdl();
            return PartialView("SubCategoryWiseSales");
        }
        [HttpGet]
        public ActionResult DateWiseCustomerOrder()
        {
            return PartialView("DateWiseCustomerOrder");
        }
        [HttpGet]
        public ActionResult ProductWiseSales()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ProductWiseSales");
        }
        [HttpGet]
        public ActionResult ProductWiseGSTReport()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ProductWiseGSTReport");
        }
        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            var ms = new MemoryStream();
            var txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            var doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            var oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            var htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        public string RenderRazorViewToString(dynamic viewModel, string viewName)
        {

            ViewData["viewModel"] = viewModel;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }
        [HttpPost]
        public ActionResult MainCategoryWise(string Submit, uint MainCategoryId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable mainCategoryWiseSales = null;
                    mainCategoryWiseSales = SalesReportBLL.GetMainCategoryWiseSalesListforExcel(MainCategoryId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(mainCategoryWiseSales, "MainCategoryWise");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=MainCategoryWise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("MainCategoryWise");
                default:
                    List<MainCategoryWiseSalesModel> mainCategoryWiseSalesModel = null;
                    mainCategoryWiseSalesModel = SalesReportBLL.GetMainCategoryWiseSalesList(MainCategoryId);
                    if (mainCategoryWiseSalesModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(mainCategoryWiseSalesModel, "MainCategoryWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=MainCategoryWise Sales Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }

                    return RedirectToAction("MainCategoryWise");
            }

        }

        [HttpPost]
        public ActionResult SubCategoryWise(string Submit, uint SubCategoryId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable subCategoryWiseSales = null;
                    subCategoryWiseSales = SalesReportBLL.GetSubCategoryWiseSalesListforExcel(SubCategoryId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(subCategoryWiseSales, "SubCategoryWise");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=SubCategoryWise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("SubCategoryWise");
                default:
                    List<SubCategoryWiseSalesModel> subCategoryWiseSalesModel = null;
                    subCategoryWiseSalesModel = SalesReportBLL.GetSubCategoryWiseSalesList(SubCategoryId);
                    if (subCategoryWiseSalesModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(subCategoryWiseSalesModel, "SubCategoryWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=SubCategoryWise Sales Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("SubCategoryWise");
            }

        }
        [HttpPost]
        public ActionResult BrandWise(string Submit, uint BrandId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable billWiseSales = null;
                    billWiseSales = SalesReportBLL.GetBrandWiseSalesListforExcel(BrandId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(billWiseSales, "BrandWiseSalesReport");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=BrandWise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("BrandWise");
                default:
                    List<BrandWiseSalesModel> brandWiseSalesModel = null;
                    brandWiseSalesModel = SalesReportBLL.GetBrandWiseSalesList(BrandId);
                    if (brandWiseSalesModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(brandWiseSalesModel, "BrandWise");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=BrandWise Sales Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("BrandWise");
            }

        }
        [HttpPost]
        public ActionResult productWiseSalesModel(string Submit, string From_Date, string To_Date, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable productWiseSales = null;
                    productWiseSales = SalesReportBLL.GetProductWiseSalesReportListforExcel(From_Date, To_Date, ProductId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(productWiseSales, "productWiseSales");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=ProductWise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("ProductWiseSales");
                default:
                    List<ProductWiseSalesModel> productWiseSalesModel = null;
                    productWiseSalesModel = SalesReportBLL.GetProductWiseSalesReportList(From_Date, To_Date, ProductId);
                    if (productWiseSalesModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(productWiseSalesModel, "productWiseSalesModel");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {

                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=ProductWise Sales Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();

                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("ProductWiseSales");
            }
        }
        [HttpPost]
        public ActionResult ProductWiseSales(string From_Date, string To_Date, uint ProductId = 0)
        {
            return RedirectToAction("ProductWiseSales");
        }
        [HttpPost]
        public ActionResult ProductWiseGSTReport(SalesReportList salesReportList, uint ProductId = 0)
        {

            return RedirectToAction("ProductWiseSales");
        }
        [HttpPost]
        public ActionResult BillWise(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable billWiseSales = null;
                    billWiseSales = SalesReportBLL.GetBillWiseSalesListForExcel(From_Date, To_Date);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(billWiseSales, "Customers");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=Billwise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("BillWise");
                default:
                    List<BillWiseSalesModel> billWiseSalesModel = null;
                    billWiseSalesModel = SalesReportBLL.GetBillWiseSalesList(From_Date, To_Date);
                    if (billWiseSalesModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(billWiseSalesModel, "BillWise");
                        var bytes = GetPDF(HTMLContent);
                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=Billwise Sales Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return RedirectToAction("BillWise");
            }
            //return RedirectToAction("BillWise");
        }

        [HttpPost]
        public ActionResult DateWiseCustomerOrderSalesModel(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable dateWiseCustomerOrderSales = null;
                    dateWiseCustomerOrderSales = SalesReportBLL.GetDateWiseCustomerOrderListforExcel(From_Date, To_Date);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dateWiseCustomerOrderSales, "dateWiseCustomerOrderSales");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=DateWiseCustomerOrder Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return View(dateWiseCustomerOrderSales);
                default:
                    List<DateWiseCustomerOrderSalesModel> dateWiseCustomerOrderSalesModel = null;
                    dateWiseCustomerOrderSalesModel = SalesReportBLL.GetDateWiseCustomerOrderList(From_Date, To_Date);
                    if (dateWiseCustomerOrderSalesModel != null)
                    {
                        var HTMLContent = RenderRazorViewToString(dateWiseCustomerOrderSalesModel, "dateWiseCustomerOrderSalesModel");
                        var bytes = GetPDF(HTMLContent);

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            var reader = new PdfReader(input);

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;filename=DateWiseCustomerOrder Sales Report.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(bytes);
                            Response.End();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Sorry, No Record Found!";
                        TempData["CssClass"] = "text-danger";
                    }
                    return View(dateWiseCustomerOrderSalesModel);
            }


        }
        [HttpPost]
        public ActionResult DateWiseCustomerOrder(string From_Date, string To_Date)
        {

            return RedirectToAction("DateWiseCustomerOrder");
        }

        private void BindPurchaseReportDropdownList()
        {
            ViewBag.PurchaseReportDdlList = EnumDropdownlistHelper.GetEnumSalesReportList();
        }

        // Franchisee Report Section

        private void BindFranchiseeList()
        {
            ViewBag.FranchiseeList = PurchaseReportBLL.GetFranchiseeList();
        }

        private void BindFranchiseeSaleReportDropdownList()
        {
            ViewBag.PurchaseReportDdlList = EnumDropdownlistHelper.GetEnumFranchiseeSalesReportList();
        }
        public ActionResult FranchiseeSales()
        {
            BindFranchiseeSaleReportDropdownList();
            return View();
        }
        [HttpPost]
        public ActionResult FranchiseeSales(SalesReportList SalesReportList)
        {
            BindFranchiseeSaleReportDropdownList();
            var rptname = (SalesReportList.FReport).ToString();

            switch (rptname)
            {
                case "BillWise":
                    return RedirectToAction("FranchiseeBillWise");

                case "DateWiseCustomerOrder":
                    return RedirectToAction("FranchiseeDateWiseOrder");

                case "ProductWiseSales":
                    return RedirectToAction("FranchiseeProductWiseSales");

                default:
                    return RedirectToAction("FranchiseeSales");
            }
        }

        [HttpGet]
        public ActionResult FranchiseeBillWise()
        {
            BindFranchiseeList();
            return PartialView("FranchiseeBillWiseSales");
        }

        [HttpGet]
        public ActionResult FranchiseeDateWiseOrder()
        {
            BindFranchiseeList();
            return PartialView("FranchiseeDateWiseCustomerOrder");
        }

        [HttpGet]
        public ActionResult FranchiseeProductWiseSales()
        {
            BindFranchiseeList();
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("FranchiseeProductWiseSales");
        }


        [HttpPost]
        public ActionResult FranchiseeBillWise(string From_Date, string To_Date, string Submit, ulong FranchiseeId = 0)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":
                        DataTable billWiseSales = null;
                        billWiseSales = SalesReportBLL.GetFBillWiseSalesListForExcel(From_Date, To_Date, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(billWiseSales, "FranchiseeBillWise");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee Billwise Sales Report.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return RedirectToAction("FranchiseeBillWise");
                    default:
                        BillWiseSalesReportModel billWiseSalesModel = null;
                        billWiseSalesModel = SalesReportBLL.GetFBillWiseSalesList(From_Date, To_Date, FranchiseeId);
                        if (billWiseSalesModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(billWiseSalesModel, "FranchiseeBillWise");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {
                                var reader = new PdfReader(input);
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee Billwise Sales Report.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return RedirectToAction("FranchiseeBillWise");
                }
            }
            return RedirectToAction("FranchiseeBillWise");
        }

        [HttpPost]
        public ActionResult FranchiseeDateWiseCustomerOrderSales(string From_Date, string To_Date, string Submit, ulong FranchiseeId)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":
                        DataTable dateWiseCustomerOrderSales = null;
                        dateWiseCustomerOrderSales = SalesReportBLL.GetFDateWiseCustomerOrderListforExcel(From_Date, To_Date, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dateWiseCustomerOrderSales, "DateWiseCustomerOrderSales");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename= Franchisee DateWiseCustomerOrder Sales Report.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return View(dateWiseCustomerOrderSales);
                    default:
                        DateWiseCustomerOrderSalesReportModel dateWiseCustomerOrderSalesModel = null;
                        dateWiseCustomerOrderSalesModel = SalesReportBLL.GetFDateWiseCustomerOrderList(From_Date, To_Date, FranchiseeId);
                        if (dateWiseCustomerOrderSalesModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(dateWiseCustomerOrderSalesModel, "FranchiseeDateWiseCustomerOrderSales");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {

                                var reader = new PdfReader(input);

                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee DateWiseCustomerOrder Sales Report.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();

                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return View(dateWiseCustomerOrderSalesModel);
                }
            }
            return RedirectToAction("FranchiseeDateWiseOrder");
        }

        [HttpPost]
        public ActionResult FranchiseeProductWiseSales(string Submit, string From_Date, string To_Date, uint ProductId = 0, ulong FranchiseeId = 0)
        {
            if (FranchiseeId != 0)
            {
                switch (Submit)
                {
                    case "Export To Excel":
                        DataTable productWiseSales = null;
                        productWiseSales = SalesReportBLL.GetFProductWiseSalesReportListforExcel(From_Date, To_Date, ProductId, FranchiseeId);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(productWiseSales, "productWiseSales");
                            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            wb.Style.Font.Bold = true;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Franchisee ProductWise Sales Report.xlsx");

                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        return RedirectToAction("FranchiseeProductWiseSales");
                    default:
                        ProductWiseSalesReportModel productWiseSalesModel = null;
                        productWiseSalesModel = SalesReportBLL.GetFProductWiseSalesReportList(From_Date, To_Date, ProductId, FranchiseeId);
                        if (productWiseSalesModel != null)
                        {
                            var HTMLContent = RenderRazorViewToString(productWiseSalesModel, "FranchiseeProductWiseSalesM");
                            var bytes = GetPDF(HTMLContent);

                            using (MemoryStream input = new MemoryStream(bytes))
                            {

                                var reader = new PdfReader(input);

                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename= Franchisee ProductWise Sales Report.pdf");
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.BinaryWrite(bytes);
                                Response.End();

                            }
                        }
                        else
                        {
                            TempData["Message"] = "Sorry, No Record Found!";
                            TempData["CssClass"] = "text-danger";
                        }
                        return RedirectToAction("FranchiseeProductWiseSales");
                }
            }
            return RedirectToAction("FranchiseeProductWiseSales");
        }
    }
}