﻿using DailyBasket.Warehouse.Core.Interface.BLL.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Repository.Reports;

using System.Data;
using System.Web.Mvc;
using DailyBasket.Warehouse.Core.Model.Report.Stock;

namespace DailyBasket.Warehouse.Areas.Reports.BLL
{
    public class StockReportBLL : IStockReport
    {
        Core.Interface.DAL.Reports.IStockReport iStockReportRepository;
        public StockReportBLL()
        {
            iStockReportRepository = new StockReportRepository();
        }

        public List<CurrentAvailableStockModel> CurrentAvailableStock(uint ProductId)
        {
            DataTable dataTable = iStockReportRepository.CurrentAvailableStock(ProductId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindCurrentAvailableStockModel(dataTable);
            }
            return null;
        }

        public DataTable CurrentAvailableStockforExcel(uint ProductId)
        {
            return iStockReportRepository.CurrentAvailableStock(ProductId);
        }
        private List<CurrentAvailableStockModel> BindCurrentAvailableStockModel(DataTable dataTable)
        {

            List<CurrentAvailableStockModel> currentAvailableStockModel = new List<CurrentAvailableStockModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                CurrentAvailableStockModel camodel = new CurrentAvailableStockModel();
                camodel.prod_name = Row["prod_name"].ToString();
                camodel.purchase_rate = Convert.ToSingle(Row["purchase_rate"]);
                camodel.quantity = Convert.ToSingle(Row["quantity"]);
                camodel.mrp = Convert.ToSingle(Row["mrp"]);
                camodel.online_sale_rate = Convert.ToSingle(Row["online_sale_rate"]);
                camodel.sale_rate_a = Convert.ToSingle(Row["sale_rate_a"]);
                camodel.sale_rate_b = Convert.ToSingle(Row["sale_rate_b"]);
                camodel.batch_no = Row["batch_no"].ToString();
                currentAvailableStockModel.Add(camodel);
            }
            return currentAvailableStockModel;
        }

        public List<ClosingStockModel> GetClosingStockReportList(string fromdate, uint ProductId)
        {
            DataTable dataTable = iStockReportRepository.GetClosingStockReportList(fromdate, ProductId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindClosingStockReportList(dataTable);
            }
           

            return null;
        }

        public DataTable GetClosingStockReportListforExcel(string fromdate, uint ProductId)
        {
            return iStockReportRepository.GetClosingStockReportList(fromdate, ProductId);
        }
        private List<ClosingStockModel> BindClosingStockReportList(DataTable dataTable)
        {
            List<ClosingStockModel> closingStockModel = new List<ClosingStockModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                ClosingStockModel clmodel = new ClosingStockModel();
                clmodel.prod_name = Row["prod_name"].ToString();
                clmodel.purchase_rate = Convert.ToSingle(Row["purchase_rate"]);
                clmodel.quantity = Convert.ToSingle(Row["quantity"]);
                clmodel.mrp = Convert.ToSingle(Row["mrp"]);
                clmodel.online_sale_rate = Convert.ToSingle(Row["online_sale_rate"]);
                clmodel.sale_rate_a = Convert.ToSingle(Row["sale_rate_a"]);
                clmodel.sale_rate_b = Convert.ToSingle(Row["sale_rate_b"]);
                clmodel.batch_no = Row["batch_no"].ToString();
                closingStockModel.Add(clmodel);
            }
            return closingStockModel;
            //DataTable ppoDatatable = dataSet.Tables[0];
            //ClosingStockModel closingStockModel = new ClosingStockModel();
            //purchaseOrderViewModel.PurchaseOrderId = Convert.ToUInt64(ppoDatatable.Rows[0]["po_id"].ToString());
            //purchaseOrderViewModel.VendorName = ppoDatatable.Rows[0]["vendor_name"].ToString();
            //purchaseOrderViewModel.BillNo = ppoDatatable.Rows[0]["bill_no"].ToString();
            //purchaseOrderViewModel.BillDate = ppoDatatable.Rows[0]["bill_date"].ToString();
            //purchaseOrderViewModel.FillingDate = ppoDatatable.Rows[0]["filling_date"].ToString();
            //purchaseOrderViewModel.BillDiscountAmount = Convert.ToSingle(ppoDatatable.Rows[0]["bill_discount_amount"].ToString());
            //purchaseOrderViewModel.TotalBillAmount = Convert.ToDouble(ppoDatatable.Rows[0]["total_bill_amount"].ToString());
            //if (dataSet.Tables[1].Rows.Count > 0)
            //{
            //    List<PurchaseProductViewModel> purchaseProductList = new List<PurchaseProductViewModel>();
            //    foreach (DataRow row in dataSet.Tables[1].Rows)
            //    {
            //        //pop.mrp, pop., pop.rack_no
            //        PurchaseProductViewModel ppViewModel = new PurchaseProductViewModel();
            //        ppViewModel.Id = Convert.ToUInt64(row["po_prod_id"].ToString());
            //        ppViewModel.StockProductId = Convert.ToUInt64(row["sp_id"]);
            //        ppViewModel.ProductId = Convert.ToUInt64(row["prod_id"].ToString());
            //        ppViewModel.ProductName = row["prod_name"].ToString();
            //        ppViewModel.Quantity = Convert.ToSingle(row["quantity"].ToString());
            //        ppViewModel.QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString());
            //        ppViewModel.ProductName = row["prod_name"].ToString();
            //        ppViewModel.PurchaseRate = Convert.ToSingle(row["purchase_rate"].ToString());
            //        ppViewModel.MRP = Convert.ToSingle(row["mrp"].ToString());
            //        ppViewModel.TotalAmount = Convert.ToSingle(row["total_amount"].ToString());
            //        ppViewModel.RackNo = row["rack_no"].ToString();
            //        purchaseOrderViewModel.TotalProductAmount = purchaseOrderViewModel.TotalProductAmount + ppViewModel.TotalAmount;
            //        purchaseProductList.Add(ppViewModel);
            //    }
            //    purchaseOrderViewModel.TotalProductCount = Convert.ToUInt16(dataSet.Tables[1].Rows.Count);
            //    purchaseOrderViewModel.purchaseProducts = purchaseProductList;
            //}

        }

        public List<ExpireWiseStockModel> ExpireWiseStock(uint ProductId)
        {
            DataTable dataTable = iStockReportRepository.ExpireWiseStock(ProductId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindExpireWiseStockModel(dataTable);
            }
            return null;
        }

        public DataTable ExpireWiseStockforExcel(uint ProductId)
        {
            return iStockReportRepository.ExpireWiseStock(ProductId);
        }
        private List<ExpireWiseStockModel> BindExpireWiseStockModel(DataTable dataTable)
        {

            List<ExpireWiseStockModel> expireWiseStockModel = new List<ExpireWiseStockModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                ExpireWiseStockModel exmodel = new ExpireWiseStockModel();
                exmodel.prod_name = Row["prod_name"].ToString();
                exmodel.batch_no = Row["batch_no"].ToString();
                exmodel.expire_date = Row["expiry_date"].ToString();
                exmodel.stock_quantity = Row["quantity"].ToString();
                exmodel.vendor_name= Row["vendor_name"].ToString();
                expireWiseStockModel.Add(exmodel);
            }
            return expireWiseStockModel;
        }


        public dynamic GetFClosingStockReportList(string fromdate, uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iStockReportRepository.GetFClosingStockReportList(fromdate, ProductId, fUserId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindClosingStockReportList(dataSet);
            }
            return null;
        }

        private dynamic BindClosingStockReportList(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            ClosingStockReportModel closingSModel = new ClosingStockReportModel();
            closingSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            closingSModel.Address = dataTable.Rows[0]["address"].ToString();
            closingSModel.City = dataTable.Rows[0]["city"].ToString();
            closingSModel.State = dataTable.Rows[0]["state"].ToString();
            closingSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            closingSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<ClosingStockModel> closingStockModel = new List<ClosingStockModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                ClosingStockModel clmodel = new ClosingStockModel();
                clmodel.prod_name = Row["prod_name"].ToString();
                clmodel.purchase_rate = Convert.ToSingle(Row["purchase_rate"]);
                clmodel.quantity = Convert.ToSingle(Row["quantity"]);
                clmodel.mrp = Convert.ToSingle(Row["mrp"]);
                clmodel.online_sale_rate = Convert.ToSingle(Row["sale_rate"]);
                clmodel.batch_no = Row["batch_no"].ToString();
                closingStockModel.Add(clmodel);
            }
            closingSModel.ClosingSRModel = closingStockModel;
            return closingSModel;
        }



        public dynamic FCurrentAvailableStock(uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iStockReportRepository.FCurrentAvailableStock(ProductId, fUserId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindFranchiseeCurrentAvailableStockModel(dataSet);
            }
            return null;
        }

        private dynamic BindFranchiseeCurrentAvailableStockModel(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            CurrentAvailableStockReportModel currentASModel = new CurrentAvailableStockReportModel();
            currentASModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            currentASModel.Address = dataTable.Rows[0]["address"].ToString();
            currentASModel.City = dataTable.Rows[0]["city"].ToString();
            currentASModel.State = dataTable.Rows[0]["state"].ToString();
            currentASModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            currentASModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<CurrentAvailableStockModel> currentAvailableStockModel = new List<CurrentAvailableStockModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                CurrentAvailableStockModel camodel = new CurrentAvailableStockModel();
                camodel.prod_name = Row["prod_name"].ToString();
                camodel.purchase_rate = Convert.ToSingle(Row["purchase_rate"]);
                camodel.quantity = Convert.ToSingle(Row["quantity"]);
                camodel.mrp = Convert.ToSingle(Row["mrp"]);
                camodel.online_sale_rate = Convert.ToSingle(Row["sale_rate"]);
                camodel.batch_no = Row["batch_no"].ToString();
                currentAvailableStockModel.Add(camodel);
            }
            currentASModel.CurrentASModel = currentAvailableStockModel;
            return currentASModel;
        }


        public dynamic FExpireWiseStock(uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iStockReportRepository.FExpireWiseStock(ProductId, fUserId);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                return BindFranchiseeExpireWiseStockModel(dataSet);
            }
            return null;
        }

        private dynamic BindFranchiseeExpireWiseStockModel(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            ExpireWiseStockReportModel expireWSModel = new ExpireWiseStockReportModel();
            expireWSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            expireWSModel.Address = dataTable.Rows[0]["address"].ToString();
            expireWSModel.City = dataTable.Rows[0]["city"].ToString();
            expireWSModel.State = dataTable.Rows[0]["state"].ToString();
            expireWSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            expireWSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<ExpireWiseStockModel> expireWiseStockModel = new List<ExpireWiseStockModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                ExpireWiseStockModel exmodel = new ExpireWiseStockModel();
                exmodel.prod_name = Row["prod_name"].ToString();
                exmodel.batch_no = Row["batch_no"].ToString();
                exmodel.expire_date = Row["expiry_date"].ToString();
                exmodel.stock_quantity = Row["quantity"].ToString();
                expireWiseStockModel.Add(exmodel);
            }
            expireWSModel.ExpireWSModel = expireWiseStockModel;
            return expireWSModel;
        }

        public DataTable GetFClosingStockReportListforExcel(string fromdate, uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iStockReportRepository.GetFClosingStockReportList(fromdate, ProductId, fUserId);
            if(dataSet.Tables[1].Rows.Count > 0)
            {
               DataTable dataTable = dataSet.Tables[1];
                return dataTable; 
            }
            return null;
        }

        public DataTable FCurrentAvailableStockforExcel(uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iStockReportRepository.FCurrentAvailableStock(ProductId, fUserId);
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[1];
                return dataTable;
            }
            return null;
        }

        public DataTable FExpireWiseStockforExcel(uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iStockReportRepository.FExpireWiseStock(ProductId, fUserId);
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[1];
                return dataTable;
            }
            return null;
        }
    }
}