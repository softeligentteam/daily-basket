﻿using DailyBasket.Warehouse.Core.Interface.BLL.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Core.Model.Report.Purchase;
using DailyBasket.Warehouse.Repository.Reports;
using DailyBasket.Warehouse.Helper;

namespace DailyBasket.Warehouse.Areas.Reports.BLL
{

    public class PurchaseReportBLL : IPurchaseReport
    {
        private Core.Interface.DAL.Reports.IPurchaseReport iPurchaseReportRepository;
        public PurchaseReportBLL()
        {
            iPurchaseReportRepository = new PurchaseReportRepository();
        }

        public BillWisePurchaseModel GetBillWiseReportList(string billno)
        {
            DataTable dataTable = iPurchaseReportRepository.GetBillWiseReportList(billno);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindBillWisePurchaseModel(dataTable);
            }
            return null;
        }

        public DataTable GetBillWiseReportListforExcel(string billno)
        {
            DataTable dataTable = iPurchaseReportRepository.GetBillWiseReportList(billno);
            dataTable.Columns["vendor_name"].SetOrdinal(0);
            return dataTable;
        }
        private BillWisePurchaseModel BindBillWisePurchaseModel(DataTable dataTable)
        {

            BillWisePurchaseModel billWisePurchaseModel = new BillWisePurchaseModel();
            billWisePurchaseModel.Bill_No = dataTable.Rows[0]["bill_no"].ToString();
            billWisePurchaseModel.Bill_Date = dataTable.Rows[0]["bill_date"].ToString();
            billWisePurchaseModel.Total_Bill_Amount = Convert.ToSingle(dataTable.Rows[0]["total_bill_amount"]);
            billWisePurchaseModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
            return billWisePurchaseModel;
        }


        public List<DateWisePurchaseModel> GetDayWisReportList(string fromdate, string todate)
        {
            DataTable dataTable = iPurchaseReportRepository.GetDayWisReportList(fromdate, todate);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindDayWisePurchaseModel(dataTable);
            }
            return null;

        }

        public DataTable GetDayWisReportListforExcel(string fromdate, string todate)
        {
            DataTable dataTable = new DataTable();
            dataTable = iPurchaseReportRepository.GetDayWisReportList(fromdate, todate);
            var bill_Amount = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));
            
            DataRow dataRow = dataTable.NewRow();
            dataRow[0] = "Grand Total";
            dataRow[1] = bill_Amount;
            dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
            return dataTable;
        }
        private List<DateWisePurchaseModel> BindDayWisePurchaseModel(DataTable dataTable)
        {
            List<DateWisePurchaseModel> dateWisePurchaseModel = new List<DateWisePurchaseModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                 DateWisePurchaseModel dtmodel = new DateWisePurchaseModel();
                dtmodel.Bill_Date = Row["bill_date"].ToString();
                dtmodel.Total_Bill_Amount = Convert.ToSingle(Row["total_bill_amount"]);
                dateWisePurchaseModel.Add(dtmodel);
            }

            return dateWisePurchaseModel;
        }

        public SelectList GetPurchaseReportList()
        {
            throw new NotImplementedException();
        }


        public List<VendorAndProductWisePurchaseModel> GetVendorWiseProductWiseList(string fromdate, string todate)
        {
            DataTable dataTable = iPurchaseReportRepository.GetVendorWiseProductWiseList(fromdate, todate);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindVendorWiseProductWisePurchaseModel(dataTable);
            }
            return null;
        }

        public DataTable GetVendorWiseProductWiseListforExcel(string fromdate, string todate)
        {
            DataTable dataTable = new DataTable();
            dataTable = iPurchaseReportRepository.GetVendorWiseProductWiseList(fromdate, todate);
            var quantityTotal = dataTable.AsEnumerable().Sum(x => x.Field<float>(4));
            var billAmount = dataTable.AsEnumerable().Sum(x => x.Field<double>(6));

            DataRow dataRow = dataTable.NewRow();
            dataRow[3] = "Quantity Total";
            dataRow[4] = quantityTotal;
            dataRow[5] = "Grand Total";
            dataRow[6] = billAmount;
            dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
            return dataTable;
        }
        private List<VendorAndProductWisePurchaseModel> BindVendorWiseProductWisePurchaseModel(DataTable dataTable)
        {


            List<VendorAndProductWisePurchaseModel> vendorAndProductWisePurchaseModel = new List<VendorAndProductWisePurchaseModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                VendorAndProductWisePurchaseModel vpmodel = new VendorAndProductWisePurchaseModel();
                vpmodel.Bill_No = Row["bill_no"].ToString();
                vpmodel.Bill_Date = Row["bill_date"].ToString();
                vpmodel.Vendor_Name =Row["vendor_name"].ToString();
                vpmodel.Vendor_Id = Convert.ToUInt64(Row["vendor_id"]);
                vpmodel.Product_Name =Row["prod_name"].ToString();
                vpmodel.Quantity = Convert.ToSingle(Row["quantity"]);
                vpmodel.Quantity_Type = Row["quantity_type"].ToString();
                vpmodel.Total_Bill_Amount = Convert.ToSingle(Row["total_amount"]);
                vendorAndProductWisePurchaseModel.Add(vpmodel);
            }

            return vendorAndProductWisePurchaseModel;
        }

        public List<VendorWisePurchaseModel> GetVendorWisePurchase()
        {
            DataTable dataTable = iPurchaseReportRepository.GetVendorWisePurchase();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindVendorWisePurchaseModel(dataTable);
            }
            return null;
        }

        public DataTable GetVendorWisePurchaseforExcel()
        {
            DataTable dataTable = new DataTable();
            dataTable = iPurchaseReportRepository.GetVendorWisePurchase();
            dataTable.Columns["vendor_name"].SetOrdinal(0);
            double subTotal = 0;
            var index = 0;
            var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(3));
            DataRow dataRow = dataTable.NewRow();

            var map = new Dictionary<string, ulong>();
            DataTable dataTableCloned = dataTable.Clone();
            foreach (DataRow row in dataTable.Rows)
            {
                dataTableCloned.ImportRow(row);
            }
            foreach (DataRow Row in dataTableCloned.Rows)
            {
                
                if (map.Count > 0 && map.ContainsValue(Convert.ToUInt64(Row["vendor_id"])) != true)
                {
                    map["vendor_Id"] = Convert.ToUInt64(Row["vendor_id"]);
                    DataRow dataRows = dataTable.NewRow();
                    dataRows[2] = "Sub Total";
                    dataRows[3] = subTotal;
                    dataTable.Rows.InsertAt(dataRows, index);
                    subTotal = 0;                    
                }
                else if (map.Count == 0)
                {
                    map["vendor_Id"] = Convert.ToUInt64(Row["vendor_id"]);
                }
                subTotal = subTotal + Convert.ToSingle(Row["Total_Bill_Amount"]);
                index++;
            }
            dataRow[2] = "Grand Total";
            dataRow[3] = grandTotal;
            dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
            return dataTable;
        }
        private List<VendorWisePurchaseModel> BindVendorWisePurchaseModel(DataTable dataTable)
        {
            List<VendorWisePurchaseModel> vendorWisePurchaseModel = new List<VendorWisePurchaseModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                VendorWisePurchaseModel vmodel = new VendorWisePurchaseModel();
                vmodel.Bill_No = Row["bill_no"].ToString();
                vmodel.Bill_Date = Row["bill_date"].ToString();
                vmodel.Vendor_Name =Row["vendor_name"].ToString();
                vmodel.Vendor_Id = Convert.ToUInt64(Row["vendor_id"]);
                vmodel.Total_Bill_Amount = Convert.ToSingle(Row["total_bill_amount"]);
                vendorWisePurchaseModel.Add(vmodel);
            }
            return vendorWisePurchaseModel;
        }

        public SelectList GetFranchiseeList()
        {
            return DropdownlistHelper.GetLongStringDropDownList(iPurchaseReportRepository.GetFranchiseeList());
        }

        //Franchisee Report Section
        public BillWisePurchaseModel GetFranchiseeBillWiseReportList(string billno)
        {
           DataTable dataTable = iPurchaseReportRepository.GetFranchiseeBillWiseReportList(billno);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindFranchiseeBillWisePurchaseModel(dataTable);
            }
            return null;
        }

        private BillWisePurchaseModel BindFranchiseeBillWisePurchaseModel(DataTable dataTable)
        {
            BillWisePurchaseModel billWisePurchaseModel = new BillWisePurchaseModel();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                billWisePurchaseModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
                billWisePurchaseModel.Address = dataTable.Rows[0]["address"].ToString();
                billWisePurchaseModel.City = dataTable.Rows[0]["city"].ToString();
                billWisePurchaseModel.State = dataTable.Rows[0]["state"].ToString();
                billWisePurchaseModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
                billWisePurchaseModel.Mobile = dataTable.Rows[0]["mobile"].ToString();
                billWisePurchaseModel.Bill_No = dataTable.Rows[0]["bill_no"].ToString();
                billWisePurchaseModel.Bill_Date = dataTable.Rows[0]["bill_date"].ToString();
                billWisePurchaseModel.Total_Bill_Amount = Convert.ToSingle(dataTable.Rows[0]["total_bill_amount"]);
                return billWisePurchaseModel;
            }
            return null;
        }

        public DataTable GetFranchiseeBillWiseReportListforExcel(string billno)
        {
            DataTable dataTable = iPurchaseReportRepository.GetFranchiseeBillWiseReportList(billno);
            return dataTable;
        }

        public dynamic GetFranchiseeDayWisReportList(string fromdate, string todate, ulong fUserId)
        {
            DataSet dataSet = iPurchaseReportRepository.GetFranchiseeDayWisReportList(fromdate, todate, fUserId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindFranchiseeDayWisePurchaseModel(dataSet);
            }
            return null;
        }

        private dynamic BindFranchiseeDayWisePurchaseModel(DataSet dataSet)
        {
            DataTable franchiseeUM = dataSet.Tables[0];
            DateWisePurchaseReportModel dateWPModel = new DateWisePurchaseReportModel();
            dateWPModel.FranchiseeName = franchiseeUM.Rows[0]["franchisee_name"].ToString();
            dateWPModel.Address = franchiseeUM.Rows[0]["address"].ToString();
            dateWPModel.City = franchiseeUM.Rows[0]["city"].ToString();
            dateWPModel.State = franchiseeUM.Rows[0]["state"].ToString();
            dateWPModel.GSTIN = franchiseeUM.Rows[0]["gstin"].ToString();
            dateWPModel.Mobile = franchiseeUM.Rows[0]["mobile"].ToString();

            DataTable dataTable = dataSet.Tables[1];
            List<DateWisePurchaseModel> dtmodel = new List<DateWisePurchaseModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                DateWisePurchaseModel dtwprModel = new DateWisePurchaseModel();
                dtwprModel.Bill_Date = Row["bill_date"].ToString();
                dtwprModel.Total_Bill_Amount = Convert.ToSingle(Row["total_bill_amount"]);
                dtmodel.Add(dtwprModel);
            }

            dateWPModel.DateWPModel = dtmodel;
            return dateWPModel;
        }

        public DataTable GetFranchiseeDayWisReportListforExcel(string fromdate, string todate, ulong fUserId)
        {
           DataSet dataSet = iPurchaseReportRepository.GetFranchiseeDayWisReportList(fromdate, todate, fUserId);
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[1];
                var bill_Amount = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));

                DataRow dataRow = dataTable.NewRow();
                dataRow[0] = "Grand Total";
                dataRow[1] = bill_Amount;
                dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
                return dataTable;
            }
            else
            {
                return null;
            }
        }
    }
}
