﻿using DailyBasket.Warehouse.Core.Interface.BLL.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using DailyBasket.Warehouse.Repository.Reports;

using DailyBasket.Warehouse.Areas.Reports.Models;
using System.Data;
using DailyBasket.Warehouse.Core.Model.Report.Sales;

namespace DailyBasket.Warehouse.Areas.Reports.BLL
{
    public class SalesReportBLL : ISalesReport
    {
        Core.Interface.DAL.Reports.ISalesReport iSalesReportRepository;
        public SalesReportBLL()
        {
            iSalesReportRepository = new SalesReportsRepository();
        }

        public List<BillWiseSalesModel> GetBillWiseSalesList(string fromdate, string todate)
        {
            DataTable dataTable = iSalesReportRepository.GetBillWiseSalesList(fromdate, todate);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindBillWiseSalesList(dataTable);
            }
            return null;
        }

        private List<BillWiseSalesModel> BindBillWiseSalesList(DataTable dataTable)
        {
            List<BillWiseSalesModel> billWiseSalesModel = new List<BillWiseSalesModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                BillWiseSalesModel billmodel = new BillWiseSalesModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                billmodel.First_Name = Row["first_name"].ToString();
                billmodel.Middle_Name = Row["middle_name"].ToString();
                billmodel.Last_Name = Row["last_name"].ToString();
                billmodel.Order_Date= Row["order_date"].ToString();
                billmodel.Corder_Id = Convert.ToUInt64(Row["corder_id"].ToString());
                billmodel.Total_Order_Amount = Convert.ToSingle(Row["total_order_amount"]);
                billWiseSalesModel.Add(billmodel);
            }

            return billWiseSalesModel;
        }
        public DataTable GetBillWiseSalesListForExcel(string fromdate, string todate)
        {
            DataTable dataTable = new DataTable();
            dataTable = iSalesReportRepository.GetBillWiseSalesList(fromdate, todate);
            var total_order_amount = dataTable.AsEnumerable().Sum(x => x.Field<double>(5));
          
            dataTable.Columns["corder_id"].SetOrdinal(3);
            DataRow dataRow = dataTable.NewRow();
            dataRow[4] = "Grand Total";
            dataRow[5] = total_order_amount;
            dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
            return dataTable;
        }
      
        public List<BrandWiseSalesModel> GetBrandWiseSalesList(uint brandid)
        {
            DataTable dataTable = iSalesReportRepository.GetBrandWiseSalesList(brandid);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindBrandWiseSalesList(dataTable);
            }
            return null;
        }

        public DataTable GetBrandWiseSalesListforExcel(uint brandid)
        {
            DataTable dataTable = new DataTable();
            dataTable = iSalesReportRepository.GetBrandWiseSalesList(brandid);
            var quantityTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));
            var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(2));

            DataRow dataRowQuantity = dataTable.NewRow();
            dataRowQuantity[0] = "Quantity Total";
            dataRowQuantity[1] = quantityTotal;
            dataTable.Rows.InsertAt(dataRowQuantity, dataTable.Rows.Count);

            DataRow dataRowTotal = dataTable.NewRow();
            dataRowTotal[0] = "Grand Total";
            dataRowTotal[1] = grandTotal;
            dataTable.Rows.InsertAt(dataRowTotal, dataTable.Rows.Count+1);
            return dataTable;

        }
        private List<BrandWiseSalesModel> BindBrandWiseSalesList(DataTable dataTable)
        {


            List<BrandWiseSalesModel> brandWiseSalesModel = new List<BrandWiseSalesModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                BrandWiseSalesModel bmodel = new BrandWiseSalesModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                bmodel.Brand = Row["brand"].ToString();
                bmodel.Quantity = Convert.ToSingle(Row["quantity"]);
                bmodel.Total_Amount = Convert.ToSingle(Row["total_amount"]);
                brandWiseSalesModel.Add(bmodel);
            }

            return brandWiseSalesModel;
        }
        public List<DateWiseCustomerOrderSalesModel> GetDateWiseCustomerOrderList(string fromdate, string todate)
        {
            DataTable dataTable = iSalesReportRepository.GetDateWiseCustomerOrderList(fromdate, todate);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindDateWiseCustomerOrderSalesModel(dataTable);
            }
            return null;
        }

        public DataTable GetDateWiseCustomerOrderListforExcel(string fromdate, string todate)
        {
            DataTable dataTable = new DataTable();
            dataTable = iSalesReportRepository.GetDateWiseCustomerOrderList(fromdate, todate);
            var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));

            DataRow dataRowQuantity = dataTable.NewRow();
            dataRowQuantity[0] = "Grand Total";
            dataRowQuantity[1] = grandTotal;
            dataTable.Rows.InsertAt(dataRowQuantity, dataTable.Rows.Count);

            return dataTable;
        }
        private List<DateWiseCustomerOrderSalesModel> BindDateWiseCustomerOrderSalesModel(DataTable dataTable)
        {


            List<DateWiseCustomerOrderSalesModel> dateWiseCustomerOrderSalesModel = new List<DateWiseCustomerOrderSalesModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                DateWiseCustomerOrderSalesModel dcomodel = new DateWiseCustomerOrderSalesModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                dcomodel.order_date = Row["order_date"].ToString();
                dcomodel.total_order_amount = Convert.ToSingle(Row["total_order_amount"]);

                dateWiseCustomerOrderSalesModel.Add(dcomodel);
            }

            return dateWiseCustomerOrderSalesModel;
        }

        public List<MainCategoryWiseSalesModel> GetMainCategoryWiseSalesList(uint MainCategoryId)
        {
            DataTable dataTable = iSalesReportRepository.GetMainCategoryWiseSalesList(MainCategoryId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindMainCategoryWiseSalesList(dataTable);
            }
            return null;
        }

        public DataTable GetMainCategoryWiseSalesListforExcel(uint MainCategoryId)
        {
            DataTable dataTable = new DataTable();
            dataTable = iSalesReportRepository.GetMainCategoryWiseSalesList(MainCategoryId);
            var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(2));
            DataTable dataTableCloned = dataTable.Clone();
            dataTableCloned.Columns[1].DataType = typeof(string);
            foreach (DataRow row in dataTable.Rows)
            {
                dataTableCloned.ImportRow(row);
            }

            DataRow dataRow = dataTableCloned.NewRow();
            dataRow[1] = "Grand Total";
            dataRow[2] = grandTotal;
            dataTableCloned.Rows.InsertAt(dataRow, dataTableCloned.Rows.Count);

            return dataTableCloned;
        }
        private List<MainCategoryWiseSalesModel> BindMainCategoryWiseSalesList(DataTable dataTable)
        {


            List<MainCategoryWiseSalesModel> mainCategoryWiseSalesModel = new List<MainCategoryWiseSalesModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                MainCategoryWiseSalesModel mcmodel = new MainCategoryWiseSalesModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                mcmodel.MainCategory = Row["main_category"].ToString();
               
                mcmodel.Quantity = Convert.ToSingle(Row["quantity"]);
                
                mcmodel.total_order_amount = Convert.ToSingle(Row["total_amount"]);
                mainCategoryWiseSalesModel.Add(mcmodel);
            }

            return mainCategoryWiseSalesModel;
        }
        public List<ProductWiseSalesModel> GetProductWiseSalesReportList(string fromdate, string todate, uint ProductId)
        {
            DataTable dataTable = iSalesReportRepository.GetProductWiseSalesReportList(fromdate, todate, ProductId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindProductWiseSalesList(dataTable);
            }
            return null;
        }

        public DataTable GetProductWiseSalesReportListforExcel(string fromdate, string todate, uint ProductId)
        {
            return iSalesReportRepository.GetProductWiseSalesReportList(fromdate, todate, ProductId);
        }
        private List<ProductWiseSalesModel> BindProductWiseSalesList(DataTable dataTable)
        {
            List<ProductWiseSalesModel> productWiseSalesModel = new List<ProductWiseSalesModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                ProductWiseSalesModel pwmodel = new ProductWiseSalesModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                pwmodel.corder_id= Convert.ToUInt64(Row["corder_id"].ToString());
                pwmodel.prod_name = Row["prod_name"].ToString();
                pwmodel.os_id = Convert.ToUInt32(Row["os_id"].ToString());
                pwmodel.order_date= Row["order_date"].ToString();
                pwmodel.first_name= Row["first_name"].ToString();
                pwmodel.middle_name = Row["middle_name"].ToString();
                pwmodel.last_name = Row["last_name"].ToString();
                pwmodel.mobile= Row["mobile"].ToString();
                pwmodel.quantity = Convert.ToSingle(Row["quantity"]);
                pwmodel.sale_rate = Convert.ToSingle(Row["sale_rate"]);
                pwmodel.gst_amount= Convert.ToSingle(Row["gst"]);
                pwmodel.mrp = Convert.ToSingle(Row["mrp"]);
                pwmodel.batch_no= Row["batch_no"].ToString();
                productWiseSalesModel.Add(pwmodel);
            }

            return productWiseSalesModel;
        }

        public SelectList GetSalesReportList()
        {
            throw new NotImplementedException();
        }

        public List<SubCategoryWiseSalesModel> GetSubCategoryWiseSalesList(uint SubCategoryId)
        {
            DataTable dataTable = iSalesReportRepository.GetSubCategoryWiseSalesList(SubCategoryId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindSubCategoryWiseSalesList(dataTable);
            }
            return null;
        }

        public DataTable GetSubCategoryWiseSalesListforExcel(uint SubCategoryId)
        {
            DataTable dataTable = new DataTable();
            dataTable = iSalesReportRepository.GetSubCategoryWiseSalesList(SubCategoryId);
            var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(2));
            DataTable dataTableCloned = dataTable.Clone();
            dataTableCloned.Columns[1].DataType = typeof(string);
            foreach (DataRow row in dataTable.Rows)
            {
                dataTableCloned.ImportRow(row);
            }

            DataRow dataRow = dataTableCloned.NewRow();
            dataRow[1] = "Grand Total";
            dataRow[2] = grandTotal;
            dataTableCloned.Rows.InsertAt(dataRow, dataTableCloned.Rows.Count);

            return dataTableCloned;
        }
        private List<SubCategoryWiseSalesModel> BindSubCategoryWiseSalesList(DataTable dataTable)
        {
            List<SubCategoryWiseSalesModel> subCategoryWiseSalesModel = new List<SubCategoryWiseSalesModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                SubCategoryWiseSalesModel scmodel = new SubCategoryWiseSalesModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                scmodel.sub_category = Row["sub_category"].ToString();
                scmodel.quantity = Convert.ToSingle(Row["quantity"]);
                scmodel.total_amount = Convert.ToSingle(Row["total_amount"]);
                subCategoryWiseSalesModel.Add(scmodel);
            }

            return subCategoryWiseSalesModel;
        }

     
        public dynamic GetFDateWiseCustomerOrderList(string fromdate, string todate, ulong fUserId)
        {
            DataSet dataSet = iSalesReportRepository.GetFDateWiseCustomerOrderList(fromdate, todate, fUserId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindFranchiseeDateWiseCustomerOrderSalesModel(dataSet);
            }
            return null;
        }
        
        private dynamic BindFranchiseeDateWiseCustomerOrderSalesModel(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            DateWiseCustomerOrderSalesReportModel dateWiseCOSModel = new DateWiseCustomerOrderSalesReportModel();
            dateWiseCOSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            dateWiseCOSModel.Address = dataTable.Rows[0]["address"].ToString();
            dateWiseCOSModel.City = dataTable.Rows[0]["city"].ToString();
            dateWiseCOSModel.State = dataTable.Rows[0]["state"].ToString();
            dateWiseCOSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            dateWiseCOSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<DateWiseCustomerOrderSalesModel> dateWiseCustomerOrderSalesModel = new List<DateWiseCustomerOrderSalesModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                DateWiseCustomerOrderSalesModel dcomodel = new DateWiseCustomerOrderSalesModel();
                dcomodel.order_date = Row["order_date"].ToString();
                dcomodel.total_order_amount = Convert.ToSingle(Row["total_order_amount"]);

                dateWiseCustomerOrderSalesModel.Add(dcomodel);
            }
            dateWiseCOSModel.DateWCOSModel = dateWiseCustomerOrderSalesModel;
            return dateWiseCOSModel;
        }
        public dynamic GetFBillWiseSalesList(string fromdate, string todate, ulong fUserId)
        {
            DataSet dataSet = iSalesReportRepository.GetFBillWiseSalesList(fromdate, todate, fUserId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindFranchiseeBillWiseSalesList(dataSet);
            }
            return null;
        }

        private dynamic BindFranchiseeBillWiseSalesList(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            BillWiseSalesReportModel billWiseSalesReportModel = new BillWiseSalesReportModel();
            billWiseSalesReportModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            billWiseSalesReportModel.Address = dataTable.Rows[0]["address"].ToString();
            billWiseSalesReportModel.City = dataTable.Rows[0]["city"].ToString();
            billWiseSalesReportModel.State = dataTable.Rows[0]["state"].ToString();
            billWiseSalesReportModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            billWiseSalesReportModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<BillWiseSalesModel> billWSalesModel = new List<BillWiseSalesModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                BillWiseSalesModel billModel = new BillWiseSalesModel();
                billModel.First_Name = Row["first_name"].ToString();
                billModel.Middle_Name = Row["middle_name"].ToString();
                billModel.Last_Name = Row["last_name"].ToString();
                billModel.Order_Date = Row["order_date"].ToString();
                billModel.Corder_Id = Convert.ToUInt64(Row["corder_id"].ToString());
                billModel.Total_Order_Amount = Convert.ToSingle(Row["total_order_amount"]);
                billWSalesModel.Add(billModel);
            }
            billWiseSalesReportModel.BillWSModel = billWSalesModel;
            return billWiseSalesReportModel;
        }

        public dynamic GetFProductWiseSalesReportList(string fromdate, string todate, uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iSalesReportRepository.GetFProductWiseSalesReportList(fromdate, todate, ProductId, fUserId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindFranchiseeProductWiseSalesList(dataSet);
            }
            return null;
        }

        private dynamic BindFranchiseeProductWiseSalesList(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            ProductWiseSalesReportModel productWSModel = new ProductWiseSalesReportModel();
            productWSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            productWSModel.Address = dataTable.Rows[0]["address"].ToString();
            productWSModel.City = dataTable.Rows[0]["city"].ToString();
            productWSModel.State = dataTable.Rows[0]["state"].ToString();
            productWSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            productWSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();


            List<ProductWiseSalesModel> productWiseSalesModel = new List<ProductWiseSalesModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                ProductWiseSalesModel pwmodel = new ProductWiseSalesModel();
                pwmodel.corder_id = Convert.ToUInt64(Row["corder_id"].ToString());
                pwmodel.prod_name = Row["prod_name"].ToString();
                pwmodel.os_id = Convert.ToUInt32(Row["os_id"].ToString());
                pwmodel.order_date = Row["order_date"].ToString();
                pwmodel.first_name = Row["first_name"].ToString();
                pwmodel.middle_name = Row["middle_name"].ToString();
                pwmodel.last_name = Row["last_name"].ToString();
                pwmodel.mobile = Row["mobile"].ToString();
                pwmodel.quantity = Convert.ToSingle(Row["quantity"]);
                pwmodel.sale_rate = Convert.ToSingle(Row["sale_rate"]);
                pwmodel.gst_amount = Convert.ToSingle(Row["gst"]);
                pwmodel.mrp = Convert.ToSingle(Row["mrp"]);
                pwmodel.batch_no = Row["batch_no"].ToString();
                productWiseSalesModel.Add(pwmodel);
            }
            productWSModel.ProductWSModel = productWiseSalesModel;
            return productWSModel;
        }


        public DataTable GetFBillWiseSalesListForExcel(string fromdate, string todate, ulong fUserId)
        {
            DataSet dataSet = iSalesReportRepository.GetFBillWiseSalesList(fromdate, todate, fUserId);
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[1];
                var total_order_amount = dataTable.AsEnumerable().Sum(x => x.Field<double>(5));

                dataTable.Columns["corder_id"].SetOrdinal(3);
                DataRow dataRow = dataTable.NewRow();
                dataRow[4] = "Grand Total";
                dataRow[5] = total_order_amount;
                dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
                return dataTable;
            }
            else
            {
                return null;
            }
        }

        public DataTable GetFDateWiseCustomerOrderListforExcel(string fromdate, string todate, ulong fUserId)
        {
            DataSet dataSet = iSalesReportRepository.GetFDateWiseCustomerOrderList(fromdate, todate, fUserId);

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[1];
                var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));

                DataRow dataRowQuantity = dataTable.NewRow();
                dataRowQuantity[0] = "Grand Total";
                dataRowQuantity[1] = grandTotal;
                dataTable.Rows.InsertAt(dataRowQuantity, dataTable.Rows.Count);
                return dataTable;
            }
            else
            {
                return null;
            }
        }

        public DataTable GetFProductWiseSalesReportListforExcel(string fromdate, string todate, uint ProductId, ulong fUserId)
        {
            DataSet dataSet = iSalesReportRepository.GetFProductWiseSalesReportList(fromdate, todate, ProductId, fUserId);
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[1];
                return dataTable;
            }
            else
            {
                return null;
            }
        }
    }
}