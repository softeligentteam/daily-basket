﻿using DailyBasket.Warehouse.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Reports.Models.ViewModels
{
    public class StockReportList
    {
        public StockReport Report { get; set; }
        public string From_Date { get; set; }
        public ulong FranchiseeId { get; set; }
    }
}