﻿using DailyBasket.Warehouse.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Reports.Models.ViewModels
{
    public class SalesReportList
    {
        public int salesId { get; set; }
        public SalesReports Report { get; set; }
        public FranchiseeSalesReports FReport { get; set; }
        public string From_Date { get; set; }
        public string To_Date { get; set; }
        public string Brand{ get; set; }
        public string Maincategory { get; set; }
        public string Subcategory { get; set; }
        public ulong FranchiseeId { get; set; }
    }
}