using DailyBasket.Warehouse.Core.Interface.BLL.Purchase;
using System;
using System.Collections.Generic;
using DailyBasket.Warehouse.Core.Model.Purchase;
using DailyBasket.Warehouse.Repository.Purchase;
using System.Data;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.GST;
using DailyBasket.Warehouse.Helper;
using DailyBasket.Warehouse.Areas.Purchase.Models.ViewModels;
using System.Collections;

namespace DailyBasket.Warehouse.Areas.Purchase.BLL
{
    public class PurchaseProductBLL : IPurchaseProduct
    {
        Core.Interface.DAL.Purchase.IPurchaseProduct iPurchaseProductRepository;
        public PurchaseProductBLL()
        {
            iPurchaseProductRepository = new PurchaseProductRepository();
        }

        public ulong AddPurchaseOrder(PurchaseOrderModel purchaseOrderModel, ulong userId)
        {
            return iPurchaseProductRepository.AddPurchaseOrder(purchaseOrderModel, userId);
        }

        public PurchaseOrderModel GetPurchaseOrderToAddProduct(ulong PurchaseOrderId)
        {
            DataTable dataTable = iPurchaseProductRepository.GetPurchaseOrderToAddProduct(PurchaseOrderId);
            if (dataTable != null)
            {
                PurchaseOrderModel poModel = new PurchaseOrderModel();
                poModel.Id = Convert.ToUInt64(dataTable.Rows[0]["po_id"].ToString());
                poModel.BillDate = dataTable.Rows[0]["bill_date"].ToString();
                poModel.FillingDate = dataTable.Rows[0]["filling_date"].ToString();
                poModel.BillNo = dataTable.Rows[0]["bill_no"].ToString();
                poModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
                poModel.BillDiscountAmount = Convert.ToSingle(dataTable.Rows[0]["bill_discount_amount"].ToString());
                poModel.TotalBillAmount = Convert.ToDouble(dataTable.Rows[0]["total_bill_amount"].ToString());
                poModel.SubmitType = (SubmitType)Enum.Parse(typeof(SubmitType), dataTable.Rows[0]["submit_type"].ToString());
                return poModel;
            }
            return null;
        }

        public PurchaseOrderModel GetPurchaseOrderDetails(ulong PurchaseOrderId)
        {
            throw new NotImplementedException();
        }

        public List<POProductModel> GetPurchaseOrderProducts(ulong PurchaseOrderId)
        {
            throw new NotImplementedException();
        }

        public int AddProduct(PurchaseEntryModel purchaseEntryModel, ulong userId)
        {
            purchaseEntryModel.TotalAmount = (purchaseEntryModel.PurchaseRate * purchaseEntryModel.Quantity);
            GSTAmountModel gstAmountModel = new GstCalcHelper().CalculateProductGst(purchaseEntryModel.ProductId, purchaseEntryModel.TotalAmount);
            if (gstAmountModel != null)
                return iPurchaseProductRepository.AddProduct(purchaseEntryModel, gstAmountModel, userId);
            return 0;
        }

        public int SubmitPurchaseOrder(ulong PurchaseOrderId, ulong userId)
        {
            DataTable dataTable = iPurchaseProductRepository.GetPurchaseOrderToCalculate(PurchaseOrderId, SubmitType.Temperary);
            if (dataTable != null)
            {
                DataTable dtProducts = iPurchaseProductRepository.GetPurchaseOrderProductsToCalculate(PurchaseOrderId, SubmitType.Temperary);
                if (dtProducts != null)
                {
                    PurchaseOrderModel purchaseOrderModel = new PurchaseOrderModel();
                    purchaseOrderModel.Id = PurchaseOrderId;
                    foreach (DataRow row in dtProducts.Rows)
                    {
                        purchaseOrderModel.SGSTAmount = purchaseOrderModel.SGSTAmount + Convert.ToSingle(row["sgst_amount"].ToString());
                        purchaseOrderModel.CGSTAmount = purchaseOrderModel.CGSTAmount + Convert.ToSingle(row["cgst_amount"].ToString());
                        purchaseOrderModel.IGSTAmount = purchaseOrderModel.IGSTAmount + Convert.ToSingle(row["igst_amount"].ToString());
                        purchaseOrderModel.TotalPurchaseAmount = purchaseOrderModel.TotalPurchaseAmount + Convert.ToDouble(row["total_amount"].ToString());
                    }
                    purchaseOrderModel.TotalBillAmount = purchaseOrderModel.TotalPurchaseAmount - Convert.ToSingle(dataTable.Rows[0]["bill_discount_amount"].ToString());
                    purchaseOrderModel.SubmitType = SubmitType.Permanent;
                    return iPurchaseProductRepository.SubmitPurchaseOrder(purchaseOrderModel, userId);
                }
            }
            return 0;
        }

        public List<PurchaseOrderModel> GetPendingPurchaseOrders()
        {
            DataTable dataTable = iPurchaseProductRepository.GetPurchaseOrders(SubmitType.Temperary);
            if (dataTable != null)
            {
                return BindPurchaseOrders(dataTable);
            }
            return null;
        }

        public List<PurchaseOrderModel> GetCompletedPurchaseOrders()
        {
            DataTable dataTable = iPurchaseProductRepository.GetPurchaseOrders(SubmitType.Permanent);
            if (dataTable != null)
            {
                return BindPurchaseOrders(dataTable);
            }
            return null;
        }

        private List<PurchaseOrderModel> BindPurchaseOrders(DataTable dataTable)
        {
            List<PurchaseOrderModel> purchaseOrdderList = new List<PurchaseOrderModel>();
            foreach (DataRow row in dataTable.Rows)
            {
                PurchaseOrderModel poModel = new PurchaseOrderModel();
                poModel.Id = Convert.ToUInt64(row["po_id"].ToString());
                poModel.BillDate = row["bill_date"].ToString();
                poModel.FillingDate = row["filling_date"].ToString();
                poModel.BillNo = row["bill_no"].ToString();
                poModel.VendorName = row["vendor_name"].ToString();
                purchaseOrdderList.Add(poModel);
            }
            return purchaseOrdderList;
        }

        public dynamic GetPendingPurchaseOrder(ulong PurchaseOrderId)
        {
            DataSet dataSet = iPurchaseProductRepository.GetPurchaseOrderWithProcucts(PurchaseOrderId, SubmitType.Temperary);
            if (dataSet.Tables[0].Rows.Count > 0)
            {
                return BindPurchaseOrderWithProducts(dataSet);
            }
            return null;
        }

        public dynamic GetCompletedPurchaseOrder(ulong PurchaseOrderId)
        {
            DataSet dataSet = iPurchaseProductRepository.GetPurchaseOrderWithProcucts(PurchaseOrderId, SubmitType.Permanent);
            if (dataSet.Tables[0].Rows.Count > 0)
            {
                return BindPurchaseOrderWithProducts(dataSet);
            }
            return null;
        }

        private PurchaseOrderViewModel BindPurchaseOrderWithProducts(DataSet dataSet)
        {
            DataTable ppoDatatable = dataSet.Tables[0];
            PurchaseOrderViewModel purchaseOrderViewModel = new PurchaseOrderViewModel();
            purchaseOrderViewModel.PurchaseOrderId = Convert.ToUInt64(ppoDatatable.Rows[0]["po_id"].ToString());
            purchaseOrderViewModel.VendorName = ppoDatatable.Rows[0]["vendor_name"].ToString();
            purchaseOrderViewModel.BillNo = ppoDatatable.Rows[0]["bill_no"].ToString();
            purchaseOrderViewModel.BillDate = ppoDatatable.Rows[0]["bill_date"].ToString();
            purchaseOrderViewModel.FillingDate = ppoDatatable.Rows[0]["filling_date"].ToString();
            purchaseOrderViewModel.BillDiscountAmount = Convert.ToSingle(ppoDatatable.Rows[0]["bill_discount_amount"].ToString());
            purchaseOrderViewModel.TotalBillAmount = Convert.ToDouble(ppoDatatable.Rows[0]["total_bill_amount"].ToString());
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<PurchaseProductViewModel> purchaseProductList = new List<PurchaseProductViewModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                {
                    //pop.mrp, pop., pop.rack_no
                    PurchaseProductViewModel ppViewModel = new PurchaseProductViewModel();
                    ppViewModel.Id = Convert.ToUInt64(row["po_prod_id"].ToString());
                    ppViewModel.StockProductId = Convert.ToUInt64(row["sp_id"]);
                    ppViewModel.ProductId = Convert.ToUInt64(row["prod_id"].ToString());
                    ppViewModel.ProductName = row["prod_name"].ToString();
                    ppViewModel.Quantity = Convert.ToSingle(row["quantity"].ToString());
                    ppViewModel.QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString());
                    ppViewModel.ProductName = row["prod_name"].ToString();
                    ppViewModel.PurchaseRate = Convert.ToSingle(row["purchase_rate"].ToString());
                    ppViewModel.MRP = Convert.ToSingle(row["mrp"].ToString());
                    ppViewModel.TotalAmount = Convert.ToSingle(row["total_amount"].ToString());
                    ppViewModel.RackNo = row["rack_no"].ToString();
                    purchaseOrderViewModel.TotalProductAmount = purchaseOrderViewModel.TotalProductAmount + ppViewModel.TotalAmount;
                    purchaseProductList.Add(ppViewModel);
                }
                purchaseOrderViewModel.TotalProductCount = Convert.ToUInt16(dataSet.Tables[1].Rows.Count);
                purchaseOrderViewModel.purchaseProducts = purchaseProductList;
            }
            return purchaseOrderViewModel;
        }

        public ArrayList GetProductTotalCountandAmount(ulong poid)
        {
            DataTable dataTable = iPurchaseProductRepository.GetProductTotalCountandAmount(poid);
            ArrayList productData = new ArrayList();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                productData.Add(Convert.ToInt16(dataTable.Rows[0]["TotalProducts"].ToString()));
                productData.Add(Convert.ToDouble(dataTable.Rows[0]["TotalProductsAmount"].ToString()));
            }
            else
            {
                productData.Add(0);
                productData.Add(0);
            }
            return productData;
        }

        public PurchaseEntryModel GetPurchaseOrderProductToUpdate(ulong poId = 0, ulong poProdId = 0, ulong spId = 0)
        {
            if (poId != 0 && poProdId != 0 && spId != 0)
            {
                DataTable dataTable = iPurchaseProductRepository.GetPurchaseOrderProductToUpdate(poId, poProdId, spId);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    PurchaseEntryModel purchaseEntryModel = new PurchaseEntryModel();
                    purchaseEntryModel.PurchaseOrderId = poId;
                    purchaseEntryModel.PopId = poProdId;
                    purchaseEntryModel.SpId = spId;
                    purchaseEntryModel.SelectProductId = dataTable.Rows[0]["prodname"].ToString();
                    purchaseEntryModel.Quantity = Convert.ToUInt16(dataTable.Rows[0]["quantity"]);
                    purchaseEntryModel.PurchaseRate = Convert.ToSingle(dataTable.Rows[0]["purchaserate"]);
                    purchaseEntryModel.MRP = Convert.ToSingle(dataTable.Rows[0]["mrp"]);
                    purchaseEntryModel.RackNo = dataTable.Rows[0]["rackno"].ToString();
                    purchaseEntryModel.Remark = dataTable.Rows[0]["remark"].ToString();
                    purchaseEntryModel.OnlineSaleRate = Convert.ToSingle(dataTable.Rows[0]["onlinesalerate"]);
                    purchaseEntryModel.SaleRateA = Convert.ToSingle(dataTable.Rows[0]["saleratea"]);
                    purchaseEntryModel.SaleRateB = Convert.ToSingle(dataTable.Rows[0]["salerateb"]);
                    purchaseEntryModel.ProductBarcodeId = dataTable.Rows[0]["barcode"].ToString();
                    purchaseEntryModel.BatchNo = dataTable.Rows[0]["batchno"].ToString();
                    purchaseEntryModel.ExpiryDate = dataTable.Rows[0]["expirydate"].ToString();
                    purchaseEntryModel.PackagingDate = dataTable.Rows[0]["packagingdate"].ToString();
                    purchaseEntryModel.AutoGenerateBarcode = Convert.ToBoolean(dataTable.Rows[0]["is_autogenerated_barcode"]);

                    return purchaseEntryModel;
                }
            }
            return null;
        }

        public int UpdatePurchaseOrderProduct(PurchaseEntryModel purchaseEntryModel, ulong userId)
        {
            purchaseEntryModel.TotalAmount = (purchaseEntryModel.PurchaseRate * purchaseEntryModel.Quantity);
            GSTAmountModel gstAmountModel = new GstCalcHelper().CalculateProductGst(purchaseEntryModel.ProductId, purchaseEntryModel.TotalAmount);
            if (gstAmountModel != null)
            {
                return iPurchaseProductRepository.UpdatePurchaseOrderProduct(purchaseEntryModel, gstAmountModel, userId);
            }
            return 0;
        }

        public PurchaseOrderModel GetPendingPurchaseOrderToUpdate(ulong purchaseOrderId)
        {
            DataTable dataTable = iPurchaseProductRepository.GetPendingPurchaseOrderToUpdate(purchaseOrderId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                PurchaseOrderModel purchaseOrderModel = new PurchaseOrderModel();
                purchaseOrderModel.Id = Convert.ToUInt64(dataTable.Rows[0]["po_id"].ToString());
                purchaseOrderModel.BillNo = dataTable.Rows[0]["bill_no"].ToString();
                purchaseOrderModel.BillDate = dataTable.Rows[0]["bill_date"].ToString();
                purchaseOrderModel.FillingDate = dataTable.Rows[0]["filling_date"].ToString();
                purchaseOrderModel.VendorId = Convert.ToUInt32(dataTable.Rows[0]["vendor_id"].ToString());
                purchaseOrderModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
                purchaseOrderModel.BillDiscountAmount = Convert.ToSingle(dataTable.Rows[0]["bill_discount_amount"].ToString());
                purchaseOrderModel.ProductCount = Convert.ToUInt16(dataTable.Rows[0]["product_count"].ToString());
                purchaseOrderModel.TotalPurchaseAmount = Convert.ToDouble(dataTable.Rows[0]["total_purchase_amount"].ToString()); // Verification Purpose only - Don't Update
                purchaseOrderModel.TotalBillAmount = Convert.ToDouble(dataTable.Rows[0]["total_bill_amount"].ToString());
                return purchaseOrderModel;
            }
            return null;

        }

        public int UpdatePendingPurchaseOrder(PurchaseOrderModel purchaseOrderModel, ulong userId)
        {
            return iPurchaseProductRepository.UpdatePendingPurchaseOrder(purchaseOrderModel, userId);
        }
    }
}