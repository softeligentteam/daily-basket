﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Areas.Purchase.BLL;
using DailyBasket.Warehouse.Areas.Purchase.Models.ViewModels;
using DailyBasket.Warehouse.Core.Interface.BLL.Purchase;
using DailyBasket.Warehouse.Core.Model.Purchase;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Purchase.Controllers
{
    [Authorize]
    public class PurchaseOrderController : Controller
    {
        private IPurchaseProduct iPurchaseProduct = new PurchaseProductBLL();
        // GET: Purchase/PurchaseOrder
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPurchaseOrder()
        {
            ViewBag.VendorDdlList = new VendorBLL().GetActiveVendorListDdl();
            return View();
        }

        [HttpPost]
        public ActionResult AddPurchaseOrder(PurchaseOrderModel purchaseOrderModel)
        {
            if (ModelState.IsValid)
            {
                var poid = iPurchaseProduct.AddPurchaseOrder(purchaseOrderModel, SessionManager.CurrentUserId);
                if (poid != 0)
                {
                    if (SessionManager.SetPurchaseOrderCookie(poid))
                    {
                        return RedirectToAction("AddPurchaseProduct");
                    }
                    TempData["Message"] = "Oops error Occured, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
                else
                {
                    TempData["Message"] = "Purchase Order is not created successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            ViewBag.VendorDdlList = new VendorBLL().GetActiveVendorListDdl();
            return View();
        }

        [HttpGet]
        public ActionResult AddPurchaseProduct(ulong poId = 0)
        {
            if (poId != 0)
            {
                if (SessionManager.SetPurchaseOrderCookie(poId))
                {
                    ArrayList purchaseEntryModel = iPurchaseProduct.GetProductTotalCountandAmount(poId);
                    int Count = Convert.ToInt16(purchaseEntryModel[0]);
                    double Amount = Convert.ToDouble(purchaseEntryModel[1]);
                    SessionManager.UpdatePendingPurchaseOrderCookie(Count, Amount);
                    BindPurchaseProductViewDdl();
                    return View();
                }
                SessionManager.UnsetPurchaseOrderCookie();
                return RedirectToAction("PendingPurchaseEntries");
            }

            if (SessionManager.IsSetPurchaseOrderCookie())
            {
                BindPurchaseProductViewDdl();
                return View();
            }
            return RedirectToAction("AddPurchaseOrder");
        }

        [HttpPost]
        public ActionResult AddPurchaseProduct(PurchaseEntryModel purchaseEntryModel, string submit)
        {
            if (ModelState.IsValid)
            {
                if (purchaseEntryModel.AutoGenerateBarcode == true)
                {
                    purchaseEntryModel.ProductBarcodeId = BarcodeHelper.AutoGenerateBarcode(purchaseEntryModel.ProductId);
                }
                if (iPurchaseProduct.AddProduct(purchaseEntryModel, SessionManager.CurrentUserId) != 0)
                {
                    switch (submit)
                    {
                        case "Add Next Product":
                            if (!SessionManager.IsSetPurchaseOrderCookie())
                            {
                                SessionManager.SetPurchaseOrderCookie(purchaseEntryModel.PurchaseOrderId);
                            }
                            TempData["Message"] = "Purchase Product is added successfully.";
                            TempData["CssClass"] = "text-success";
                            SessionManager.UpdatePurchaseOrderCookie(purchaseEntryModel.TotalAmount);
                            return RedirectToAction("AddPurchaseProduct");
                        case "Submit Order":
                            if (iPurchaseProduct.SubmitPurchaseOrder(purchaseEntryModel.PurchaseOrderId, SessionManager.CurrentUserId) != 0)
                            {
                                SessionManager.UnsetPurchaseOrderCookie();
                            }
                            return RedirectToAction("ViewPurchaseOrder", new { poId = purchaseEntryModel.PurchaseOrderId });
                        default:
                            SessionManager.UnsetPurchaseOrderCookie();
                            return RedirectToAction("AddPurchaseOrder");
                    }
                }
            }
            BindPurchaseProductViewDdl();
            return View();
        }

        private void BindPurchaseProductViewDdl()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
        }

        [HttpGet]
        public ActionResult UpdatePurchaseProduct(ulong poId = 0, ulong poProdId = 0, ulong spId = 0)
        {
            if (poId != 0 & poProdId != 0 & spId != 0)
            {
                if (SessionManager.SetPurchaseOrderCookie(poId))
                {
                    ArrayList purchaseEntryModel = iPurchaseProduct.GetProductTotalCountandAmount(poId);
                    int Count = Convert.ToInt16(purchaseEntryModel[0]);
                    double Amount = Convert.ToDouble(purchaseEntryModel[1]);
                    SessionManager.UpdatePendingPurchaseOrderCookie(Count, Amount);
                    BindPurchaseProductViewDdl();

                    PurchaseEntryModel purchaseEM = iPurchaseProduct.GetPurchaseOrderProductToUpdate(poId, poProdId, spId);
                    //purchaseEM.ProductId = Convert.ToUInt16(prodId);
                    return View("UpdatePurchaseOrderProduct", purchaseEM);
                }
                SessionManager.UnsetPurchaseOrderCookie();
                return RedirectToAction("PendingPurchaseEntries");
            }

            if (SessionManager.IsSetPurchaseOrderCookie())
            {
                BindPurchaseProductViewDdl();
                return View();
            }
            return RedirectToAction("AddPurchaseOrder");
        }

        [HttpPost]
        public ActionResult UpdatePurchaseProduct(PurchaseEntryModel purchaseEntryModel, string submit)
        {
            if (ModelState.IsValid)
            {
                if (purchaseEntryModel.AutoGenerateBarcode == true)
                {
                    purchaseEntryModel.ProductBarcodeId = BarcodeHelper.AutoGenerateBarcode(purchaseEntryModel.ProductId);
                }
                if (iPurchaseProduct.UpdatePurchaseOrderProduct(purchaseEntryModel, SessionManager.CurrentUserId) != 0)
                {
                    switch (submit)
                    {
                        case "Submit Order":
                            if (iPurchaseProduct.SubmitPurchaseOrder(purchaseEntryModel.PurchaseOrderId, SessionManager.CurrentUserId) != 0)
                            {
                                SessionManager.UnsetPurchaseOrderCookie();
                            }
                            return RedirectToAction("ViewPurchaseOrder", new { poId = purchaseEntryModel.PurchaseOrderId });
                        default:
                            SessionManager.UnsetPurchaseOrderCookie();
                            return RedirectToAction("ViewPendingPurchaseEntry", new { poId = purchaseEntryModel.PurchaseOrderId });
                    }
                }
                return RedirectToAction("PendingPurchaseEntries");
            }
            return View("UpdatePurchaseOrderProduct");
        }
        
        public ActionResult PendingPurchaseEntries()
        {
            SessionManager.UnsetPurchaseOrderCookie();
            List<PurchaseOrderModel> purchaseOrderList = iPurchaseProduct.GetPendingPurchaseOrders();
            return View(purchaseOrderList);
        }

        public ActionResult ViewPendingPurchaseEntry(ulong poId = 0)
        {
            if (poId != 0)
            {
                PurchaseOrderViewModel purchaseOrderViewModel = (PurchaseOrderViewModel)iPurchaseProduct.GetPendingPurchaseOrder(poId);
                if (purchaseOrderViewModel != null)
                    return View(purchaseOrderViewModel);
            }
            return RedirectToAction("PendingPurchaseEntries");
        }

        public ActionResult ViewPurchaseOrders()
        {
            List<PurchaseOrderModel> purchaseOrderList = iPurchaseProduct.GetCompletedPurchaseOrders();
            return View(purchaseOrderList);
        }

        public ActionResult ViewPurchaseOrder(ulong poId = 0)
        {
            if (poId != 0)
            {
                PurchaseOrderViewModel purchaseOrderViewModel = (PurchaseOrderViewModel)iPurchaseProduct.GetCompletedPurchaseOrder(poId);
                if (purchaseOrderViewModel != null)
                    return View(purchaseOrderViewModel);
            }
            return RedirectToAction("ViewPurchaseOrders");
        }

        public ActionResult UpdatePendingPurchaseOrder(ulong purchaseOrderId = 0)
        {
            if (purchaseOrderId != 0)
            {
                PurchaseOrderModel purchaseOrderModel = iPurchaseProduct.GetPendingPurchaseOrderToUpdate(purchaseOrderId);
                if (purchaseOrderModel != null)
                {
                    ViewBag.VendorDdlList = new VendorBLL().GetActiveVendorListDdl();
                    return View(purchaseOrderModel);
                }
            }
            return RedirectToAction("PendingPurchaseEntries");
        }

        [HttpPost]
        public ActionResult UpdatePendingPurchaseOrder(PurchaseOrderModel purchaseOrderModel)
        {
            if (ModelState.IsValid)
            {
                if (iPurchaseProduct.UpdatePendingPurchaseOrder(purchaseOrderModel, SessionManager.CurrentUserId) != 0)
                {
                    return RedirectToAction("PendingPurchaseEntries");
                }
                else
                {
                    TempData["Message"] = "Purchase Order is not updated successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            ViewBag.VendorDdlList = new VendorBLL().GetActiveVendorListDdl();
            return View(purchaseOrderModel);
        }
    }
}