﻿using System.Collections.Generic;

namespace DailyBasket.Warehouse.Areas.Purchase.Models.ViewModels
{
    public class PurchaseOrderViewModel
    {
        public ulong PurchaseOrderId { get; set; }
        public string VendorName { get; set; }
        public string BillNo { get; set; }
        public string BillDate { get; set; }
        public string FillingDate { get; set; }
        public float BillDiscountAmount { get; set; }
        public double TotalBillAmount { get; set; }
        public float TotalProductAmount { get; set; }
        public ulong TotalProductCount { get; set; }
        public List<PurchaseProductViewModel> purchaseProducts { get; set; }
    }
}