﻿using DailyBasket.Warehouse.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Areas.Purchase.Models.ViewModels
{
    public class PurchaseProductViewModel
    {
        public ulong Id { get; set; }
        public ulong PoId { get; set; }
        public ulong StockProductId { get; set; }
        public ulong ProductId { get; set; }
        public string ProductName { get; set; }
        public float Quantity { get; set; }
        public QuantityType QuantityType { get; set; }
        public float PurchaseRate { get; set; }
        public float MRP { get; set; }
        public float TotalAmount { get; set; }
        public string RackNo { get; set; }
    }
}