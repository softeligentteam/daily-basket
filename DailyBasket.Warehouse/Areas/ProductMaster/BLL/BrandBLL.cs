﻿using DailyBasket.Warehouse.Core.Interface.BLL.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.Product;
using System.Web.Mvc;
using DailyBasket.Warehouse.Repository.ProductMaster;
using System.Data;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Helper;

namespace DailyBasket.Warehouse.Areas.ProductMaster.BLL
{
    public class BrandBLL : IBrand
    {
        BrandRepository brandRepository = new BrandRepository();
        public int AddBrand(BrandModel brandModel, ulong userId)
        {
            return brandRepository.AddBrand(brandModel, userId);
        }

        public SelectList GetActiveBrandListDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(brandRepository.GetActiveBrandListDdl());
        }

        public string GetBrandImageToDelete(uint brandId)
        {
            DataTable dataTable = brandRepository.GetBrandImageToDelete(brandId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0]["brand_image"].ToString();
            }
            return null;
        }

        public List<BrandModel> GetBrandList()
        {
            DataTable dataTable = brandRepository.GetBrandList();

            if (dataTable != null)
            {
                List<BrandModel> brandList = new List<BrandModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    brandList.Add(new BrandModel
                    {
                        Id = Convert.ToUInt32(row["brand_id"].ToString()),
                        Name = row["brand"].ToString(),
                        Status = Convert.ToByte(row["status"].ToString())
                    });
                }
                return brandList;
            }
            return null;
        }

        public BrandModel GetBrandToUpdate(uint brandId)
        {
            DataTable dataTable = brandRepository.GetBrandToUpdate(brandId);
            if (dataTable != null || dataTable.Rows.Count > 0)
            {
                BrandModel brandModel = new BrandModel();
                brandModel.Id = Convert.ToUInt32(dataTable.Rows[0]["brand_id"].ToString());
                brandModel.Name = dataTable.Rows[0]["brand"].ToString();
                return brandModel;
            }
            return null;
        }

        public int UpdateBrand(BrandModel brandModel, ulong userId)
        {
            return brandRepository.UpdateBrand(brandModel, userId);
        }
    }
}