﻿using DailyBasket.Warehouse.Core.Interface.BLL.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.Users;
using System.Web.Mvc;
using DailyBasket.Warehouse.Repository.ProductMaster;
using System.Data;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Helper;

namespace DailyBasket.Warehouse.Areas.ProductMaster.BLL
{
    public class VendorBLL : IVendor
    {
        VendorRepository vendorRepository = new VendorRepository();
        public int AddVendor(VendorModel vendorModel, ulong userId)
        {
            return vendorRepository.AddVendor(vendorModel, userId);
        }

        public SelectList GetActiveVendorListDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(new VendorRepository().GetVendorListDdl());
        }

        public List<VendorModel> GetVendorList()
        {
            DataTable dataTable = vendorRepository.GetVendorList();

            List<VendorModel> vendorList = new List<VendorModel>();
            if (dataTable != null)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    vendorList.Add(new VendorModel
                    {
                        Id = Convert.ToUInt32(row["vendor_id"].ToString()),
                        FirmName = row["vendor_name"].ToString(),
                        Name = row["contact_person"].ToString(),
                        Mobile = Convert.ToUInt64(row["mobile"].ToString()),
                        Phone = row["phone"].ToString(),
                        Email = row["email"].ToString(),
                        Status = Convert.ToByte(row["status"].ToString())
                    });
                }
                return vendorList;
            }
            return null;
        }

        public VendorModel GetVendorToUpdate(uint vendorId)
        {
            DataTable dataTable = vendorRepository.GetVendorToUpdate(vendorId);
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                VendorModel vendorModel = new VendorModel();
                vendorModel.Id = Convert.ToUInt32(dataTable.Rows[0]["vendor_id"].ToString());
                vendorModel.FirmName = dataTable.Rows[0]["vendor_name"].ToString();
                vendorModel.Address = dataTable.Rows[0]["address"].ToString();
                vendorModel.Locality = dataTable.Rows[0]["locality"].ToString();
                vendorModel.City = dataTable.Rows[0]["city"].ToString();
                vendorModel.Name = dataTable.Rows[0]["contact_person"].ToString();
                vendorModel.Mobile = Convert.ToUInt64(dataTable.Rows[0]["mobile"].ToString());
                vendorModel.Phone = dataTable.Rows[0]["phone"].ToString();
                vendorModel.Email = dataTable.Rows[0]["email"].ToString();
                vendorModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
                vendorModel.Status = Convert.ToByte(dataTable.Rows[0]["status"].ToString());
                return vendorModel;
            }
            return null;
        }

        public int UpdateVendor(VendorModel vendorModel, ulong userId)
        {
            vendorModel.IsFirmNameChanged = !vendorRepository.IsVendorNameExists(vendorModel.FirmName); // If exists No change
            vendorModel.IsGSTINChanged = !vendorRepository.IsVendorGSTINExists(vendorModel.GSTIN); // If exists No change
            return vendorRepository.UpdateVendor(vendorModel, userId);
        }
    }
}