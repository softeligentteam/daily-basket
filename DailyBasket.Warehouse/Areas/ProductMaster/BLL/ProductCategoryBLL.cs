﻿using DailyBasket.Warehouse.Core.Interface.BLL.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.Product;
using DailyBasket.Warehouse.Repository.ProductMaster;
using System.Data;
using System.Web.Mvc;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Helper;

namespace DailyBasket.Warehouse.Areas.ProductMaster.BLL
{
    public class ProductCategoryBLL : IProductCategory
    {
        ProductCategoryRepository productCategoryRepository = new ProductCategoryRepository();
        // Add Product's Main Category
        public int AddMainCategory(MainCategoryModel mainCategoryModel, ulong userId)
        {
            return productCategoryRepository.AddMainCategory(mainCategoryModel, userId);
        }

        // Add Product's Sub Category
        public int AddSubCategory(SubCategoryModel subCategoryModel, ulong userId)
        {
            return productCategoryRepository.AddSubCategory(subCategoryModel, userId);
        }

        // Get Active Product's Main Category List To Bind Dropdown List
        public SelectList GetActiveMainCategoryListDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(productCategoryRepository.GetActiveMainCategoryListDdl());
        }
        
        public SelectList GetActiveSubCategoryListDdl(uint MainCategoryId)
        {
            return DropdownlistHelper.GetIntStringDropDownList(productCategoryRepository.GetActiveSubCategoryListDdl(MainCategoryId));
        }

        // All Subcategories To Generate CheckBoxes - WebApp Model
        public List<IntStringDdlModel> GetActiveSubCategoryModelList()
        {
            return DropdownlistHelper.GetIntStringModelDropDownList(productCategoryRepository.GetActiveSubCategoryListDdl());
        }

        public List<IntStringDdlModel> GetActiveSubCategoryModelList(uint MainCategoryId)
        {
            throw new NotImplementedException();
        }

        public string GetMainCategoryImageToDelete(uint mainCategoryId)
        {
            DataTable dataTable = productCategoryRepository.GetMainCategoryImageToDelete(mainCategoryId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0]["mc_image"].ToString();
            }
            return null;
        }

        public List<MainCategoryModel> GetMainCategoryList()
        {
            DataTable dataTable = productCategoryRepository.GetMainCategoryList();
            if (dataTable != null)
            {
                List<MainCategoryModel> mainCategoryList = new List<MainCategoryModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    mainCategoryList.Add(new MainCategoryModel
                    {
                        MainCategoryId = Convert.ToUInt32(row["mc_id"].ToString()),
                        MainCategoryName = row["main_category"].ToString(),
                        Status = Convert.ToByte(row["status"].ToString())
                    });
                }
                return mainCategoryList;
            }
            return null;
        }

        public MainCategoryModel GetMainCategoryToUpdate(uint mainCategoryId)
        {
            DataTable dataTable = productCategoryRepository.GetMainCategoryToUpdate(mainCategoryId);
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                MainCategoryModel mainCategoryModel = new MainCategoryModel();
                mainCategoryModel.MainCategoryId = Convert.ToUInt32(dataTable.Rows[0]["mc_id"].ToString());
                mainCategoryModel.MainCategoryName = dataTable.Rows[0]["main_category"].ToString();
                return mainCategoryModel;
            }
            return null;
        }

        public string GetSubCategoryImageToDelete(uint subCategoryId)
        {
            DataTable dataTable = productCategoryRepository.GetSubCategoryImageToDelete(subCategoryId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0]["sc_image"].ToString();
            }
            return null;
        }

        public List<SubCategoryModel> GetSubCategoryList()
        {
            DataTable dataTable = productCategoryRepository.GetSubCategoryList();
            if (dataTable != null)
            {
                List<SubCategoryModel> subCategoryList = new List<SubCategoryModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    subCategoryList.Add(new SubCategoryModel
                    {
                        SubCategoryId = Convert.ToUInt32(row["sc_id"].ToString()),
                        SubCategoryName = row["sub_category"].ToString(),
                        MainCategory = row["main_category"].ToString(),
                        Status = Convert.ToByte(row["status"].ToString())
                    });
                }
                return subCategoryList;
            }
            return null;
        }

        public SubCategoryModel GetSubCategoryToUpdate(uint SubCategoryId)
        {
            DataTable dataTable = productCategoryRepository.GetSubCategoryToUpdate(SubCategoryId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                SubCategoryModel subCategoryModel = new SubCategoryModel();
                subCategoryModel.SubCategoryId = Convert.ToUInt32(dataTable.Rows[0]["sc_id"].ToString());
                subCategoryModel.MainCategoryId = Convert.ToUInt32(dataTable.Rows[0]["mc_id"].ToString());
                subCategoryModel.MainCategoryName = dataTable.Rows[0]["main_category"].ToString();
                subCategoryModel.SubCategoryName = dataTable.Rows[0]["sub_category"].ToString();
                return subCategoryModel;
            }
            return null;
        }

        public int UpdateMainCategory(MainCategoryModel mainCategoryModel, ulong userId)
        {
            return productCategoryRepository.UpdateMainCategory(mainCategoryModel, userId);
        }

        public int UpdateMainCategoryStatus(uint mainCategoryId, uint status, ulong userId)
        {
            return productCategoryRepository.UpdateMainCategoryStatus(mainCategoryId, status, userId);
        }

        public int UpdateSubCategory(SubCategoryModel subCategoryModel, ulong userId)
        {
            return productCategoryRepository.UpdateSubCategory(subCategoryModel, userId);
        }

        public int UpdateSubcategoryStatus(uint subCategoryId, uint status, ulong userId)
        {
            return productCategoryRepository.UpdateSubcategoryStatus(subCategoryId, status, userId);
        }

        public SelectList GetAllSubCatogoryListDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(productCategoryRepository.GetAllSubCatogoryListDdl());
        }
    }
}