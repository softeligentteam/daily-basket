﻿using DailyBasket.Warehouse.Core.Interface.BLL.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Model.Product;
using System.Web.Mvc;
using DailyBasket.Warehouse.Repository.ProductMaster;
using System.Data;
using DailyBasket.Warehouse.Helper;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using System.Collections;
using DailyBasket.Warehouse.Core.Enum;
using System.Runtime.CompilerServices;

namespace DailyBasket.Warehouse.Areas.ProductMaster.BLL
{
    public class ProductBLL : IProduct
    {
        Core.Interface.DAL.ProductMaster.IProduct iproductRepository;
        public ProductBLL()
        {
            iproductRepository = new ProductRepository();
        }
        public int AddProduct(ProductModel productModel, ulong userId)
        {
            return iproductRepository.AddProduct(productModel, userId);
        }

        public int AddProductHsn(ProductHsnModel productHsnModel, ulong userId)
        {
            return iproductRepository.AddProductHsn(productHsnModel, userId);
        }

        public SelectList GetActiveProductListDdl()
        {
            return DropdownlistHelper.GetLongStringDropDownList(iproductRepository.GetActiveProductListDdl());
        }

        public SelectList GetActiveProductListDdl(uint mainCategoryId, uint subCategoryId)
        {
            return DropdownlistHelper.GetLongStringDropDownList(iproductRepository.GetActiveProductListDdl(mainCategoryId, subCategoryId));
        }

        public SelectList GetProductHsnListDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(iproductRepository.GetProductHsnListDdl());
        }

        public SelectList GetConvertableProductListDdl(uint mainCategoryId, uint subCategoryId, ulong productId)
        {
            return DropdownlistHelper.GetLongStringDropDownList(iproductRepository.GetConvertableProductListDdl(mainCategoryId, subCategoryId, productId));
        }

        public List<ProductModel> GetProductList()
        {
            DataTable dataTable = iproductRepository.GetProductList();
            if (dataTable != null)
            {
                List<ProductModel> productList = new List<ProductModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    ProductModel productModel = new ProductModel();
                    productModel.Id = Convert.ToUInt32(row["prod_id"].ToString());
                    productModel.Name = row["prod_name"].ToString();
                    productModel.SelectProductHSNNumberId = row["hsn_number"].ToString();
                    productModel.SelectMainCategoryId = row["main_category"].ToString();
                    productModel.SelectSubCategoryId = row["sub_category"].ToString();
                    productModel.Status = Convert.ToByte(row["status"].ToString());

                    productList.Add(productModel);
                }
                return productList;
            }
            return null;
        }

        public ProductModel GetProductToUpdate(ulong productId)
        {
            DataTable dataTable = iproductRepository.GetProductToUpdate(productId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                //prod_id, mc_id, , sc_id, , phsn_id, hsn_number, prod_name, quantity, quantity_type, brand_id, sgst, igst, cgst, is_convertable, description, display_index
                ProductModel productModel = new ProductModel();
                productModel.Id = Convert.ToUInt32(dataTable.Rows[0]["prod_id"].ToString());
                productModel.MainCategoryId = Convert.ToUInt32(dataTable.Rows[0]["mc_id"].ToString());
                productModel.SelectMainCategoryId = dataTable.Rows[0]["main_category"].ToString();
                productModel.SubCategoryId = Convert.ToUInt32(dataTable.Rows[0]["sc_id"].ToString());
                productModel.SelectSubCategoryId = dataTable.Rows[0]["sub_category"].ToString();
                productModel.BrandId = Convert.ToUInt32(dataTable.Rows[0]["brand_id"].ToString());
                productModel.SelectBrandId = dataTable.Rows[0]["brand"].ToString();
                productModel.ProductHSNNumberId = Convert.ToUInt32(dataTable.Rows[0]["phsn_id"].ToString());
                productModel.SelectProductHSNNumberId = dataTable.Rows[0]["hsn_number"].ToString();
                productModel.IsNameUpdated = false;
                productModel.Name = dataTable.Rows[0]["prod_name"].ToString();
                productModel.Quantity = Convert.ToSingle(dataTable.Rows[0]["quantity"].ToString());
                productModel.QuantityType = dataTable.Rows[0]["quantity_type"].ToString();
                productModel.SGST = Convert.ToSingle(dataTable.Rows[0]["sgst"].ToString());
                productModel.IGST = Convert.ToSingle(dataTable.Rows[0]["igst"].ToString());
                productModel.CGST = Convert.ToSingle(dataTable.Rows[0]["cgst"].ToString());
                productModel.DisplayIndex = Convert.ToUInt32(dataTable.Rows[0]["display_index"].ToString());
                productModel.IsConvertable = Convert.ToBoolean(dataTable.Rows[0]["is_convertable"].ToString());
                productModel.ProductDescription = dataTable.Rows[0]["description"].ToString();
                productModel.ProductDisclaimer = dataTable.Rows[0]["disclaimer"].ToString();
                productModel.Status = Convert.ToByte(dataTable.Rows[0]["status"].ToString());
                return productModel;
            }
            return null;
        }

        public int UpdateProduct(ProductModel productModel, ulong userId)
        {
            return iproductRepository.UpdateProduct(productModel, userId);
        }

        public ArrayList GetProductImagesToDelete(ulong productId)
        {
            DataTable dataTable = iproductRepository.GetProductImagesToDelete(productId);
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                return new ArrayList
                {
                    dataTable.Rows[0]["prod_image1"].ToString(),
                    dataTable.Rows[0]["prod_image2"].ToString(),
                    dataTable.Rows[0]["prod_image3"].ToString(),
                    dataTable.Rows[0]["prod_image4"].ToString()
                };
            }
            return null;
        }

        public int UpdateProductStatus(ulong productId, uint status, ulong userId)
        {
            throw new NotImplementedException();
        }
    }
}