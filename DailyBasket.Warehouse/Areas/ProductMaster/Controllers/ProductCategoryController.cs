﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.Product;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.ProductMaster.Controllers
{
    [Authorize]
    public class ProductCategoryController : Controller
    {
        ProductCategoryBLL productCategoryBll = new ProductCategoryBLL();
        // GET: ProductMaster/ProductCategory
        public ActionResult Index()
        {
            return RedirectToAction("MainCategoryList");
        }
        
        public ActionResult MainCategoryList()
        {
            return View(productCategoryBll.GetMainCategoryList());
        }
        
        public ActionResult SubCategoryList()
        {
            return View(productCategoryBll.GetSubCategoryList());
        }

        [HttpGet]
        public ActionResult AddMainCategory()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMainCategory(MainCategoryModel mainCategoryModel, HttpPostedFileBase MainCategoryImage)
        {
            if (ModelState.IsValid)
            {
                mainCategoryModel.MainCategoryImage = ImageUploadHelper.UploadMainCategoryImage(MainCategoryImage);
                if (productCategoryBll.AddMainCategory(mainCategoryModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Product Main Category is added successfully";
                    TempData["cssClass"] = "text-success";
                    return RedirectToAction("AddMainCategory");
                }
                else
                {
                    TempData["Message"] = "Product Main Category is not added. Try again.";
                    TempData["cssClass"] = "text-danger";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult AddSubCategory()
        {
            // Get Active Product's Main Category List To Bind Dropdown List
            ViewBag.MainCategoryDdlList = productCategoryBll.GetActiveMainCategoryListDdl();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSubCategory(SubCategoryModel subCategoryModel, HttpPostedFileBase SubCategoryImage)
        {
            if (ModelState.IsValid)
            {
                subCategoryModel.SubCategoryImage = ImageUploadHelper.UploadSubCategoryImage(SubCategoryImage);
                if (productCategoryBll.AddSubCategory(subCategoryModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Product SubCategory is added successfully";
                    TempData["cssClass"] = "text-success";
                    return RedirectToAction("AddSubCategory");
                }
                else
                {
                    TempData["Message"] = "Product SubCategory is not added. Try again.";
                    TempData["cssClass"] = "text-danger";
                }
            }
            ViewBag.MainCategoryDdlList = productCategoryBll.GetActiveMainCategoryListDdl();
            return View();
        }

        [HttpGet]
        public ActionResult UpdateMainCategory(uint mainCatId = 0)
        {
            if(mainCatId != 0)
            {
                MainCategoryModel mainCategoryModel = productCategoryBll.GetMainCategoryToUpdate(mainCatId);
                if (mainCategoryModel != null)
                    return View(mainCategoryModel);
            }
            return RedirectToAction("MainCategoryList");
        }

        [HttpPost]
        public ActionResult UpdateMainCategory(MainCategoryModel mainCategoryModel, string submit, HttpPostedFileBase MainCategoryImage)
        {
            if (ModelState.IsValid)
            {
                switch (submit)
                {
                    case "Update Category":
                        if (productCategoryBll.UpdateMainCategory(mainCategoryModel, SessionManager.CurrentUserId) != 0)
                        {
                            TempData["Message"] = "Main Category is updated successfully.";
                            TempData["CssClass"] = "text-success";
                            return View("UpdateMainCategory", mainCategoryModel);
                        }
                        else
                        {
                            TempData["Message"] = "Opp's occured error! Try again.";
                            TempData["CssClass"] = "text-success";
                            return View("UpdateMainCategory", mainCategoryModel);
                        }
                    case "Update Image":
                        mainCategoryModel.MainCategoryImage = ImageUploadHelper.UploadMainCategoryImage(MainCategoryImage);
                        if(mainCategoryModel.MainCategoryImage != null)
                        {
                            string oldMCImage = productCategoryBll.GetMainCategoryImageToDelete(mainCategoryModel.MainCategoryId);
                            if (productCategoryBll.UpdateMainCategory(mainCategoryModel, SessionManager.CurrentUserId) != 0)
                            {
                                ImageUploadHelper.DeleteImage(oldMCImage);
                                TempData["Message"] = "Main Category Image is uploaded successfully.";
                                TempData["CssClass"] = "text-success";
                                TempData["McImageView"] = "displayMCImageView";
                                return View("UpdateMainCategory", mainCategoryModel);
                            }
                        }
                        TempData["McImageView"] = "displayMCImageView";
                        TempData["Message"] = "Opp's occured error! Try again.";
                        TempData["CssClass"] = "text-success";
                        return View("UpdateMainCategory", mainCategoryModel);
                    default:
                        return RedirectToAction("MainCategoryList");
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult UpdateSubCategory(uint subCatId = 0)
        {
            if (subCatId != 0)
            {
                SubCategoryModel subCategoryModel = productCategoryBll.GetSubCategoryToUpdate(subCatId);
                if (subCategoryModel != null)
                    return View(subCategoryModel);
            }
            return RedirectToAction("SubCategoryList");
        }

        [HttpPost]
        public ActionResult UpdateSubCategory(SubCategoryModel subCategoryModel, string submit, HttpPostedFileBase SubCategoryImage)
        {
            if (ModelState.IsValid)
            {
                switch (submit)
                {
                    case "Update SubCategory":
                        if (productCategoryBll.UpdateSubCategory(subCategoryModel, SessionManager.CurrentUserId) != 0)
                        {
                            TempData["Message"] = "SubCategory is updated successfully.";
                            TempData["CssClass"] = "text-success";
                            return View("UpdateSubCategory", subCategoryModel);
                        }
                        else
                        {
                            TempData["Message"] = "Opp's occured error! Try again.";
                            TempData["CssClass"] = "text-danger";
                            return View("UpdateSubCategory", subCategoryModel);
                        }
                    case "Update SubCategory Image":
                        subCategoryModel.SubCategoryImage = ImageUploadHelper.UploadSubCategoryImage(SubCategoryImage);
                        if (subCategoryModel.SubCategoryImage != null)
                        {
                            string oldMCImage = productCategoryBll.GetSubCategoryImageToDelete(subCategoryModel.SubCategoryId);
                            if (productCategoryBll.UpdateSubCategory(subCategoryModel, SessionManager.CurrentUserId) != 0)
                            {
                                ImageUploadHelper.DeleteImage(oldMCImage);
                                TempData["Message"] = "SubCategory Image is uploaded successfully.";
                                TempData["CssClass"] = "text-success";
                                TempData["ScImageView"] = "displaySCImageView";
                                return View("UpdateSubCategory", subCategoryModel);
                            }
                        }
                        TempData["ScImageView"] = "displaySCImageView";
                        TempData["Message"] = "Opp's occured error! Try again.";
                        TempData["CssClass"] = "text-danger";
                        return View("UpdateSubCategory", subCategoryModel);
                    default:
                        return RedirectToAction("SubCategoryList");
                }
            }
            return View();
        }

        public ActionResult UpdateMainCategoryStatus(uint mainCatId = 0, uint status = 0)
        {
            if (mainCatId != 0)
            {
                if (status == (uint)Status.Active)
                    status = (uint)Status.Inactive;
                else
                    status = (uint)Status.Active;
                productCategoryBll.UpdateMainCategoryStatus(mainCatId, status, SessionManager.CurrentUserId);
            }
            return RedirectToAction("MainCategoryList");
        }

        public ActionResult UpdateSubCategoryStatus(uint subCatId = 0, uint status = 0)
        {
            if (subCatId != 0)
            {
                if (status == (uint)Status.Active)
                    status = (uint)Status.Inactive;
                else
                    status = (uint)Status.Active;
                productCategoryBll.UpdateSubcategoryStatus(subCatId, status, SessionManager.CurrentUserId);
            }
            return RedirectToAction("SubCategoryList");
        }
    }
}