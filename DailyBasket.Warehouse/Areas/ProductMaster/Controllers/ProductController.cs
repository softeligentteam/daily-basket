﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Core.Model.DropDownList;
using DailyBasket.Warehouse.Core.Model.Product;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.ProductMaster.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        ProductBLL productBLL = new ProductBLL();
        // GET: ProductMaster/Product
        public ActionResult Index()
        {
            return RedirectToAction("ProductList");
        }

        public ActionResult ProductList()
        {
            return View(productBLL.GetProductList());
        }

        [HttpGet]
        public ActionResult AddProduct()
        {
            BindProductDropdownList(0, "Add"); // 0 - Main Category Id
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProduct(ProductModel productModel, HttpPostedFileBase ProductImage1, HttpPostedFileBase ProductImage2, HttpPostedFileBase ProductImage3, HttpPostedFileBase ProductImage4)
        {
            if (ModelState.IsValid)
            {
                productModel.ProductImage1 = ImageUploadHelper.UploadProductImage(ProductImage1);
                productModel.ProductImage2 = ImageUploadHelper.UploadProductImage(ProductImage2);
                productModel.ProductImage3 = ImageUploadHelper.UploadProductImage(ProductImage3);
                productModel.ProductImage4 = ImageUploadHelper.UploadProductImage(ProductImage4);
                if (productBLL.AddProduct(productModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Product is added successfully.";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("AddProduct");
                }
                else
                {
                    TempData["Message"] = "Product is not added successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            BindProductDropdownList(productModel.MainCategoryId, "Add");
            return View();
        }

        // Bind Product Form DropdownList Data
        private void BindProductDropdownList(uint MainCategoryId, string operation)
        {
            ViewBag.BrandDdlList = new BrandBLL().GetActiveBrandListDdl();
            ViewBag.ProductHSNDdlList = productBLL.GetProductHsnListDdl();
            ViewBag.MainCategoryDdlList = new ProductCategoryBLL().GetActiveMainCategoryListDdl();
            //ViewBag.QuantityTypeDdlList = EnumDropdownlistHelper.GetEnumQuantityTypeDdlList();
            if (MainCategoryId != 0)
                ViewBag.SubCategoryDdlList = new ProductCategoryBLL().GetActiveSubCategoryListDdl(MainCategoryId);
            ViewBag.DisplayIndexDdlList = EnumDropdownlistHelper.GetEnumProductDisplayIndexIdNameDdlList();
        }

        [HttpGet]
        public ActionResult AddProductHsnNumber()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProductHsnNumber(ProductHsnModel productHsnModel)
        {
            if (ModelState.IsValid)
            {
                if (productBLL.AddProductHsn(productHsnModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Product Hsn Number is added successfully.";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("AddProductHsnNumber");
                }
                else
                {
                    TempData["Message"] = "Product Hsn Number is not added successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult UpdateProduct(ulong productId = 0)
        {
            if(productId != 0)
            {
                ProductModel productModel = productBLL.GetProductToUpdate(productId);
                if(productModel != null)
                {
                    BindProductDropdownList(productModel.MainCategoryId, "Update"); // 0 - Main Category Id
                    return View(productModel);
                }
            }
            return RedirectToAction("ProductList");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProduct(ProductModel productModel, string submit, HttpPostedFileBase ProductImage1, HttpPostedFileBase ProductImage2, HttpPostedFileBase ProductImage3, HttpPostedFileBase ProductImage4)
        {
            BindProductDropdownList(productModel.MainCategoryId, "Update");
            if (ModelState.IsValid)
            {
                switch (submit)
                {
                    case "Update Product":
                        if (productBLL.UpdateProduct(productModel, SessionManager.CurrentUserId) != 0)
                        {
                            /*TempData["Message"] = "Product is updated successfully."; // old code
                            TempData["CssClass"] = "text-success";
                            return View(productModel); */
                            return RedirectToAction("ProductList");
                        }
                        else
                        {
                            TempData["Message"] = "Opp's occured error! Try again.";
                            TempData["CssClass"] = "text-danger";
                        }
                        return View(productModel);
                    case "Update Product Images":
                        if (ProductImage1 != null)
                        {
                            productModel.ProductImage1 = ImageUploadHelper.UploadProductImage(ProductImage1);
                            productModel.ProductImage2 = ImageUploadHelper.UploadProductImage(ProductImage2);
                            productModel.ProductImage3 = ImageUploadHelper.UploadProductImage(ProductImage3);
                            productModel.ProductImage4 = ImageUploadHelper.UploadProductImage(ProductImage4);
                            ArrayList productlImages = productBLL.GetProductImagesToDelete(productModel.Id);
                            if (productBLL.UpdateProduct(productModel, SessionManager.CurrentUserId) != 0)
                            {
                                DeleteProductImages(productlImages);
                                /*TempData["Message"] = "Product Image(s) are uploaded successfully."; // old code
                                TempData["CssClass"] = "text-success";
                                TempData["PImageView"] = "displayProductImageView"; 
                                return View(productModel);*/
                                return RedirectToAction("ProductList");
                            }
                        }
                        TempData["PImageView"] = "displayProductImageView";
                        TempData["Message"] = "Opp's occured error! Try again.";
                        TempData["CssClass"] = "text-danger";
                        return View(productModel);
                    default:
                        return RedirectToAction("ProductList");
                }
            }
            if(submit.Equals("Update Product Images"))
                TempData["PImageView"] = "displayProductImageView";
            return View(productModel);
        }

        private void DeleteProductImages(ArrayList productImages)
        {
            if (productImages != null)
            {
                for (int index = 0; index < productImages.Count; index++)
                {
                    if(productImages[index] != null)
                        ImageUploadHelper.DeleteImage(productImages[index].ToString());
                }
            }
        }
    }
}