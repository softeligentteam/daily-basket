﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.BLL;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Model.Users;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.ProductMaster.Controllers
{
    [Authorize]
    public class VendorController : Controller
    {
        VendorBLL vendorBLL = new VendorBLL();
        // GET: ProductMaster/Vendor
        public ActionResult Index()
        {
            return RedirectToAction("VendorList");
        }

        public ActionResult VendorList()
        {
            return View("VendorList", vendorBLL.GetVendorList());
        }

        [HttpGet]
        public ActionResult AddVendor()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddVendor(VendorModel vendorModel)
        {
            if (ModelState.IsValid)
            {
                if (vendorBLL.AddVendor(vendorModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Vendor is Added Successfully...";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("AddVendor");
                }
                else
                {
                    TempData["Message"] = "Vendor is not added successfully, Please try again!";
                    TempData["CssClass"] = "text-success";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult UpdateVendor(uint vendorId = 0)
        {
            if(vendorId != 0)
            {
                VendorModel vendorModel = vendorBLL.GetVendorToUpdate(vendorId);
                if (vendorModel != null)
                    return View(vendorModel);
            }
            return RedirectToAction("VendorList");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateVendor(VendorModel vendorModel)
        {
            if (ModelState.IsValid)
            {
                if (vendorBLL.UpdateVendor(vendorModel, SessionManager.CurrentUserId) != 0)
                {
                    return RedirectToAction("VendorList");
                }
                else
                {
                    TempData["Message"] = "Vendor is not added successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            return View();
        }

    }
}