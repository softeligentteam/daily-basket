﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Core.Model.Product;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.ProductMaster.Controllers
{
    [Authorize]
    public class BrandController : Controller
    {
        BrandBLL brandBLL = new BrandBLL();
        // GET: ProductMaster/Brand
        public ActionResult Index()
        {
            return RedirectToAction("BrandList");
        }

        public ActionResult BrandList()
        {
            return View(brandBLL.GetBrandList());
        }

        [HttpGet]
        public ActionResult AddBrand()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBrand(BrandModel brandModel, HttpPostedFileBase BrandImage)
        {
            if (ModelState.IsValid)
            {
                brandModel.BrandImage = ImageUploadHelper.UploadBrandImage(BrandImage);
                if (brandBLL.AddBrand(brandModel, SessionManager.CurrentUserId) != 0)
                {
                    TempData["Message"] = "Brand is added successfully...";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction("AddBrand");
                }
                else
                {
                    TempData["Message"] = "Brand is not added successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult UpdateBrand(uint brandId = 0)
        {
            if (brandId != 0)
            {
                BrandModel brandModel = brandBLL.GetBrandToUpdate(brandId);
                if (brandModel != null)
                {
                    return View(brandModel);
                }
            }
            return RedirectToAction("BrandList");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateBrand(BrandModel brandModel, string submit, HttpPostedFileBase BrandImage)
        {
            if (ModelState.IsValid)
            {
                switch (submit)
                {
                    case "Update Brand":
                        if (brandBLL.UpdateBrand(brandModel, SessionManager.CurrentUserId) != 0)
                        {
                            return RedirectToAction("BrandList");
                        }
                        else
                        {
                            TempData["Message"] = "Brand is not added successfully, Please try again!";
                            TempData["CssClass"] = "text-danger";
                            return View(brandModel);
                        }
                    case "Update Image":
                        brandModel.BrandImage = ImageUploadHelper.UploadBrandImage(BrandImage);
                        if (brandModel.BrandImage != null)
                        {
                            string oldBrandImage = brandBLL.GetBrandImageToDelete(brandModel.Id);
                            if (brandBLL.UpdateBrand(brandModel, SessionManager.CurrentUserId) != 0)
                            {
                                ImageUploadHelper.DeleteImage(oldBrandImage);
                                TempData["Message"] = "Brand Image is uploaded successfully.";
                                TempData["CssClass"] = "text-success";
                                TempData["BImageView"] = "displayBImageView";
                                return View(brandModel);
                            }
                        }
                        TempData["BImageView"] = "displayBImageView";
                        TempData["Message"] = "Opp's occured error! Try again.";
                        TempData["CssClass"] = "text-danger";
                        return View(brandModel);
                    default:
                        return RedirectToAction("BrandList");
                }
            }
            return View();
        }
    }
}