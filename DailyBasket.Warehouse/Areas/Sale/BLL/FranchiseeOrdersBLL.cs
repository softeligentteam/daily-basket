﻿using DailyBasket.Warehouse.Core.Interface.BLL.Sale;
using DailyBasket.Warehouse.Repository.Sale;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Warehouse.Core.Enum;
using System.Data;
using System.Runtime.CompilerServices;
using DailyBasket.Warehouse.Areas.Sale.Models.ViewModels;
using DailyBasket.Warehouse.Core.Model.Purchase;
using DailyBasket.Warehouse.Core.Model.GST;
using DailyBasket.Warehouse.Helper;
using System.Collections;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Sale.BLL
{
    public class FranchiseeOrdersBLL : IFranchiseeOrders
    {
        private Core.Interface.DAL.Sale.IFranchiseeOrders iFranchiseeOrdersRepository;
        public FranchiseeOrdersBLL()
        {
            iFranchiseeOrdersRepository = new FranchiseeOrdersRepository();
        }



        public dynamic GetFranchiseeOrders(OrderStatus orderStatus)
        {
            DataTable dataTable = iFranchiseeOrdersRepository.GetFranchiseeOrders(orderStatus);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindFranchiseeOrders(dataTable);
            }
            return null;
        }

        private List<FranchiseeOrderViewModels> BindFranchiseeOrders(DataTable dataTable)
        {
            List<FranchiseeOrderViewModels> franchiseeOrdersVMList = new List<FranchiseeOrderViewModels>();
            foreach (DataRow Row in dataTable.Rows)
            {
                FranchiseeOrderViewModels franchiseeOrdersVM = new FranchiseeOrderViewModels();
                franchiseeOrdersVM.Id = Convert.ToUInt64(Row["fc_po_id"].ToString());
                franchiseeOrdersVM.VendorName = Row["vendor_name"].ToString();
                franchiseeOrdersVM.OrderDate = Row["order_date"].ToString();
                franchiseeOrdersVMList.Add(franchiseeOrdersVM);
            }
            return franchiseeOrdersVMList;
        }
        public dynamic GetFranchiseeOrder(ulong foid, OrderStatus orderStatus)
        {

            DataSet dataSet = iFranchiseeOrdersRepository.GetFranchiseeOrder(foid, orderStatus);

            if (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0] != null)
            {
                return BindPurchaseOrderWithProducts(dataSet);

            }
            return null;
        }
        private dynamic BindPurchaseOrderWithProducts(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            FCPurchaseOrderModel poProductModel = new FCPurchaseOrderModel();
            poProductModel.Id = Convert.ToUInt64(dataTable.Rows[0]["fc_po_id"]);
            poProductModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
            poProductModel.OrderDate = dataTable.Rows[0]["order_date"].ToString();
            poProductModel.FCUserId = Convert.ToUInt64(dataTable.Rows[0]["fcuser_id"].ToString());

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                var map = new Dictionary<string, ulong>();
                List<FranchiseePOProductModel> pOrderProductList = new List<FranchiseePOProductModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                {
                    if (map.ContainsValue(Convert.ToUInt64(row["prod_id"])) != true)
                    {
                        FranchiseePOProductModel poProductLM = new FranchiseePOProductModel();
                        poProductLM.ProdId = Convert.ToUInt64(row["prod_id"].ToString());
                        poProductLM.ProdName = row["prod_name"].ToString();
                        poProductLM.FranchiseeQuantity = Convert.ToSingle(row["quantity"]);
                        //poProductLM.WarehouseQuantity = Convert.ToSingle(row["available_stock"]);
                        poProductLM.MRP = Convert.ToSingle(row["mrp"]);
                        poProductLM.OnlineSaleRateA = Convert.ToSingle(row["online_sale_rate"]);
                        //poProductLM.SGST = Convert.ToSingle(row["sgst"]);
                        //poProductLM.CGST = Convert.ToSingle(row["cgst"]);
                        //poProductLM.Total = Convert.ToSingle(row["total"]);
                        poProductLM.APS = Convert.ToSingle(row["available_stock"]);
                        pOrderProductList.Add(poProductLM);
                    }
                    if (Convert.ToInt64(row["available_stock"]) == 0)
                    {
                        map["prodId"] = Convert.ToUInt64(row["prod_id"]);
                    }
                }
                poProductModel.FranchisePOPModelList = pOrderProductList;
            }
            return poProductModel;
        }

        public int DeletePurchaseProduct(ulong poId, ulong prodId)
        {
            if (poId != 0 && prodId != 0)
                return iFranchiseeOrdersRepository.DeletePurchaseProduct(poId, prodId);
            return 0;
        }

        //public int UpdatePurchaseOrder(ulong poId, string[] prodId, string[] warehouseQuantity)
        //{
        //    foreach (string prodid in prodId)
        //    {
        //        foreach (string wqty in warehouseQuantity)
        //        {
        //            DataTable dataTable = iFranchiseeOrdersRepository.GetPurchaseOrderProductDetails(Convert.ToUInt64(prodid), Convert.ToSingle(wqty));

        //        }
        //    }

        //    return iFranchiseeOrdersRepository.UpdatePurchaseOrder(poId, prodId, warehouseQuantity);
        //}

        //public ulong AddPurchaseOrder(ulong FCUserId, string OrderDate, ulong Id, OrderStatus osId, SubmitType submitType, ulong cUserId)
        //{
        //    return iFranchiseeOrdersRepository.AddPurchaseOrder(FCUserId, OrderDate, Id, osId, submitType, cUserId);
        //}


        public dynamic GetPurchaseOrderProductDetails(string[] prodId, string[] warehouseQuantity, ulong fcPoId, ulong FCUserId, string OrderDate, OrderStatus inProcess, SubmitType submitType)
        {

            return iFranchiseeOrdersRepository.AddPurchaseOrderProduct(prodId, warehouseQuantity, SessionManager.CurrentUserId, fcPoId, FCUserId, OrderDate, inProcess, submitType);
        }

        public dynamic GetFCOrderHistory(OrderStatus orderStatus)
       {

            DataTable dataTable = iFranchiseeOrdersRepository.GetFCOrderHistory(orderStatus);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindFranchiseeOrderHistory(dataTable);
            }
            return null;
        }

        private List<FCPurchaseOrderModel> BindFranchiseeOrderHistory(DataTable dataTable)
        {
            List<FCPurchaseOrderModel> FCPOModel = new List<FCPurchaseOrderModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                FCPurchaseOrderModel franchiseeOrdersVM = new FCPurchaseOrderModel();
                franchiseeOrdersVM.Id = Convert.ToUInt64(Row["fc_po_id"].ToString());    //reference franchisees order id
                franchiseeOrdersVM.PoId = Convert.ToUInt64(Row["po_id"].ToString());    //order id
                franchiseeOrdersVM.VendorName = Row["vendor_name"].ToString();
                franchiseeOrdersVM.OrderDate = Row["order_date"].ToString();
                FCPOModel.Add(franchiseeOrdersVM);
            }
            return FCPOModel;
        }

        public dynamic GetFCPurchaseProducts(ulong poId)
        {
            DataSet dataSet = iFranchiseeOrdersRepository.GetFCPurchaseProducts(poId);

            if (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0] != null)
            {
                return BindPOrderWithProducts(dataSet);

            }
            return null;
        }

        private dynamic BindPOrderWithProducts(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            FCPurchaseOrderModel fcPOProductModel = new FCPurchaseOrderModel();
            fcPOProductModel.Id = Convert.ToUInt64(dataTable.Rows[0]["fc_po_id"]);
            fcPOProductModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
            fcPOProductModel.OrderDate = dataTable.Rows[0]["order_date"].ToString();

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<FranchiseePOProductModel> franchiseePOPModel = new List<FranchiseePOProductModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                {
                    FranchiseePOProductModel franchiseePOProductModel = new FranchiseePOProductModel();
                    franchiseePOProductModel.ProdName = row["prodname"].ToString();
                    franchiseePOProductModel.FranchiseeQuantity = Convert.ToSingle(row["quantity"]);
                    franchiseePOPModel.Add(franchiseePOProductModel);
                }
                fcPOProductModel.FranchisePOPModelList = franchiseePOPModel;
            }
            return fcPOProductModel;
        }

       
        public dynamic GetFranchiseeOrdersList(OrderStatus orderStatus)
        {
            DataTable dataTable = iFranchiseeOrdersRepository.GetFranchiseeOrdersList(orderStatus);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindFranchiseeOrdersList(dataTable);
            }
            return null;
        }

        private List<FCPurchaseOrderModel> BindFranchiseeOrdersList(DataTable dataTable)
        {
            List<FCPurchaseOrderModel> franchiseeOrdersMList = new List<FCPurchaseOrderModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                FCPurchaseOrderModel franchiseeOrdersM = new FCPurchaseOrderModel();
                franchiseeOrdersM.Id = Convert.ToUInt64(Row["po_id"].ToString());
                franchiseeOrdersM.VendorName = Row["vendor_name"].ToString();
                franchiseeOrdersM.OrderDate = Row["order_date"].ToString();
                franchiseeOrdersMList.Add(franchiseeOrdersM);
            }
            return franchiseeOrdersMList;
        }

        public dynamic GetFranchiseeOrderDetails(ulong foid, OrderStatus orderStatus)
        {

            DataSet dataSet = iFranchiseeOrdersRepository.GetFranchiseeOrderDetails(foid, orderStatus);

            if (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0] != null)
            {
                return BindPOWithProducts(dataSet);

            }
            return null;
        }
        private dynamic BindPOWithProducts(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            FCPurchaseOrderModel poProductModel = new FCPurchaseOrderModel();
            poProductModel.Id = Convert.ToUInt64(dataTable.Rows[0]["ref_order_id"]);
            poProductModel.PoId = Convert.ToUInt64(dataTable.Rows[0]["order_id"]);
            poProductModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            poProductModel.Address = dataTable.Rows[0]["address"].ToString();
            poProductModel.City = dataTable.Rows[0]["city"].ToString();
            poProductModel.State = dataTable.Rows[0]["state"].ToString();
            poProductModel.PinCode = Convert.ToInt32(dataTable.Rows[0]["pincode"].ToString());
            poProductModel.Mobile = dataTable.Rows[0]["mobile"].ToString();
            poProductModel.Email= dataTable.Rows[0]["email"].ToString();
            poProductModel.OrderDate = dataTable.Rows[0]["order_date"].ToString();
            poProductModel.TotalBillAmount = Convert.ToUInt64(dataTable.Rows[0]["total_bill_amount"].ToString());
            poProductModel.FUserId = Convert.ToUInt64(dataTable.Rows[0]["fuser_id"].ToString());


            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<FranchiseePOProductModel> pOrderProductList = new List<FranchiseePOProductModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                {
                        FranchiseePOProductModel poProductLM = new FranchiseePOProductModel();
                        poProductLM.ProdId = Convert.ToUInt64(row["prod_id"].ToString());
                        poProductLM.SpId = Convert.ToUInt64(row["sp_id"].ToString());
                        poProductLM.ProdName = row["prod_name"].ToString();
                        poProductLM.FranchiseeQuantity = Convert.ToSingle(row["quantity"]);
                        poProductLM.OnlineSaleRateA = Convert.ToSingle(row["price"]);
                        poProductLM.MRP = Convert.ToSingle(row["mrp"]);
                        poProductLM.Total = Convert.ToSingle(row["total_amount"]);
                        pOrderProductList.Add(poProductLM);
                                        
                }
                poProductModel.FranchisePOPModelList = pOrderProductList;
            }
            return poProductModel;
        }

        public int ChangeOrderStatus(ulong FranchiseeOrderId, ulong WarehouseOrderId, uint orderStatusId, ulong currentUserId)
        {
            return iFranchiseeOrdersRepository.ChangeOrderStatus(FranchiseeOrderId, WarehouseOrderId, orderStatusId, currentUserId);
        }

        public int RevertProductsStock(ulong WarehouseOrderId, uint orderStatusId, ulong userId)
        {
            return iFranchiseeOrdersRepository.RevertProductsStock(WarehouseOrderId, orderStatusId, userId);
        }

        public int AddStockProducts(ulong orderId, ulong cuserId)
        {
            return iFranchiseeOrdersRepository.AddStockProducts(orderId, cuserId);
        }

        public ulong GenerateFranchiseeOrder(ulong cUserId, OrderStatus osId, ulong fcUserId)
        {
            return iFranchiseeOrdersRepository.GenerateFranchiseeOrder(cUserId, osId, fcUserId);
        }
        public dynamic GetPurchaseOrderProductToAddList(ulong poId)
        {
            DataSet dataSet = iFranchiseeOrdersRepository.GetPurchaseOrderProductToAddList(poId);

            if (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0] != null)
            {
                return BindPurchaseOrderProduct(dataSet);
            }
            return null;
        }

        private FCPurchaseOrderModel BindPurchaseOrderProduct(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                FCPurchaseOrderModel poProductModel = new FCPurchaseOrderModel();
                poProductModel.Id = Convert.ToUInt64(dataTable.Rows[0]["fc_po_id"]);
                poProductModel.OrderDate = dataTable.Rows[0]["order_date"].ToString();
                poProductModel.FCUserId = Convert.ToUInt64(dataTable.Rows[0]["fcuser_id"].ToString());
                poProductModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
                List <FranchiseePOProductModel> poProductList = new List<FranchiseePOProductModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                    {
                        FranchiseePOProductModel poProductLM = new FranchiseePOProductModel();
                        poProductLM.ProdId = Convert.ToUInt64(row["prod_id"]);
                        poProductLM.ProdName = row["prodname"].ToString();
                        poProductLM.FranchiseeQuantity = Convert.ToSingle(row["quantity"]);
                        poProductLM.APS= Convert.ToSingle(row["available_stock"]);
                        poProductList.Add(poProductLM);
                    }
                    poProductModel.FranchisePOPModelList = poProductList;
                    return poProductModel;
            }
            return null;
        }

        public SelectList GetFranchiseeList()
        {
            return DropdownlistHelper.GetLongStringDropDownList(iFranchiseeOrdersRepository.GetFranchiseeList());    
        }

        public int AddPurchaseOrderProduct(FCPurchaseOrderModel fcPurchaseOrderModel)
        {
            return iFranchiseeOrdersRepository.AddPurchaseOrderProduct(fcPurchaseOrderModel);
        }

        public int DeleteFranchiseePurchaseProduct(ulong poId, ulong prodId)
        {
            if (poId != 0 && prodId != 0)
                return iFranchiseeOrdersRepository.DeleteFranchiseePurchaseProduct(poId, prodId);
            return 0;
        }

        public int UpdateFranchiseePOSubmitType(SubmitType submitType, ulong fcPoId)
        {
            return iFranchiseeOrdersRepository.UpdateFranchiseePOSubmitType(submitType, fcPoId);
        }

        public dynamic GetFranchiseeOrderToPrint(ulong foId, ulong orderStatus)
        {
            DataSet dataSet = iFranchiseeOrdersRepository.GetFranchiseeOrderToPrint(foId, orderStatus);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                return BindInvoiceFranchiseeOrderProducts(dataSet);
            }
            return null;
        }

        private FranchiseeOrderViewModels BindInvoiceFranchiseeOrderProducts(DataSet dataSet)
        {
            DataTable coDataTable = dataSet.Tables[0];
            FranchiseeOrderViewModels franchiseeOrdersVM = new FranchiseeOrderViewModels();
            //Customer Details
            franchiseeOrdersVM.FranchiseeName = coDataTable.Rows[0]["franchisee_name"].ToString();
            franchiseeOrdersVM.FAddress = coDataTable.Rows[0]["address"].ToString();
            franchiseeOrdersVM.FCity = coDataTable.Rows[0]["city"].ToString();
            franchiseeOrdersVM.FState = coDataTable.Rows[0]["state"].ToString();
            franchiseeOrdersVM.FMobile = coDataTable.Rows[0]["mobile"].ToString();
            franchiseeOrdersVM.Gstin = coDataTable.Rows[0]["gstin"].ToString();
            franchiseeOrdersVM.Email = coDataTable.Rows[0]["official_email_id"].ToString();
            franchiseeOrdersVM.FState = coDataTable.Rows[0]["state"].ToString();
            franchiseeOrdersVM.OrderDate = coDataTable.Rows[0]["order_date"].ToString();
            franchiseeOrdersVM.Id = Convert.ToUInt64(coDataTable.Rows[0]["order_id"].ToString());
            franchiseeOrdersVM.Total_Bill_Amount = Convert.ToSingle(coDataTable.Rows[0]["total_bill_amount"].ToString());
            franchiseeOrdersVM.GstAmount = Convert.ToSingle(coDataTable.Rows[0]["igst_amount"].ToString()) + Convert.ToSingle(coDataTable.Rows[0]["cgst_amount"].ToString()) + Convert.ToSingle(coDataTable.Rows[0]["sgst_amount"].ToString());
            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<FranchiseeOrderProductViewModel> franchiseeOrderVMList = new List<FranchiseeOrderProductViewModel>();
                foreach (DataRow Row in dataSet.Tables[1].Rows)
                {
                    FranchiseeOrderProductViewModel franchiseeOrderProductVM = new FranchiseeOrderProductViewModel();
                    franchiseeOrderProductVM.ProductName = Row["prod_name"].ToString();
                    franchiseeOrderProductVM.Quantity = Convert.ToSingle(Row["quantity"]);
                    franchiseeOrderProductVM.TotalAmount = Convert.ToDouble(Row["total_amount"]);
                    franchiseeOrderProductVM.Sale_Rate = Convert.ToSingle(Row["price"].ToString());
                    franchiseeOrderProductVM.Mrp = Convert.ToSingle(Row["mrp"].ToString());
                   

                    franchiseeOrderVMList.Add(franchiseeOrderProductVM);

                    franchiseeOrdersVM.QuantityCount = franchiseeOrdersVM.QuantityCount + (uint)franchiseeOrderProductVM.Quantity;
                    franchiseeOrdersVM.ProductTotalAmount = franchiseeOrdersVM.ProductTotalAmount + franchiseeOrderProductVM.TotalAmount;
                    franchiseeOrdersVM.SavingAmount = franchiseeOrdersVM.SavingAmount + CalculateSavingAmount(franchiseeOrderProductVM.Mrp, franchiseeOrderProductVM.Sale_Rate, franchiseeOrderProductVM.Quantity);
                }
               
                franchiseeOrdersVM.FranchiseeOrders = franchiseeOrderVMList;
            }
            else
            {
                return franchiseeOrdersVM = null;
            }


            return franchiseeOrdersVM;
        }

        private float CalculateSavingAmount(float mrp, float saleRate, float quantity)
        {
            float difference = mrp - saleRate;
            if (difference > 0)
            {
                return (difference * quantity);
            }
            return 0;
        }


    }
}