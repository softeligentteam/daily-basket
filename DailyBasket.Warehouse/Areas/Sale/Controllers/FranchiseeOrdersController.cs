﻿using DailyBasket.Warehouse.Areas.ProductMaster.BLL;
using DailyBasket.Warehouse.Areas.Sale.BLL;
using DailyBasket.Warehouse.Areas.Sale.Models.ViewModels;
using DailyBasket.Warehouse.Core.Enum;
using DailyBasket.Warehouse.Core.Interface.BLL.Sale;
using DailyBasket.Warehouse.Core.Model.Purchase;
using DailyBasket.Warehouse.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.Sale.Controllers
{
    public class FranchiseeOrdersController : Controller
    {
        // GET: Sale/FranchiseeOrders
        private IFranchiseeOrders iFranchiseeOrdersBLL = new FranchiseeOrdersBLL();
        private ICustomerOrders iCustomerOrdersBLL = new CustomerOrdersBLL();

        public ActionResult FranchiseePurchaseOrders()
        {
            return View("FranchiseePendingOrders", GetFranchiseeOrders(OrderStatus.InProcess));
        }

        private List<FranchiseeOrderViewModels> GetFranchiseeOrders(OrderStatus orderStatus)
        {
            return iFranchiseeOrdersBLL.GetFranchiseeOrders(orderStatus);
        }

        // Get Purchase Order History of all Franchisee
        [HttpGet]
        public ActionResult FranchiseeOrdersHistory()
        {
            return View("FranchiseePurchaseOrdersHistory", iFranchiseeOrdersBLL.GetFCOrderHistory(OrderStatus.InProcess));
        }

        [HttpGet]
        public ActionResult FranchiseeOrderHistory(ulong refFCPoId = 0)
        {
            if(refFCPoId != 0)
            {
                  FCPurchaseOrderModel fcPurchaseOrderModel = iFranchiseeOrdersBLL.GetFCPurchaseProducts(refFCPoId);
                  return View(fcPurchaseOrderModel);
            }
            return RedirectToAction("FranchiseeOrdersHistory");
        }

        private FCPurchaseOrderModel GetFranchiseeOrder(OrderStatus orderStatus, ulong foId)
        {
            return iFranchiseeOrdersBLL.GetFranchiseeOrder(foId, orderStatus);
        }

        [HttpGet]
        public ActionResult DeletePurchaseProduct(ulong poId = 0, ulong prodId = 0)
        {
            if (poId != 0 && prodId != 0)
            {
                if (iFranchiseeOrdersBLL.DeletePurchaseProduct(poId, prodId) != 0)
                {
                    TempData["Message"] = "Product deleted successfully.";
                    TempData["CssClass"] = "text-success";
                }
                else
                {
                    TempData["Message"] = "product not deleted successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
           // ViewBag.MethodName = "InProcessOrder";
            //ViewBag.PageTitle = "In Process Franchise Order";
            //TempData["poId"] = poId;
            return RedirectToAction("AddPurchaseOrder", new { foId = poId });
        }

        [HttpGet]
        public ActionResult AddPurchaseOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Franchisee Order";
            FCPurchaseOrderModel franchiseeOrderModel = GetFranchiseeOrder(OrderStatus.InProcess, foId);
            if (franchiseeOrderModel == null)
                return RedirectToAction("FranchiseePurchaseOrders");
            franchiseeOrderModel.Status = OrderStatus.InProcess;
            return View("AddPurchaseOrder", franchiseeOrderModel);
        }

        [HttpPost]
        public ActionResult AddPurchaseOrder(string[] ProductId, string[] Quantity, ulong Id, ulong FCUserId, string OrderDate)
        {
            if (ProductId != null && Quantity != null)
            {
                // Id = Ref Order Id;
                //FCUserId = Franchisee Created user id;
                   if (iFranchiseeOrdersBLL.GetPurchaseOrderProductDetails(ProductId, Quantity, Id, FCUserId, OrderDate, OrderStatus.InProcess, SubmitType.Temperary) != 0)
                    {

                        return RedirectToAction("FranchiseePurchaseOrders");
                    }
                    else
                    {
                        return RedirectToAction("AddPurchaseOrder", new { foId = Id});
                    }

            }
            return null;
        }


        // START FRANCHISEES ORDERS FLOW
        public ActionResult InProcessOrders()
        {
            ViewBag.MethodName = "InProcessOrder";
            ViewBag.PageTitle = "In Process Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrdersList(OrderStatus.InProcess));
        }

        public ActionResult ReadytoDispatchOrders()
        {
            ViewBag.MethodName = "ReadytoDispatchOrder";
            ViewBag.PageTitle = "Ready to Dispatch Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrdersList(OrderStatus.ReadyToDispatch));
        }
        public ActionResult DispatchedOrders()
        {
            ViewBag.MethodName = "DispatchedOrder";
            ViewBag.PageTitle = "Dispatch Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrdersList(OrderStatus.Dispatched));
        }

        public ActionResult DeliveredOrders()
        {
            ViewBag.MethodName = "DeliveredOrder";
            ViewBag.PageTitle = "Delivered Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrdersList(OrderStatus.Delivered));
        }

        public ActionResult CancelledOrders()
        {
            ViewBag.MethodName = "CancelledOrder";
            ViewBag.PageTitle = "Cancelled Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrdersList(OrderStatus.Cancelled));
        }
        
        private List<FCPurchaseOrderModel> GetFranchiseeOrdersList(OrderStatus orderStatus)
        {
            return iFranchiseeOrdersBLL.GetFranchiseeOrdersList(orderStatus);
        }

        [HttpGet]
        public ActionResult InProcessOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "In Process Franchisee Order";
            
            FCPurchaseOrderModel fcPOrderModel = GetFranchiseeOrderDetails(OrderStatus.InProcess, foId);
            if (fcPOrderModel == null)
                return RedirectToAction("InProcessOrders");
            fcPOrderModel.Status = OrderStatus.InProcess;
            BindOrderStatusDdlList(OrderStatus.InProcess);
            return View("FranchiseeOrder", fcPOrderModel);
        }
        public ActionResult ReadytoDispatchOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Ready to Dispatch Customer Order";
           
            FCPurchaseOrderModel fcPOrderModel = GetFranchiseeOrderDetails(OrderStatus.ReadyToDispatch, foId);
            if (fcPOrderModel == null)
                return RedirectToAction("ReadytoDispatchOrders");
            fcPOrderModel.Status = OrderStatus.ReadyToDispatch;
            BindOrderStatusDdlList(OrderStatus.ReadyToDispatch);
            return View("FranchiseeOrder", fcPOrderModel);
        }

        public ActionResult DispatchedOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Dispatch Customer Order";
            FCPurchaseOrderModel fcPOrderModel = GetFranchiseeOrderDetails(OrderStatus.Dispatched, foId);
            if (fcPOrderModel == null)
                return RedirectToAction("DispatchedOrders");
            fcPOrderModel.Status = OrderStatus.Dispatched;
            BindOrderStatusDdlList(OrderStatus.Dispatched);
            return View("FranchiseeOrder", fcPOrderModel);
        }

        public ActionResult DeliveredOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Delivered Customer Order";
            FCPurchaseOrderModel fcPOrderModel = GetFranchiseeOrderDetails(OrderStatus.Delivered, foId);
            if (fcPOrderModel == null)
                return RedirectToAction("DeliveredOrders");
            fcPOrderModel.Status = OrderStatus.Delivered;
            BindOrderStatusDdlList(OrderStatus.Delivered);
            return View("FranchiseeOrder", fcPOrderModel);
        }

        public ActionResult CancelledOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Cancelled Customer Order";
           
            FCPurchaseOrderModel fcPOrderModel = GetFranchiseeOrderDetails(OrderStatus.Cancelled, foId);
            if (fcPOrderModel == null)
                return RedirectToAction("CancelledOrders");
            fcPOrderModel.Status = OrderStatus.Cancelled;
            BindOrderStatusDdlList(OrderStatus.Cancelled);
            return View("FranchiseeOrder", fcPOrderModel);
        }
        private FCPurchaseOrderModel GetFranchiseeOrderDetails(OrderStatus orderStatus, ulong coId)
        {
            return iFranchiseeOrdersBLL.GetFranchiseeOrderDetails(coId, orderStatus);
        }

        private void BindOrderStatusDdlList(OrderStatus orderStatusId)
        {
            ViewBag.OrderStatusDdlList = iCustomerOrdersBLL.GetActiveOrderStatusDdlList((uint)(orderStatusId));
        }

        //Change Status of Current Order 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeCurrentOrderStatus(FCPurchaseOrderModel fcPurchaseOrderModel)
        {
            // Array for Complete Product Path

            string completeUrl = Request.UrlReferrer.AbsolutePath.ToString();
            string[] arrayUrl = new string[4];
            arrayUrl = completeUrl.Split('/');
            if (ModelState.IsValid)
            {
                if (iFranchiseeOrdersBLL.ChangeOrderStatus(fcPurchaseOrderModel.Id, fcPurchaseOrderModel.PoId, fcPurchaseOrderModel.OrderStatusId, SessionManager.CurrentUserId) != 0)
                {
                    // Restore Product Stock
                    if (fcPurchaseOrderModel.OrderStatusId == (uint)OrderStatus.Cancelled)
                    {
                        iFranchiseeOrdersBLL.RevertProductsStock(fcPurchaseOrderModel.PoId, (uint)OrderStatus.Cancelled, SessionManager.CurrentUserId);
                    }

                    // Add pop stock to sp when order delivered
                    if (fcPurchaseOrderModel.OrderStatusId == (uint)OrderStatus.Delivered)
                    {
                        iFranchiseeOrdersBLL.AddStockProducts(fcPurchaseOrderModel.PoId, SessionManager.CurrentUserId);
                    }

                    return RedirectToAction(arrayUrl[3] + "s");  // Redirect to last request
                }
                else
                {
                    TempData["Message"] = "Order Status not Updated successfully, Please try again!";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction(arrayUrl[3]); // Redirect to current page
                }
            }
            return View(arrayUrl[3]);
        }


        //Generate Order for Franchisee
        private void BindPurchaseProductViewDdl()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
        }

        private void BindFranchiseeList()
        {
            ViewBag.FranchiseeList = iFranchiseeOrdersBLL.GetFranchiseeList();
        }

        //code for add purchase order

        [HttpGet]
        public ActionResult GenerateFranchiseeOrder()
        {
            //ulong poId = iFranchiseeOrdersBLL.GenerateFranchiseeOrder(SessionManager.CurrentUserId, OrderStatus.InProcess, fcPurchaseOrderModel.FCUserId);
            BindFranchiseeList();
            return View();
        }

        [HttpPost]
        public ActionResult GenerateFranchiseeOrder(FCPurchaseOrderModel fcPurchaseOrderModel)
        {
            
            if(fcPurchaseOrderModel != null) {
                ulong poId = iFranchiseeOrdersBLL.GenerateFranchiseeOrder(SessionManager.CurrentUserId, OrderStatus.InProcess, fcPurchaseOrderModel.FCUserId);

                TempData["poid"] = poId; 
                return RedirectToAction("AddPurchaseOrderProduct");
            }
            return RedirectToAction("InProcessOrders");  // return to myorders List
        }

        [HttpGet]
        public ActionResult AddPurchaseOrderProduct()
        {
            ulong poId = Convert.ToUInt64(TempData["poid"]);
            if(poId != 0)
            {
                if (poId != 0)
                {
                    FCPurchaseOrderModel fcPurchaseOrderModel = iFranchiseeOrdersBLL.GetPurchaseOrderProductToAddList(poId);
                    //fcPurchaseOrderModel.Id = poId;
                    BindPurchaseProductViewDdl();
                    return View("AddFranchiseeOrderProduct", fcPurchaseOrderModel);
                }
            }
            return RedirectToAction("InProcessOrders");  // return to myorders List
        }
        
        [HttpPost]
        public ActionResult AddFranchiseeOrderProduct(FCPurchaseOrderModel fcPurchaseOrderModel)
        {
            if (ModelState.IsValid)
            {
                if (fcPurchaseOrderModel != null)
                {
                    if (iFranchiseeOrdersBLL.AddPurchaseOrderProduct(fcPurchaseOrderModel) != 0)
                    {
                        TempData["Message"] = "Product added successfully.";
                        TempData["CssClass"] = "text-success";
                    }
                    else
                    {
                        TempData["Message"] = "Oops error Occured, Please try again!";
                        TempData["CssClass"] = "text-danger";
                    }
                    TempData["poId"] = fcPurchaseOrderModel.Id;
                    return RedirectToAction("AddPurchaseOrderProduct");
                }
            }
            return RedirectToAction("InProcessOrders");    // redirect to my order list;
        }

        [HttpGet]
        public ActionResult DeleteFranchiseePurchaseProduct(ulong poId = 0, ulong prodId = 0)
        {
            if (poId != 0 && prodId != 0)
            {
                if (iFranchiseeOrdersBLL.DeleteFranchiseePurchaseProduct(poId, prodId) != 0)
                {
                    TempData["Message"] = "Product deleted successfully.";
                    TempData["CssClass"] = "text-success";
                }
                else
                {
                    TempData["Message"] = "product not deleted successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            TempData["poId"] = poId;
            return RedirectToAction("AddPurchaseOrderProduct");
        }

        [HttpPost]
        public ActionResult AddPurchaseOrderProduct(string[] ProductId, string[] Quantity, ulong Id, ulong FCUserId, string OrderDate)
        {
            if (ProductId != null && Quantity != null)
            {
                // Id = Ref Order Id;
                //FCUserId = Franchisee Created user id;
                if (iFranchiseeOrdersBLL.UpdateFranchiseePOSubmitType(SubmitType.Permanent, Id) != 0)
                {
                    if (iFranchiseeOrdersBLL.GetPurchaseOrderProductDetails(ProductId, Quantity, Id, FCUserId, OrderDate, OrderStatus.InProcess, SubmitType.Temperary) != 0)
                    {
                        return RedirectToAction("FranchiseePurchaseOrders");
                    }
                    else
                    {
                        TempData["poId"] = Id;
                        return RedirectToAction("AddPurchaseOrderProduct");
                    }
                }
            }
            return null;
        }

        public ActionResult PrintOrder(ulong orderId = 0, ulong orderStatus = 0)
        {
            FranchiseeOrderViewModels franchiseeOrderModel = null;
            if (orderId != 0)
                franchiseeOrderModel = iFranchiseeOrdersBLL.GetFranchiseeOrderToPrint(orderId, orderStatus);
            return View(franchiseeOrderModel);
        }
    }
}