﻿using System.Web.Mvc;

namespace DailyBasket.Warehouse.Areas.ReturnIssue
{
    public class ReturnIssueAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ReturnIssue";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ReturnIssue_default",
                "ReturnIssue/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}