﻿using DailyBasket.Warehouse.BLL;
using DailyBasket.Warehouse.Core.Model.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        DashboardBLL dashboardBLL;
        public DashboardController()
        {
            dashboardBLL = new DashboardBLL();
        }
        // GET: Dashboard
        public ActionResult Index()
        {
            DashboardModel dashboardModel = dashboardBLL.Get_No_of_Customers();
            if (dashboardModel != null)
            {
                return View("Dashboard", dashboardModel);
            }
            return null;
        }
    }
}