﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Web;

namespace DailyBasket.Warehouse.Helper
{
    public class ImageUploadHelper
    {
        //Production Image Path
        public static string fullPath = "c:/inetpub/vhosts/s198-12-154-142.secureserver.net/static.dailybaskets.co.in/";
        //Development Image Path
        //public static string fullPath = "c:/inetpub/vhosts/s198-12-154-142.secureserver.net/static.dailybaskets.co.in/dev-static/";
        public static string UploadProductImage(HttpPostedFileBase file)
        {
            // Old Code
            /*
            var productImagepath = "~/Static/Uploads/Products/";
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(productImagepath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(productImagepath));
                //SetPermissions(productImagepath);
            }

            if(file != null)
            {
                string imagePath = HttpContext.Current.Server.MapPath(productImagepath);
                string fileName = Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("yyyymmddMMss") + Path.GetExtension(file.FileName);
                file.SaveAs(Path.Combine(imagePath, fileName));
                return productImagepath.Substring(2) + fileName;
            }
            return null;
            */
            return UploadImage(file, "Uploads/Products/");
        }

        public static string UploadBrandImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/ProductBrand/");
        }

        public static string UploadMainCategoryImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/ProductMainCategory/");
        }

        public static string UploadSubCategoryImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/ProductSubCategory/");
        }

        public static string UploadWebSliderImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/Slider/Web/");
        }

        public static string UploadAndroidSliderImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/Slider/Android/");
        }

        public static string UploadOfferImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/Offers/");
        }

        public static string UploadTieupSliderImage(HttpPostedFileBase file)
        {
            return UploadImage(file, "Uploads/Offers/Tieup/");
        }

        private static void SetPermissions(string dirPath)
        {
            DirectoryInfo info = new DirectoryInfo(dirPath);
            WindowsIdentity self = WindowsIdentity.GetCurrent();
            DirectorySecurity ds = info.GetAccessControl();
            ds.AddAccessRule(
                new FileSystemAccessRule(self.Name,
                    FileSystemRights.FullControl,
                    InheritanceFlags.ObjectInherit |
                    InheritanceFlags.ContainerInherit,
                    PropagationFlags.None,
                    AccessControlType.Allow
                 )
            );
            info.SetAccessControl(ds);
        }

        private static string UploadImage(HttpPostedFileBase file, string directory)
        {
            //string fullPath = "c:/inetpub/vhosts/s198-12-154-142.secureserver.net/static.dailybaskets.co.in/" + directory;
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(fullPath + directory);
                //SetPermissions(fullPath + directory);
            }

            if (file != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("yyyymmddMMss") + Path.GetExtension(file.FileName);
                file.SaveAs(Path.Combine(fullPath + directory, fileName));
                return directory + fileName;
            }
            return null;
        }

        public static void DeleteImage(string imagePath)
        {
            if (File.Exists(fullPath + imagePath))
            {
                File.Delete(fullPath + imagePath);
            }
        }
    }
}