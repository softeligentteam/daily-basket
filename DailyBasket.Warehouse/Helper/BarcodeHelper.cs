using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Helper
{
    public class BarcodeHelper
    {
        public static string AutoGenerateBarcode(ulong productId)
        {
            const int barcodeLength = 6;
            string tempProdId = Convert.ToString(productId);
            return tempProdId.PadLeft(barcodeLength, '0');
        }
    }
}