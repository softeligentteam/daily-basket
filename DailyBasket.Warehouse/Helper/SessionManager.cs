﻿using DailyBasket.Warehouse.Areas.Purchase.BLL;
using DailyBasket.Warehouse.Core.Model.Login;
using DailyBasket.Warehouse.Core.Model.Purchase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace DailyBasket.Warehouse.Helper
{
    public class SessionManager
    {
        public static bool SetPurchaseOrderCookie(ulong poid)
        {
            PurchaseOrderModel purchaseOrderModel = new PurchaseProductBLL().GetPurchaseOrderToAddProduct(poid);
            if (purchaseOrderModel != null)
            {
                HttpCookie poCookie = new HttpCookie("PoModelCookie");
                poCookie["PoId"] = purchaseOrderModel.Id.ToString();
                poCookie["BillDate"] = purchaseOrderModel.BillDate.ToString();
                poCookie["FillingDate"] = purchaseOrderModel.FillingDate.ToString();
                poCookie["BillNo"] = purchaseOrderModel.BillNo.ToString();
                poCookie["VendorName"] = purchaseOrderModel.VendorName.ToString();
                poCookie["ProductTotalCount"] = "0";
                poCookie["ProductTotalAmount"] = "0";
                poCookie["BillDiscountAmount"] = purchaseOrderModel.BillDiscountAmount.ToString();
                poCookie["TotalBillAmount"] = purchaseOrderModel.TotalBillAmount.ToString();
                poCookie["SubmitType"] = purchaseOrderModel.SubmitType.ToString();
                poCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;

        }

        public static bool UpdatePurchaseOrderCookie(double TotalProductAmount)
        {
            HttpCookie poCookie = HttpContext.Current.Request.Cookies["PoModelCookie"];
            if (poCookie != null)
            {
                poCookie.Values["ProductTotalCount"] = (Convert.ToUInt16(poCookie.Values["ProductTotalCount"]) + 1).ToString();
                poCookie.Values["ProductTotalAmount"] = String.Format("{0:0.00}", Convert.ToDouble(poCookie.Values["ProductTotalAmount"]) + TotalProductAmount);
                poCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }

        public static bool UpdatePendingPurchaseOrderCookie(int TotalProductsCount, double TotalProductAmount)
        {
            HttpCookie poCookie = HttpContext.Current.Request.Cookies["PoModelCookie"];
            if (poCookie != null)
            {
                poCookie.Values["ProductTotalCount"] = TotalProductsCount.ToString();
                poCookie.Values["ProductTotalAmount"] = String.Format("{0:0.00}", TotalProductAmount);
                poCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }

        public static bool UpdatePurchaseTotalBillAmountCookie(float UpdatedBillAmount)
        {
            HttpCookie poCookie = HttpContext.Current.Request.Cookies["PoModelCookie"];
            if (poCookie != null)
            {
                poCookie.Values["TotalBillAmount"] = UpdatedBillAmount.ToString();
                poCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }

        public static bool UnsetPurchaseOrderCookie()
        {
            if (HttpContext.Current.Request.Cookies["PoModelCookie"] != null)
            {
                HttpCookie poCookie = HttpContext.Current.Request.Cookies["PoModelCookie"];
                poCookie.Value = null;
                poCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }

        public static bool IsSetPurchaseOrderCookie()
        {
            HttpCookie poCookie = HttpContext.Current.Request.Cookies["PoModelCookie"];
            if (poCookie != null && (poCookie.Value != "" || poCookie.Value != null))
            {
                poCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }

        public static void SetFormsAuthentication(SessionUserModel sessionUserModel)
        {
            // Autherization
            FormsAuthentication.SetAuthCookie(sessionUserModel.UserType+sessionUserModel.UserId, false);

            var userData = new Dictionary<string, string>();
            userData.Add("CUserId", sessionUserModel.UserId.ToString());
            userData.Add("CUserName", sessionUserModel.UserName);
            userData.Add("CRole", sessionUserModel.UserType);
            string userDataString = JsonConvert.SerializeObject(userData);
            var authTicket = new FormsAuthenticationTicket(1, "UserDataCookie", DateTime.Now, DateTime.Now.AddMinutes(30), false, userDataString);
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            //HttpContext.Response.Cookies.Add(authCookie);
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        public static void UnsetFormsAuthentication()
        {
            if(HttpContext.Current.Request != null)
            {
                /*FormsAuthentication.SignOut();
                //HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                //HttpContext.Current.Session.Abandon();

                // clear authentication cookie
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                authCookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(authCookie);
                
                HttpContext.Current.Request.Cookies.Clear();
                HttpContext.Current.Response.Cookies.Clear();*/

                FormsAuthentication.SignOut();

                // Drop all the information held in the session
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();

                // clear authentication cookie
                HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie1.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie1);

                // clear session cookie
                HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
                cookie2.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie2);
            }
        }

        public static ulong CurrentUserId
        {
            get
            {
                ulong userId = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    userId = (ulong)Convert.ToInt64(userData["CUserId"].ToString());
                }

                return userId;
            }
        }

        public static string CurrentUserName
        {
            get
            {
                string userName = string.Empty;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    userName = userData["CUserName"].ToString();
                }

                return userName;
            }
        }

        public static string CurrentUserType
        {
            get
            {
                string userType = string.Empty;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    userType = userData["CRole"].ToString();
                }

                return userType;
            }
        }
    }
}