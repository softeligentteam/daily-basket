﻿using DailyBasket.Warehouse.Core.Model.GST;
using DailyBasket.Warehouse.Repository.ProductMaster;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DailyBasket.Warehouse.Helper
{
    public class GstCalcHelper
    {
        public GSTAmountModel CalculateProductGst(ulong productId, float amount)
        {
            DataTable dataTable = new ProductRepository().GetProductGSTRates(productId);
            if(dataTable != null)
            {
                GSTAmountModel gstAmountModel = new GSTAmountModel();
                gstAmountModel.SGST = Convert.ToSingle(dataTable.Rows[0]["sgst"].ToString());
                gstAmountModel.CGST = Convert.ToSingle(dataTable.Rows[0]["cgst"].ToString());
                gstAmountModel.IGST = 0;
                
                if(gstAmountModel.SGST != 0 && gstAmountModel.CGST != 0)
                {
                    gstAmountModel.SGSTAmount = amount - (amount / (1 + (gstAmountModel.SGST / 100)));
                    gstAmountModel.CGSTAmount = amount - (amount / (1 + (gstAmountModel.CGST / 100)));
                    gstAmountModel.IGSTAmount = 0;
                    return gstAmountModel;
                }
                gstAmountModel.SGSTAmount = 0;
                gstAmountModel.CGSTAmount = 0;
                gstAmountModel.IGSTAmount = 0;
                return gstAmountModel;
            }
            return null;
        }
    }
}