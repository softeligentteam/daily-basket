﻿using System.Web.Optimization;

namespace DailyBasket.Warehouse.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Template CSS Bundles
            bundles.Add(new StyleBundle("~/bundles/css/layout").Include(
                        "~/contents/Skins/_all-skins.min.css",
                        "~/contents/blue.css",
                        "~/contents/custom.css"));

            bundles.Add(new StyleBundle("~/bundles/css/bootstrap").Include(
                      "~/contents/bootstrap/bootstrap.min.css",
                      "~/contents/date-picker/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/bundles/css/date-picker").Include(
                      "~/contents/date-picker/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/bundles/css/dailybasket").Include(
                      "~/contents/DailyBasket.min.css"));

            // Template JS Bundles
            bundles.Add(new ScriptBundle("~/bundles/script/jquery").Include(
                        "~/scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/script/layout").Include(
                     "~/scripts/app.min.js",
                      "~/scripts/pages/dashboard.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/bootstrap-ui").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/datepicker/jquery.inputmask.date.extensions.js",
                      "~/scripts/datepicker/jquery-inputmask.js",
                      "~/scripts/datepicker/date-picker.js",
                      "~/scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/bootstrap").Include(
                      "~/scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/moment").Include(
                      "~/scripts/raphael-min.js",
                      "~/scripts/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/jQuery-2.2.0").Include(
                      "~/scripts/jQuery-2.2.0.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/jquery-ui").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/jquery-ui.js"));

            //Asynchronous Requests JS

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/brand-form-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/brand-form-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/pbrand-image-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/pbrand-image-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/product-add-form-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-add-form-ddl.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-form-validation.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-image-validations.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/product-update-form-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-update-form-ddl.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-form-validation.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-image-validations.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/product-hsn-form").Include(
                     "~/scripts/jQuery-2.2.0.min.js",
                     "~/scripts/AsyncRequests/ProductMaster/product-hsn-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/product-maincat-form").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/maincat-form-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/product-subcat-form").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/subcat-form-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/pcategory-image-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/pcategory-image-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/vendor-add-form-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/vendor-add-form-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/vendor-update-form-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/vendor-update-form-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/Purchase/purchase-sale-entry-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-autopopulate-list.js",
                      "~/scripts/AsyncRequests/Purchase/purchase-sale-entry-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/ProductMaster/convertable-product-list").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/sub-category-ddl.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-list-by-mscat.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/Purchase/pop-validatations-form").Include(
                     "~/scripts/AsyncRequests/ProductMaster/product-sale-rate.js",
                     "~/scripts/AsyncRequests/Stock/product-batch-validation.js",
                     "~/scripts/AsyncRequests/Purchase/pop-validatations-form.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/purchase/purchase-order-date-validations").Include(
                   "~/scripts/jQuery-2.2.0.min.js",
                   "~/scripts/AsyncRequests/Purchase/po-validations-form.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/Purchase/update-purchase-order-product").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-sale-rate.js",
                      "~/scripts/AsyncRequests/Stock/product-batch-validation.js",
                       "~/scripts/AsyncRequests/ProductMaster/product-autopopulate-list.js",
                      "~/scripts/AsyncRequests/Purchase/pop-update-validation-forms.js"));

            /*bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/product-sale-rate").Include(
                      "~/scripts/AsyncRequests/ProductMaster/product-sale-rate.js"));*/

            bundles.Add(new ScriptBundle("~/bundles/script/async/users/users-validation").Include(
                     "~/scripts/AsyncRequests/users/users-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/stock/product-conversion-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/Stock/product-conversion-validation.js",
                      "~/scripts/AsyncRequests/Stock/product-batch-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/purchase/delete-pending-purchase-entry").Include(
                     "~/scripts/jQuery-2.2.0.min.js",
                     "~/scripts/AsyncRequests/Purchase/pop-delete-validation-form.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/webapp/offer-add-form-validations").Include(
                     "~/scripts/jQuery-2.2.0.min.js",
                     "~/scripts/AsyncRequests/WebApp/Offer/offer-add-form-validations.js",
                     "~/scripts/AsyncRequests/WebApp/Offer/offer-image-validation.js"));
            
            // JS Code
            bundles.Add(new ScriptBundle("~/bundles/script/async/productmaster/product-autopopulate-list-with-jquery").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-autopopulate-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/webapp/slider/android-slider-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/WebApp/Slider/android-slider-image-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/webapp/slider/web-slider-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/WebApp/Slider/web-slider-image-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/tab-switch-script").Include(
                      "~/scripts/tab-switch-script.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/sale/print-customer-orders").Include(
                "~/scripts/jQuery-2.2.0.min.js",
                "~/scripts/AsyncRequests/Sale/print-customer-order.js"));


            bundles.Add(new ScriptBundle("~/bundles/script/sale/print-franchisee-orders").Include(
              "~/scripts/jQuery-2.2.0.min.js",
              "~/scripts/AsyncRequests/Sale/print-franchisee-order.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/product-master/brand-ddl").Include(
                     "~/scripts/jQuery-2.2.0.min.js",
                     "~/scripts/AsyncRequests/ProductMaster/brand-ddl.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/users/franchisee-add-form-validation").Include(
                     "~/scripts/jQuery-2.2.0.min.js",
                     "~/scripts/AsyncRequests/Users/franchisee-add-form-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/users/franchisee-update-form-validation").Include(
                   "~/scripts/jQuery-2.2.0.min.js",
                   "~/scripts/AsyncRequests/Users/franchisee-update-form-validation.js"));

        }
    }
}